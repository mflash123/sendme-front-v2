type DialogDefaultContextType = {
  open: boolean;
  setOpen: (value: boolean) => void;
}

type ContextType = {
    wizard: IWizard;
    //saveWizard: (data) => void;
   // finishWizard: () => void; //123123
    saveWizard: (data,isFinish?:boolean) => void;
  };
  
  interface IWizard {
    //setWizard:(data?:any,callback?) => void;
  }

  // type DraftjsTextContextType = {
  //   editorState: import('draft-js').EditorState; 
  //   setEditorState: (value: import('draft-js').EditorState) => void;
  //   contentState?: any;
  //   setContentState: (contentId?:number)=>void;
  // }
  type DraftjsTextContextType = {
    editorState: import('draft-js').EditorState;
    setEditorState: (value: import('draft-js').EditorState) => void;
    contentState?: any;
    setContentState: (contentId?:number)=>void;
  }