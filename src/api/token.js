const token = () => {
    return localStorage.getItem('token')
}

export default token