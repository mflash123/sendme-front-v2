import token from "./token";
import { getUser, getInstagram } from "./getUser";
import typeGroups from "./typeGroups";
import typesGroup from "./typesGroup";
import {
  storeContent,
  getContent,
  updateContent,
  deleteContent,
  weightContent,
  pinContent,
} from "./content";
import {
  getProducts,
  getProduct,
  updateProduct,
  updateProductWeight,
  removeProduct,
  storeProduct,
} from "./product";
import { getIntegration, getIntegrationbyType } from "./getIntegration";
import { getPaymentLink } from "./payment";
import { setTheme, getThemeApi } from "./theme";
import {
  updateUsername,
  getInstagramUrl,
  callbackInstagram,
  isInstagramConnect,
  deleteUser,
  getAvailableCategory,
  userReport,
} from "./user";
import { getActions, getAction } from "./actions";

export {
  token,
  getUser,
  getInstagram,
  typeGroups,
  typesGroup,
  storeContent,
  getContent,
  updateContent,
  deleteContent,
  weightContent,
  pinContent,
  getProducts,
  getProduct,
  storeProduct,
  updateProduct,
  updateProductWeight,
  removeProduct,
  getIntegration,
  getIntegrationbyType,
  getPaymentLink,
  setTheme,
  getThemeApi,
  updateUsername,
  getInstagramUrl,
  callbackInstagram,
  isInstagramConnect,
  deleteUser,
  getAvailableCategory,
  getActions,
  getAction,
  userReport,
};
