import styled, { keyframes } from "styled-components";
import {
  FormLabel,
  TextField,
  FormGroup,
  Typography,
  FormControlLabel,
  Avatar,
  Link,
  Button,
} from "@material-ui/core";
import { Editor } from "@tinymce/tinymce-react";

export const FormStyled = styled.form`
  margin-top: 20px;
  margin-bottom: 20px;
`;

export const FormGroupStyled = styled(FormGroup)`
  margin-top: 10px;
  margin-bottom: 25px;
`;

export const FormLabelStyled = styled(FormLabel)`
  margin-bottom: 5px;
`;

export const ButtonMarginStyled = styled(Button)`
  margin-bottom: 5px;
`;

export const DashboardLabel = styled(Typography)`
  font-weight: 300;
`;

export const BlockLabel = styled(Typography)`
  font-weight: 600;
  font-size: 1.25rem;
  padding-bottom: 1rem;
`;

export const SwitchForm = styled(FormControlLabel)`
  margin-top: 15px;
`;

export const EditorTiny = styled(Editor)`
  border-bottom: 1px solid black;
`;

const breatheAnimation = keyframes`
 0% { box-shadow: 0 0 10px 5px rgb(0 0 0 / 0%) }
 50% { box-shadow: 0 0 10px 5px rgb(0 0 0 / 50%) }
 100% { box-shadow: 0 0 10px 5px rgb(0 0 0 / 0%) }
`;

export const AvatarStyled = styled(Avatar)`
  width: fit-content;
  height: fit-content;
  animation-name: ${breatheAnimation};
  animation-duration: 4s;
  animation-iteration-count: infinite;
`;

export const LinkStyled = styled(Link)`
  margin-top: 15px;
`;

export const EditorDraftStyled = styled.div`
  box-sizing: border-box;
  border: 1px solid #ddd;
  cursor: text;
  padding: 16px;
  border-radius: 2px;
  margin-bottom: 2em;
  box-shadow: inset 0 1px 8px -3px #ababab;
  background: #fefefe;
`;

export const EditorButton = styled.button`
  background: #fbfbfb;
  color: #888;
  font-size: 18px;
  border: 0;
  padding-top: 5px;
  vertical-align: bottom;
  height: 34px;
  width: 36px;
  &:hover {
    background: #f3f3f3;
    outline: 0;
  }
`;

const FadeInAnimation = keyframes`
0%   { opacity: 0; }
100% { opacity: 1; }
`;

export const FadeIn = styled.div`
  -webkit-animation: ${FadeInAnimation} 1s ease-out; /* Safari 4+ */
  -moz-animation: ${FadeInAnimation} 1s ease-out; /* Fx 5+ */
  -o-animation: ${FadeInAnimation} 1s ease-out; /* Opera 12+ */
  animation: ${FadeInAnimation} 1s ease-out; /* IE 10+, Fx 29+ */
`;
