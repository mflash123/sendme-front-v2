import { createMuiTheme, ThemeProvider } from '@material-ui/core/styles';

import grey from '@material-ui/core/colors/grey';

const DefaultTheme = createMuiTheme({
  palette: {
    type: 'light',
    background: {
      paper:"#fff",
      default:grey[50]
    }
  },
});

export default DefaultTheme