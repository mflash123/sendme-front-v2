import { createMuiTheme, ThemeProvider } from '@material-ui/core/styles';
import {
  red,pink,purple,deepPurple,indigo,blue,lightBlue,cyan,teal,green,lightGreen,lime,yellow,amber,orange,deepOrange,brown
} from '@material-ui/core/colors';

const RedTheme = createMuiTheme({
  //background: 'linear-gradient(45deg, #FE6B8B 30%, #FF8E53 90%)',
  palette: {
    type: 'dark',
    primary: {
      main : red[900],
    },
    background: {
      paper:red[700],
      default:red[900]
      
    }
  },
});

const PinkTheme = createMuiTheme({
  palette: {
    type: 'dark',
    primary: {
      main : pink[900],
    },
    background: {
      paper:pink[700],
      default:pink[900]
    }
  },
});

const PurpleTheme = createMuiTheme({
  palette: {
    type: 'dark',
    primary: {
      main : purple[900],
    },
    background: {
      paper:purple[700],
      default:purple[900]
    }
  },
});

const DeepPurpleTheme = createMuiTheme({
  palette: {
    type: 'dark',
    primary: {
      main : deepPurple[900],
    },
    background: {
      paper:deepPurple[700],
      default:deepPurple[900]
    }
  },
});

const IndigoTheme = createMuiTheme({
  palette: {
    type: 'dark',
    primary: {
      main : indigo[900],
    },
    background: {
      paper:indigo[700],
      default:indigo[900]
    }
  },
});

const BlueTheme = createMuiTheme({
  palette: {
    type: 'dark',
    primary: {
      main : blue[900],
    },
    background: {
      paper:blue[700],
      default:blue[900]
    }
  },
});

const LightBlueTheme = createMuiTheme({
  palette: {
    type: 'dark',
    primary: {
      main : lightBlue[900],
    },
    background: {
      paper:lightBlue[700],
      default:lightBlue[900]
    }
  },
});

const CyanTheme = createMuiTheme({
  palette: {
    type: 'dark',
    primary: {
      main : cyan[900],
    },
    background: {
      paper:cyan[700],
      default:cyan[900]
    }
  },
});

const TealTheme = createMuiTheme({
  palette: {
    type: 'dark',
    primary: {
      main : teal[900],
    },
    background: {
      paper:teal[700],
      default:teal[900]
    }
  },
});

const GreenTheme = createMuiTheme({
  palette: {
    type: 'dark',
    primary: {
      main : green[900],
    },
    background: {
      paper:green[700],
      default:green[900]
    }
  },
});

const LightGreenTheme = createMuiTheme({
  palette: {
    type: 'dark',
    primary: {
      main : lightGreen[900],
    },
    background: {
      paper:lightGreen[700],
      default:lightGreen[900]
    }
  },
});

const LimeTheme = createMuiTheme({
  palette: {
    type: 'light',
    primary: {
      main : lime[900],
    },
    background: {
      paper:lime[700],
      default:lime[900]
    }
  },
});

const YellowTheme = createMuiTheme({
  palette: {
    type: 'light',
    primary: {
      main : yellow[900],
    },
    background: {
      paper:yellow[700],
      default:yellow[900]
    }
  },
});

const AmberTheme = createMuiTheme({
  palette: {
    type: 'light',
    primary: {
      main : amber[900],
    },
    background: {
      paper:amber[700],
      default:amber[900]
    }
  },
});

const OrangeTheme = createMuiTheme({
  palette: {
    type: 'light',
    primary: {
      main : orange[900],
    },
    background: {
      paper:orange[700],
      default:orange[900]
    }
  },
});

const DeepOrangeTheme = createMuiTheme({
  palette: {
    type: 'dark',
    primary: {
      main : deepOrange[900],
    },
    background: {
      paper:deepOrange[700],
      default:deepOrange[900]
    }
  },
});

const BrownTheme = createMuiTheme({
  palette: {
    type: 'dark',
    primary: {
      main : brown[900],
    },
    background: {
      paper:brown[700],
      default:brown[900]
    }
  },
});

export  {
  RedTheme,
  PinkTheme,
  PurpleTheme,
  DeepPurpleTheme,
  IndigoTheme,
  BlueTheme,
  LightBlueTheme,
  CyanTheme,
  TealTheme,
  GreenTheme,
  LightGreenTheme,
  LimeTheme,
  YellowTheme,
  AmberTheme,
  OrangeTheme,
  DeepOrangeTheme,
  BrownTheme,
}