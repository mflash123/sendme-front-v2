import DefaultTheme from './DefaultTheme'
import DarkTheme from './DarkTheme'
import {  RedTheme,
  PinkTheme,
  PurpleTheme,
  DeepPurpleTheme,
  IndigoTheme,
  BlueTheme,
  LightBlueTheme,
  CyanTheme,
  TealTheme,
  GreenTheme,
  LightGreenTheme,
  LimeTheme,
  YellowTheme,
  AmberTheme,
  OrangeTheme,
  DeepOrangeTheme,
  BrownTheme,
} from './PackTheme'

const themes = {
    DefaultTheme,
    DarkTheme,
      RedTheme,
  PinkTheme,
  PurpleTheme,
  DeepPurpleTheme,
  IndigoTheme,
  BlueTheme,
  LightBlueTheme,
  CyanTheme,
  TealTheme,
  GreenTheme,
  LightGreenTheme,
  LimeTheme,
  YellowTheme,
  AmberTheme,
  OrangeTheme,
  DeepOrangeTheme,
  BrownTheme,
}

export default function getTheme(theme) {
    return themes[theme]
}