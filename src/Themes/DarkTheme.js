import { createMuiTheme, ThemeProvider } from '@material-ui/core/styles';
import grey from '@material-ui/core/colors/grey';

const DarkTheme = createMuiTheme({
  palette: {
    type: 'dark',
    // primary: {
    //   main : red[700],
    // },
    background: {
      paper:grey[800],
      default:grey[900]
    },

    // custom: {
    //   myOwnComponent: {
    //     margin: "10px 10px",
    //     backgroundColor: "lightgreen"
    //   }
    // }

  },
});

export default DarkTheme