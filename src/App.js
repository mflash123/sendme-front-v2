import React, { useContext } from "react";
import { Helmet } from "react-helmet";
import styled from "styled-components";

import {
  Grid,
  ThemeProvider,
  LinearProgress,
  Container,
  CssBaseline,
} from "@material-ui/core";

import { Dashboard } from "./components/Dashboard";
import { Auth, Signup, Recovery } from "./components/Auth";
import { Callback } from "./components/Dashboard/Settings/Instagram";

import { Guest } from "./components/Guest";
import { Payment } from "./components/Payment";

//Style
// import { CssBaseline } from '@material-ui/core';
// import {ThemeContext} from './context/ThemeContext'
import { Route, Switch, useParams, Redirect } from "react-router-dom";
import Axios from "axios";

export default function App({view_username}) {

  const username = localStorage.getItem("username");
  const loggedIn = username === null ? false : true;
  console.log("APP",view_username);
  return (
    <Container>
      <Grid item xs={12}>

        <CssBaseline />

        <Switch>
          <Route exact path="/signin" component={Auth} />
          <Route exact path="/signup" component={Signup} />
          <Route exact path="/logout" component={Logout} />
          <Route exact path="/recovery" component={Recovery} />
          <Route exact path="/r/:code" component={RecoverCode} />
          {/* <Route exact path="/verify/:code" component={Verify} /> */}
          <Route exact path="/callback/instagram" component={Callback} />

          <Route exact path="/payment/success" component={Success} />
          <Route exact path="/payment/fail" component={Fail} />
          <Route exact path="/payment/:tarifId" component={Payment} />

          <Route path="/dashboard">
            <Helmet>
              <title>Dashboard</title>
            </Helmet>
            {loggedIn ? <Dashboard /> : <Redirect to="/signin" />}
          </Route>
          
          <Route path="/:username">
            <Guest guest={true} username={view_username} />
          </Route>
          <Route exact path="/">
            <Guest guest={true} username={view_username} />
          </Route>
        

        </Switch>
        
      </Grid>
    </Container>
  );
}

/** Verify transfered to back side */
// const Verify = () => {
//   let { code } = useParams();
//   Axios.post(process.env.REACT_APP_API_URL + "/api/v2/verify", {
//     code,
//   });
//   return <Redirect to={"/signin?verify=1"} />;
// };

const RecoverCode = () => {
  let { code } = useParams();
  Axios.get(process.env.REACT_APP_API_URL + "/api/v2/code/" + code);
  console.log(code);
  return <Redirect to={"/signin?code=true"} />;
};

const Logout = () => {
  localStorage.removeItem("username");
  localStorage.removeItem("token");
  window.location.replace("/");
};

const Success = () => {
  const Vertcentral = styled.div`
    margin-top: 20%;
    text-align: center;
  `;
  setTimeout(() => window.location.replace("/dashboard"), 3000);

  return (
    <Vertcentral>
      <LinearProgress />
      Спасибо за оплату!
      <br />
      Через несколько секунд вы будете перенаправлены в Ваш личный кабинет
    </Vertcentral>
  );
};

const Fail = () => {
  setTimeout(() => window.location.replace("/dashboard"), 3000);

  const Vertcentral = styled.div`
    margin-top: 20%;
    text-align: center;
  `;
  return (
    <Vertcentral>
      <LinearProgress />
      К сожалению, что-то пошло не так=(
      <br />
      Попробуйте еще раз
      <br />
      Через несколько секунд вы будете перенаправлены в Ваш личный кабинет
    </Vertcentral>
  );
};
