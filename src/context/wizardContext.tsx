import * as React from "react";
import { ApiContext } from './ApiContext';


export const WizardContext = React.createContext<ContextType | null>(null);



const WizardProvider: React.FC<React.ReactNode> = ({ children }) => {
  const { storeContent } = React.useContext(ApiContext)
  const [wizard, setWizard] = React.useState<IWizard>('');
  console.log('WizardProvider')

  const saveWizard = (data: object, isFinish?: boolean) => {
    if (isFinish) {
      storeContent({ ...wizard, ...data }, 99)
      /**
       * я не делаю через стейт, потому что стейт не успевает обновиться. Поэтому я сделал isFinish, которая без стейт уже шлет в бек
       */
    } else {
      setWizard({ ...wizard, ...data }); //update
    }

  };



  console.log('Wizard state:', wizard)

  return (
    <WizardContext.Provider value={{ wizard, saveWizard }}>
      <>
        {children}
      </>
    </WizardContext.Provider>
  );
};

export default WizardProvider;
