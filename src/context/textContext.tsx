import { EditorState } from "draft-js";
import * as React from "react";

type IProps = {
//     // setState2:any;
//     // state2?:any;
//    // raw:string;
//   //priceDialog:boolean;

}
interface IContent {
  contentId?:number;
  }
export const TextContext = React.createContext<IProps | null>(null);

// const [editorState, setEditorState] = React.useState(() =>
// EditorState.createEmpty()
// );

const TextProvider: React.FC<React.ReactNode> = ({ children }) => {
   const [editorState, setEditorState] =  React.useState(() =>
   EditorState.createEmpty()
   );
   const [contentState, setContentState] =  React.useState<IContent>()

console.log(editorState,contentState)
  return (
    <TextContext.Provider value={{ editorState, setEditorState, contentState, setContentState }}>
      <>
        {children}
      </>
    </TextContext.Provider>
  );
};

export default TextProvider;
