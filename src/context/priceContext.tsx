import * as React from "react";

interface IProps {
  //priceDialog:boolean;
}
export const PriceContext = React.createContext<IProps | null>(null);



const PriceProvider: React.FC<React.ReactNode> = ({ children }) => {
  const [open, setOpen] = React.useState<IProps>(false);

  return (
    <PriceContext.Provider value={{ open, setOpen }}>
      <>
        {children}
      </>
    </PriceContext.Provider>
  );
};

export default PriceProvider;
