import React, { useContext } from 'react'
import { Button } from '@material-ui/core'
import InstagramIcon from '@material-ui/icons/Instagram';
import {ApiContext} from './../../../context/ApiContext'
import {useLocation} from "react-router-dom";

export const Instagram = () => {

    const { getInstagramUrl } = useContext(ApiContext)

    const handleClick = () => {
        getInstagramUrl()
        .then(({data})=>{
            console.log(data)
            window.location.replace(data);
        })
        .catch((error)=>{
            console.log(error)
        })

    }

    return(
        <Button
        variant="contained"
       // color="primary"
        startIcon={<InstagramIcon />}
        onClick={handleClick}
        disableElevation={true}
        >
            Подключить Instagram 👌
        </Button>

    )
}

export const Callback = () => {
    const { callbackInstagram } = useContext(ApiContext)

    const search = useLocation().search;
    const code = new URLSearchParams(search).get('code');
    callbackInstagram(code)
    .then(({data})=>{
        //console.log(data)
        localStorage.setItem("notification_connect_instagram", true);
        window.location.replace('/dashboard');
    })
    .catch((error)=>{
        console.log(error)
    })
    return true
}