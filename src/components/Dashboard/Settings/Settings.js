import React, { useDebugValue, useState, useContext } from "react";
import {
  Card,
  CardContent,
  Typography,
  Select,
  FormControl,
  TextField,
  InputAdornment,
  FormGroup,
  IconButton,
  FormHelperText,
  LinearProgress,
  Button,
  Dialog,
  DialogContent,
  DialogTitle,
  Grid,
} from "@material-ui/core";
import { FormGroupStyled } from "./../../../styled";
import { UploadCrop } from "./../../UploadCrop";
import SaveIcon from "@material-ui/icons/Save";
import { ApiContext } from "./../../../context/ApiContext";
import { useForm, Controller } from "react-hook-form";
import CheckIcon from "@material-ui/icons/Check";
import Alert from "@material-ui/lab/Alert";
import CloseIcon from "@material-ui/icons/Close";
import { Instagram } from "./Instagram";
import PhoneInput from "react-phone-input-2";
import "react-phone-input-2/lib/style.css";
import DeleteForeverIcon from "@material-ui/icons/DeleteForever";
import { DeleteAccount } from "./DeleteAccount";
import ExitToAppIcon from "@material-ui/icons/ExitToApp";

const Settings = ({ user, showSettings, setShowSettings }) => {
  const [error, setError] = useState(false);
  const [success, setSuccess] = useState(false);
  const [deleteDialog, setDeleteDialog] = useState(false);
  const { updateUsername } = useContext(ApiContext);
  const {
    register,
    handleSubmit,
    watch,
    control,
    setValue,
    formState: { errors },
  } = useForm();

  console.log(user);
  const handleClick = () => {};

  const onSubmit = (data, e) => {
    //data.email data.password
    console.log(data.username);
    if (user.tarif.id <= 2) {
      setError("Только на тарифе PRO вы можете изменить Вашу ссылку");
    } else {
      updateUsername(data, user.username)
        .then(() => {
          localStorage.setItem("username", data.username);
          setSuccess(true);
          setTimeout(() => {
            window.location.reload(); //because cant update main user url in topmenu
          }, 5000);
        })
        .catch((error) => {
          setError(error.response.data.message);
          //console.log(error.response.data.message)
        });
    }

    //мне не нужно доставать значение во внешку функции, потому что я здесь прост ставлю local storage+redirect
    // Axios.post(process.env.REACT_APP_API_URL+'/api/v2/auth',{
    //     data
    // })
    // .then(res=>{
    //     localStorage.setItem('username',res.data.username)
    //     localStorage.setItem('token',res.data.token)
    //     window.location.replace("/dashboard");
    //     console.log(res)
    // })
    // .catch(error=>{
    //     console.log(error)
    //     setError('Login or Password error')
    // })
  };


  return (
    <Dialog open={showSettings}>
      <DialogTitle>
        <IconButton
          style={{ position: "absolute", right: "5%" }}
          onClick={() => setShowSettings(!showSettings)}
        >
          <CloseIcon />
        </IconButton>
      </DialogTitle>
      <DialogContent>
        <Card elevation={0}>
          <CardContent>
            <form onSubmit={handleSubmit(onSubmit)}>
              <FormGroupStyled>
                <FormControl style={{ width: "100%" }}>
                  <Controller
                    name="username"
                    control={control}
                    defaultValue={user.username}
                    rules={{
                      required: "Field is required",
                    }}
                    render={({ field }) => (
                      <TextField
                        {...field}
                        error={errors?.username}
                        helperText={
                          Boolean(errors?.username) && "Field is required"
                        }
                        label="Ссылка на страницу"
                        variant="outlined"
                        InputProps={{
                          startAdornment: (
                            <InputAdornment position="start">
                              https://
                            </InputAdornment>
                          ),
                          endAdornment: (
                            <>
                              <InputAdornment position="end">
                                .sendme.cc/
                              </InputAdornment>

                              <InputAdornment position="end">
                                {success ? (
                                  <CheckIcon />
                                ) : (
                                  <IconButton type="submit">
                                    <SaveIcon />
                                  </IconButton>
                                )}
                              </InputAdornment>
                            </>
                          ),
                        }}
                        autoComplete="off"
                      />
                    )}
                  />

                  <FormHelperText error>{error}</FormHelperText>
                  {success && (
                    <Alert>
                      <LinearProgress /> Username changed. Reload after 5secs...
                    </Alert>
                  )}
                </FormControl>
              </FormGroupStyled>
            </form>
            <FormGroupStyled>
              <UploadCrop user={user} />
            </FormGroupStyled>
            <Grid
              container
              direction="row"
              justify="space-between"
              alignItems="center"
            >
              <Grid item>
                <Button
                  variant="contained"
                  color="secondary"
                  size="small"
                  startIcon={<DeleteForeverIcon />}
                  onClick={() => {
                    setDeleteDialog(true);
                  }}
                >
                  Delete me
                </Button>
                <FormHelperText>Delete Account</FormHelperText>
                <DeleteAccount
                  open={deleteDialog}
                  setOpen={setDeleteDialog}
                  userId={user.id}
                />
              </Grid>
              <Grid item>
                <Button
                  variant="contained"
                  component="a"
                  color="primary"
                  size="small"
                  startIcon={<ExitToAppIcon />}
                  href="/logout"
                >
                  Logout
                </Button>
                <FormHelperText>Logout from Account</FormHelperText>
                <DeleteAccount
                  open={deleteDialog}
                  setOpen={setDeleteDialog}
                  userId={user.id}
                />
              </Grid>
            </Grid>
          </CardContent>
        </Card>
      </DialogContent>
    </Dialog>
  );
};

export default Settings;
