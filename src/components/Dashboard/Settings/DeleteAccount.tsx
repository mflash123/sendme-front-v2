import { Button, Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle } from '@material-ui/core';
import React, { useContext } from 'react';
import { ApiContext } from "./../../../context/ApiContext";

export interface IProps {
    open: boolean;
    setOpen: (open: boolean) => void;
    userId: number;
}

export const DeleteAccount: React.FC<IProps> = ({ open, setOpen, userId }) => {
    const { deleteUser } = useContext(ApiContext);

    const handleClickDelete = () => {
        deleteUser(userId)
        .then(()=>{
            localStorage.removeItem("username");
            localStorage.removeItem("token");
            window.location.replace("/");
        })
        .catch((error:any)=>{
            console.log(error)
        })
    }
    return (
        <Dialog
            open={open}
        >
            <DialogTitle>Account Delete Confirmation 🤧</DialogTitle>
            <DialogContent>
                <DialogContentText>
                    When you delete your account, all data will be permanently removed from our system in accordance with GDPR.
                    This action can not be undone as all data is forever deleted.
                </DialogContentText>
                {/* <DialogContentText>
                    После удаления Вашего аккаунта, вся информация будет удалена с нашей платформы.
                    Эту операцию невозможно будет отменить.
                </DialogContentText> */}
            </DialogContent>
            <DialogActions>
                <Button variant="contained"
                    color="secondary"
                    size="small"
                    onClick={handleClickDelete}>Delete Account😭</Button>
                <Button color="primary" variant="contained" onClick={() => { setOpen(false) }}>Cancel🤗</Button>
            </DialogActions>
        </Dialog>
    )

}