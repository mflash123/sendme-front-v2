import React, { useContext, useEffect, useState } from "react";
import * as JivoSite from "react-jivosite";

import { Route, Switch, Link, useLocation } from "react-router-dom";

import { TopMenu } from "./../../layout/TopMenu";
import ShareLink from "./../../components/ShareLink";
import FooterGlobal from "./../../layout/FooterGlobal";
import Settings from "./Settings/Settings";

import { DragWorkSpace } from "../DragWorkSpace";
import { ApiContext } from "../../context/ApiContext";
import { DragItemOrderContext } from "../../context/DragItemOrderContext";

import { LinkStyled } from "./../../styled";

import { DialogWizard } from "./../Wizard/Dialog";
import {
  Grid,
  ThemeProvider,
  Breadcrumbs,
  Typography,
} from "@material-ui/core";

import SettingsIcon from "@material-ui/icons/Settings";
import HomeIcon from "@material-ui/icons/Home";
import Alert from "@material-ui/lab/Alert";

import { Guest } from "../Guest";
import { Group } from "../Group";
import { TypesList, Type } from "../Types";

/** Product create/modify */
//import Product from "./../Types/Product/Product";
//import Intergation from '../Integration/Integration'
import { ThemeContext } from "./../../context/ThemeContext";
import { ToggleButton, ToggleButtonGroup } from "@material-ui/lab";
import { logo } from "../../base64";
import YouTube from "react-youtube";
import { NotificationInstagramSuccess } from "../Notifications/Notifications";
import PriceProvider from "./../../context/priceContext";
import TextProvider from "./../../context/textContext";

const ProductList = React.lazy(() => import("./../Types/Product/ProductList"));
const Product = React.lazy(() => import("./../Types/Product/Product"));

const Dashboard = () => {
  const [muiTheme] = useContext(ThemeContext);
  const [showSettings, setShowSettings] = useState(false);
  const [loading, setLoading] = useState(true);

  const username = localStorage.getItem("username");
  if (username === null) {
  }

  /**API */
  const { weightContent, getUser } = useContext(ApiContext);
  const { items, setItems } = useContext(DragItemOrderContext);
  const [tarif, setTarif] = useState({ id: 1, name: "Free" });
  const [active, setActive] = useState(true);
  const [user, setUser] = useState(false);
  const [productPreview, setProductPreview] = useState(false);
  const [wizard, setWizard] = useState(false);

  const handleDrag = (result) => {
    //console.log(result)
    let newDragItems = Array.from(items);
    let copied = newDragItems.splice(result.source.index, 1)[0];
    newDragItems.splice(result.destination.index, 0, copied);
    setItems(newDragItems);
    //order to api
    console.log(newDragItems);
  };

  const routes = [
    {
      path: "/dashboard",
      exact: true,
      component: () => (
        <DragWorkSpace handleDrag={handleDrag} items={items} tarif={tarif} />
      ),
      //preview: () => <Guest guest={false} user={user} /> Perfomance problem
    },
    {
      path: "/dashboard/add",
      exact: true,
      component: () => <Group tarif={tarif} />,
      breadcrumbs: [
        {
          name: "Dashboard",
          link: "/dashboard",
        },
        {
          name: "Add",
          link: "/dashboard/add",
        },
      ],
    },
    {
      path: "/dashboard/add/:groupId",
      exact: true,
      component: () => <TypesList />,
      breadcrumbs: [
        {
          name: "Dashboard",
          link: "/dashboard",
        },
        {
          name: "Add",
          link: "/dashboard/add",
        },
        {
          name: "Messages",
          link: "/dashboard/add/2",
        },
      ],
    },
    {
      path: "/dashboard/add/:groupId/:typeId",
      exact: true,
      component: () => <Type />,
      breadcrumbs: [
        {
          name: "Dashboard",
          link: "/dashboard",
        },
        {
          name: "Add",
          link: "/dashboard/add",
        },
      ],
    },
    {
      path: "/dashboard/product",
      exact: true,
      component: () => <Product />,
    },
    {
      path: "/dashboard/product/:productId",
      exact: true,
      component: () => <Product edit={true} />,
      breadcrumbs: [
        {
          name: "Dashboard",
          link: "/dashboard",
        },
        {
          name: "Add",
          link: "/dashboard/add",
        },
      ],
    },
    {
      path: "/dashboard/plist/:contentId",
      exact: true,
      component: () => <ProductList />,
    },
    {
      path: "/dashboard/edit/:contentId",
      exact: true,
      component: () => <Type />,
    },
  ];

  useEffect(() => {
    getUser(username).then((value) => {
      if (typeof value != "undefined") {
        setUser(value.data);

        if (value.data.isAdmin) {
          console.log(value.data);
          if (!items) {
            console.log(value.data.items);
            setItems(value.data.items);
            setTarif(value.data.tarif);
            setActive(value.data.isActive);
            setLoading(false);
          }
        } else {
          document.location.href = "/signin";
        }
      }
    });
  }, []);

  useEffect(() => {
    //weightContent callback
    const data = async () => {
      console.log("weightContent callback");
      weightContent(items);
    };
    data();

    if (items.length < 1) {
      setWizard(true);
    }
  }, [items]);

  useEffect(() => {
    window.jivo_onLoadCallback = () => {
      const data = {
        name: username,
        description: "https://sendme.cc/" + username + "\nTarif: " + tarif.name,
      };
      window.jivo_api.setContactInfo(data);
    };
  }, [username]);

  if (loading)
    return (
      <div style={{ position: "absolute", left: "50%" }}>
        <img style={{ position: "relative", left: "-50%" }} src={logo} />
      </div>
    );

  return (
    <ThemeProvider theme={muiTheme.mainTheme}>
      <PriceProvider>
        <TextProvider>
        <NotificationInstagramSuccess />
        <TopMenu tarif={tarif} user={user} settingsCallback={setShowSettings} />
        <DialogWizard open={wizard} />

        <JivoSite.Widget id="Pcv2LhpIM3" />
        <Grid style={{ paddingLeft: 24, paddingRight: 24, paddingBottom: 60 }}>
          <ShareLink username={username} active={active} />
          {/* 
        Settings могу теперь перенести в TopMenu, тут безсмыслкенно дергаю стект и пробрасываю его в ТОПМЕНЮ
         */}
          <Settings
            user={user}
            showSettings={showSettings}
            setShowSettings={setShowSettings}
          />
          <Grid container>
            <Grid item md={6} xs={12}>
              <Switch>
                {routes.map((route, index) => {
                  return (
                    <Route key={index} path={route.path} exact={route.exact}>
                      <Breadcrumbs aria-label="breadcrumb">
                        {typeof route.breadcrumbs !== "undefined"
                          ? route.breadcrumbs.map((val) => (
                              <LinkStyled component={Link} to={val.link}>
                                {val.name}
                              </LinkStyled>
                            ))
                          : ""}
                      </Breadcrumbs>
                      <route.component />
                    </Route>
                  );
                })}
              </Switch>
            </Grid>
            <Grid item md={6} xs={12} style={{ minHeight: 700 }}>
              {items.length > 0 ? (
                <Guest guest={false} user={user} />
              ) : (
                <>
                  <Alert severity="error">Добавьте хотябы один элемент</Alert>
                  <YouTube videoId="TFOWsfZB5T8" />
                </>
              )}
            </Grid>
          </Grid>
        </Grid>

        <Grid
        container
          alignContent="center"
          alignItems="center"
          justify="center"
          style={{ display: "flex", flexDirection: "column" }}
        >
          <FooterGlobal />
        </Grid>
        </TextProvider>
      </PriceProvider>
    </ThemeProvider>
  );
};

export default Dashboard;
