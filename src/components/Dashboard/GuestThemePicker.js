import React from 'react'
import styled from 'styled-components'
import {
    red,grey,pink,purple,deepPurple,indigo,blue,lightBlue,cyan,teal,green,lightGreen,lime,yellow,amber,orange,deepOrange,brown
  } from '@material-ui/core/colors';

import { Button, ButtonGroup } from '@material-ui/core';


// const ButtonGroupStyledDesktop = styled(ButtonGroup)`
//   @media(max-width: 1150px) {
//     display:none
//   }
// `;

const ButtonGroupStyledMobile = styled(ButtonGroup)`
  display: table;
    margin: auto;
    margin-bottom: 20px;
`;

const GuestThemePicker = ({handlePickGuestTheme}) => {
    return(
        <ButtonGroupStyledMobile size="small">
            <Button style={{height:20,minWidth:20,backgroundColor:'white'}} onClick={handlePickGuestTheme} name="DefaultTheme"></Button>
            <Button style={{height:20,minWidth:20,backgroundColor:grey[800]}} onClick={handlePickGuestTheme} name="DarkTheme"></Button>
            <Button style={{height:20,minWidth:20,backgroundColor:red[700]}} onClick={handlePickGuestTheme} name="RedTheme"></Button>
            <Button style={{height:20,minWidth:20,backgroundColor:pink[700]}} onClick={handlePickGuestTheme} name="PinkTheme"></Button>
            <Button style={{height:20,minWidth:20,backgroundColor:purple[700]}} onClick={handlePickGuestTheme} name="PurpleTheme"></Button>
            <Button style={{height:20,minWidth:20,backgroundColor:deepPurple[700]}} onClick={handlePickGuestTheme} name="DeepPurpleTheme"></Button>
            <Button style={{height:20,minWidth:20,backgroundColor:indigo[700]}} onClick={handlePickGuestTheme} name="IndigoTheme"></Button>
            <Button style={{height:20,minWidth:20,backgroundColor:blue[700]}} onClick={handlePickGuestTheme} name="BlueTheme"></Button>
            <Button style={{height:20,minWidth:20,backgroundColor:lightBlue[700]}} onClick={handlePickGuestTheme} name="LightBlueTheme"></Button>
            <Button style={{height:20,minWidth:20,backgroundColor:cyan[700]}} onClick={handlePickGuestTheme} name="CyanTheme"></Button>
            <Button style={{height:20,minWidth:20,backgroundColor:teal[700]}} onClick={handlePickGuestTheme} name="TealTheme"></Button>
            <Button style={{height:20,minWidth:20,backgroundColor:green[700]}} onClick={handlePickGuestTheme} name="GreenTheme"></Button>
            <Button style={{height:20,minWidth:20,backgroundColor:lightGreen[700]}} onClick={handlePickGuestTheme} name="LightGreenTheme"></Button>
            <Button style={{height:20,minWidth:20,backgroundColor:lime[700]}} onClick={handlePickGuestTheme} name="LimeTheme"></Button>
            <Button style={{height:20,minWidth:20,backgroundColor:yellow[700]}} onClick={handlePickGuestTheme} name="YellowTheme"></Button>
            <Button style={{height:20,minWidth:20,backgroundColor:amber[700]}} onClick={handlePickGuestTheme} name="AmberTheme"></Button>
            <Button style={{height:20,minWidth:20,backgroundColor:orange[700]}} onClick={handlePickGuestTheme} name="OrangeTheme"></Button>
            <Button style={{height:20,minWidth:20,backgroundColor:deepOrange[700]}} onClick={handlePickGuestTheme} name="DeepOrangeTheme"></Button>
            <Button style={{height:20,minWidth:20,backgroundColor:brown[700]}} onClick={handlePickGuestTheme} name="BrownTheme"></Button>
        </ButtonGroupStyledMobile>
    )
}

export default GuestThemePicker