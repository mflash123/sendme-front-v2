// Template:
// import React from 'react'
// import { Paper, Card, Typography, Button, Avatar, Box } from '@material-ui/core'
import Blocks from '../Guest/Blocks/Blocks'
import BlocksAdmin from './BlocksAdmin'
//import Products from './Products'
import ItemAdmin from './ItemAdmin'
import ItemAvatar from './ItemAvatar'
import ItemMenu from './ItemMenu'
import ItemBody from './ItemBody'
//import Textareacontent from './TextAreaContent'


// const Blocks = () => {
//     return(
//         <Paper  elevation={1} style={{width:415}}>
//         <Card style={{padding:15}}>
//             <Box><Button variant="contained" color="primary" style={{width:'100%'}}>Primary</Button></Box>

//         <Box justifyContent="center" display="flex"><Button color="primary" style={{width:'100%'}}>Primary</Button></Box>
//         <Box justifyContent="center" display="flex"><Button variant="outlined" color="primary" style={{width:'100%'}}>Primary</Button></Box>
//         <Box justifyContent="center" display="flex"><Typography>🔥Продажа чего то крутого 🔥😎</Typography></Box>
//         <Box justifyContent="center" display="flex">
//             <Avatar alt="Remy Sharp" src="https://material-ui.com/static/images/avatar/3.jpg" />
//         </Box>
//         <Box justifyContent="center" display="flex">
//             <Avatar style={{width:200,height:200}} alt="Remy Sharp" src="https://material-ui.com/static/images/avatar/3.jpg" />
//         </Box>
//         </Card>
//         </Paper>
//     )
// }
export {
    Blocks,
    BlocksAdmin,
    //Products,
    ItemAdmin,
    ItemAvatar,
    ItemMenu,
    ItemBody
}