import { Card, CardContent, Grid, IconButton, Paper, Typography } from '@material-ui/core';
import DeleteForeverIcon from '@material-ui/icons/DeleteForever';
import React from 'react';
import { PriceContext } from './../../context/priceContext';

export const AdverbBlockAdmin = () => {
    const { setOpen } = React.useContext(PriceContext) as DialogDefaultContextType;

    return (<Grid
        container
        direction="row"
        justify="flex-start"
        alignItems="center"
        style={{ paddingBottom: 15 }}
    >
        <Grid item xs={1}></Grid>
        <Grid item xs={10}>
            <Grid container alignItems="center">
                <Grid item xs={11}>
                    <Paper elevation={0} style={{ background: 'none' }}>
                        <Card elevation={0} style={{ background: 'none', border: '1px dashed black' }}>
                            <CardContent style={{ padding: "1.5rem" }}>
                                <Typography color="textSecondary">Рекламный блок - показывается на всех страницах</Typography>
                            </CardContent>
                        </Card>
                    </Paper>
                </Grid>
                <Grid item xs={1}>
                    <IconButton edge={"end"} onClick={() => setOpen(true)} >
                        <DeleteForeverIcon />
                    </IconButton>

                </Grid>
            </Grid>
        </Grid>
    </Grid>
    )
}
