import React, { useContext } from "react";

import { Menu, MenuItem, Typography } from "@material-ui/core";
import { Edit, BarChart, Delete, PinDrop } from "@material-ui/icons";

import { ApiContext } from "../../context/ApiContext";
import { Link } from "react-router-dom";
import { useTranslation } from 'react-i18next';

const MenuItems = ({ handleDelete, handlePin, value }) => {
  const { t } = useTranslation();
  const { id, type_id, data } = value;

  const Items = () => {
    switch (type_id) {
      case 27:
        return (
          <MenuItem onClick={handlePin}>
            <PinDrop />
            <Typography>
              {JSON.parse(value.data).type === "avatarTop" ||
              JSON.parse(value.data).type === "bannerTop"
                ? "Unpin"
                : "Pin"}
            </Typography>
          </MenuItem>
        );

      case 21:
        return false;
      
      case 29:
          return (
            <MenuItem component={Link} to={"/dashboard/plist/" + id}>
              <Edit />
              <Typography>{t('edit')}</Typography>
            </MenuItem>
          );

      default:
        return (
          <MenuItem component={Link} to={"/dashboard/edit/" + id}>
            <Edit />
            <Typography>{t('edit')}</Typography>
          </MenuItem>
        );
    }
  };

  return (
    <>
      <Items />
      <MenuItem onClick={handleDelete}>
        <Delete /> {t('delete')}
      </MenuItem>
    </>
  );
};

const ItemMenu = ({ anchorEl, handleClose, value }) => {
  const { id, type_id, data } = value;
  const { deleteContent, updateContent, pinContent } = useContext(ApiContext); //only for count

  const handleDelete = () => {
    deleteContent(id).then(() => {
      document.location.reload(true);
    });
  };

  console.log(value);

  const handlePin = () => {
    // let dataUpdate = JSON.parse(data)
    // dataUpdate.type = 'avatarTop'
    // console.log(dataUpdate)
    //updateContent(dataUpdate,id)
    pinContent(id).then(() => {
      document.location.reload(true);
    });
  };

  return (
    <Menu
      anchorEl={anchorEl}
      keepMounted
      open={Boolean(anchorEl)}
      onClose={handleClose}
      variant="menu"
    >
      {}
      {/* {type_id != 27 && (
        <MenuItem component={Link} to={"/dashboard/edit/" + id}>
          <Edit />
          <Typography>{t('edit')}</Typography>
        </MenuItem>
      )}
      {type_id === 27 && (
        <MenuItem onClick={handlePin}>
          <PinDrop />
          <Typography>
            {JSON.parse(value.data).type === "avatarTop" ||
            JSON.parse(value.data).type === "bannerTop"
              ? "Unpin"
              : "Pin"}
          </Typography>
        </MenuItem>
      )} */}

      {/* <MenuItem onClick={handleClose}><BarChart /> Statistics</MenuItem> */}
      <MenuItems
        handleDelete={handleDelete}
        //type_id={type_id}
        handlePin={handlePin}
        value={value}
      />
    </Menu>
  );
};

export default ItemMenu;
