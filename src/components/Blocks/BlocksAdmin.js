import React from "react";

import {
  Grid,
  Avatar,
  Typography,
  Box,
  Button,
  Paper,
  Card,
  CardContent,
  IconButton,
} from "@material-ui/core";
import MoreVertIcon from "@material-ui/icons/MoreVert";
import { Menu } from "@material-ui/icons";
import { Alert, AlertTitle } from "@material-ui/lab/";
import { ItemAdmin } from "./";

/**
 *
 * @param {data:string} props
 * Этот компонент возвращает уже готовый блок. Подбирает блок самостоятельно, исходя из типа
 */

const BlocksAdmin = (props) => {
  const { provided, value } = props;
  console.log(value);

  return (
    <Grid
      container
      direction="row"
      justify="flex-start"
      alignItems="center"
      style={{ paddingBottom: 15 }}
    >
      <Grid item xs={1}>
        <Typography {...provided.dragHandleProps}>
          <Menu />
        </Typography>
      </Grid>
      <Grid item xs={10}>
        <Paper elevation={0}>
          <Card elevation={0}>
            <CardContent style={{ padding: "1.5rem", wordBreak: "break-all" }}>
              <ItemAdmin value={value} />

              {/* {!value.enabled && (
                <Alert severity="error" style={{ marginTop: 5 }}>
                  <AlertTitle>Ошибка</AlertTitle>
                  <Typography noWrap variant="caption" display="block">
                    Этот элемент не входит в Ваш тариф
                  </Typography>
                  <Typography variant="caption" display="block">
                    Перейдите на тариф PRO, чтобы отобразить элемент
                  </Typography>
                </Alert>
              )} */}
            </CardContent>
          </Card>
        </Paper>
      </Grid>
    </Grid>
  );
};

export default BlocksAdmin;
