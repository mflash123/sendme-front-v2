import React from 'react'
import {Instagram,
    WhatsApp,
    Telegram,
    Facebook,
    Link,
    Phone,
    Mail,
    YouTube,
    TextFields,
    AccountCircle,
    Store,
    Map,
    QuestionAnswer,
    SettingsOverscan,
} from '@material-ui/icons';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome'
import { faTiktok } from '@fortawesome/free-brands-svg-icons'


const ItemAvatar = props => {
    const {type} = props
    console.log(type)

    switch (type) {
        case 5:
            //instagram
            return (<Instagram />)
        case 6:
            //whatsapp
            return (<WhatsApp />)
        case 7:
            //telegram
            return (<Telegram />)
        case 8:
            //skype
            return (<i className="fab fa-skype fa-2x"></i>)
        case 9:
            //vk
            return (<i className="fab fa-vk fa-2x"></i>)
        case 10:
            //fb
            return (<Facebook />)
        case 11:
            //site
            return (<Link />)
        case 15:
            //viber
            return (<i className="fab fa-viber fa-2x"></i>)
        case 16:
            //tel
            return (<Phone />)
        case 17:
            //email
            return (<Mail />)
        case 18:
            //buy btn
            return (<i className="fab fa-whatsapp-square fa-2x"></i>)
        case 19:
            //line
            return (<i className="fab fa-line fa-2x"></i>)
        case 20:
            //youtube
            return (<YouTube />)
        case 21:
        case 37:
            //CustomButton
            return (<SettingsOverscan />)
        case 24:
            //ok
            return (<i className="fab fa-odnoklassniki fa-2x"></i>)
        case 26:
        case 39:
            //text
            return (<TextFields />)

        case 27:
            //avatar
            return (<AccountCircle />)
    
        case 29:
            //product
            return (<Store />)

        case 34:
            //yandex map
            return (<Map />)

        case 35:
            //question-ask
            return (<QuestionAnswer />)

        case 36:
                //youtube
                return (<YouTube />)

        case 38:
            //tiktok
            return (<FontAwesomeIcon icon={faTiktok} />)

        default:
            return (
            <>UnategorizedItemAvatar {type}</>
            )
    }


}
ItemAvatar.defaultProps = {
    
}

export default ItemAvatar