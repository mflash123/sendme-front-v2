import React, { useState } from "react";

import {
  Grid,
  Avatar,
  Typography,
  Box,
  Button,
  Paper,
  Card,
  CardContent,
  IconButton,
  Menu,
  MenuItem,
} from "@material-ui/core";
import { MoreVert, PinDrop } from "@material-ui/icons";

import { ItemMenu, ItemAvatar, ItemBody } from "./index";

const ItemAdmin = (props) => {
  const [anchorEl, setAnchorEl] = useState(null);

  const { value } = props;
  console.log(value);
  let pin = false;
  if (value.type_id === 27) {
    pin =
      JSON.parse(value.data).type === "avatarTop" ||
      JSON.parse(value.data).type === "bannerTop";
    //   pin = JSON.parse(value.data).type==='avatarTop'
  }
  console.log(pin);

  const handleClick = (e) => {
    setAnchorEl(e.currentTarget);
  };
  const handleClose = () => {
    setAnchorEl(null);
  };
  return (
    <Grid container alignItems="center">
      <Grid item xs={1} style={{ fontSize: "1rem" }}>
        <ItemAvatar type={value.type_id} />
      </Grid>
      <Grid item xs={10}>
        <ItemBody type={value.type_id} data={value.data} />
      </Grid>
      <Grid item xs={1}>
        {pin && (
          <IconButton edge={"end"}>
            <PinDrop />
          </IconButton>
        )}
        <IconButton edge={"end"} onClick={handleClick}>
          <MoreVert />
        </IconButton>
        <ItemMenu value={value} anchorEl={anchorEl} handleClose={handleClose} />
      </Grid>
    </Grid>
  );
};

export default ItemAdmin;
