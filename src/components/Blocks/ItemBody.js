import React from 'react'
import { Box, Avatar } from '@material-ui/core'
import {DashboardLabel} from './../../styled'
import { convertFromRaw } from "draft-js";



const ItemBody = props => {
    const {type,data} = props
    console.log(data)
    const {name,destination} = JSON.parse(data)


    switch (type) {
        case 21:
            //CustomButton
            return (
                <Box style={{paddingLeft:'1.5rem'}}>
                    <DashboardLabel variant={'h6'}>Конструктор кнопок</DashboardLabel>
                    <DashboardLabel variant={'h6'} color={'textSecondary'} noWrap>{name}</DashboardLabel>
                </Box>  
              )

        case 26:
            //text
            return (
                <Box style={{paddingLeft:'1.5rem'}}>
                    <DashboardLabel variant={'h6'}>Текстовый блок</DashboardLabel>
                    <DashboardLabel variant={'h6'} color={'textSecondary'} noWrap>{name}</DashboardLabel>
                </Box>  
              )
        case 27:
            //avatar
            let {fileName} = JSON.parse(data)
            console.log(data)
            let elementName='';
            if(JSON.parse(data).type==='avatar' || JSON.parse(data).type==='avatarTop' ){
              //  elementName='Avatar'
                return (
                    <Box style={{paddingLeft:'1.5rem'}}>
                        <DashboardLabel variant={'h6'} display={'inline'}>Изображение</DashboardLabel>
                        <Avatar  alt="Remy Sharp" src={process.env.REACT_APP_STORAGE_URL+fileName} />
                    </Box>  
                  )

            } else if ( JSON.parse(data).type==='banner' || JSON.parse(data).type==='bannerTop' ){
                return (
                    <Box style={{paddingLeft:'1.5rem'}}>
                        <DashboardLabel variant={'h6'} display={'inline'}>Баннер</DashboardLabel>
                        <Avatar variant="rounded"  alt="Remy Sharp" src={process.env.REACT_APP_STORAGE_URL+fileName} />
                    </Box>  
                  )

              //  elementName='Banner'
            }
        
        case 29:
            //Product
            return (
                <Box style={{paddingLeft:'1.5rem'}}>
                    <DashboardLabel variant={'h6'}>Товары</DashboardLabel>
                    <DashboardLabel variant={'h6'} color={'textSecondary'} noWrap>Каталог товаров</DashboardLabel>
                </Box>  
                )
        
        case 34:
            //yandex map
            return (
                <Box style={{paddingLeft:'1.5rem'}}>
                    <DashboardLabel variant={'h6'}>Яндекс карта</DashboardLabel>
                </Box>  
                )

        case 35:
            //yandex map
            return (
                <Box style={{paddingLeft:'1.5rem'}}>
                    <DashboardLabel variant={'h6'}>Вопрос - ответ</DashboardLabel>
                </Box>  
                )

        case 36:
            //youtube
            return (
                <Box style={{paddingLeft:'1.5rem'}}>
                    <DashboardLabel variant={'h6'}>Youtube</DashboardLabel>
                </Box>  
                )

        case 39:
            //textDraftjs
            let raw = convertFromRaw(JSON.parse(data)).getPlainText();
            return (
                <Box style={{paddingLeft:'1.5rem'}}>
                    <DashboardLabel variant={'h6'}>Текстовый блок</DashboardLabel>
                    <DashboardLabel variant={'h6'} color={'textSecondary'} noWrap>{raw}</DashboardLabel>
                </Box>  
              )
            
    
        default:
            return (
                <Box style={{paddingLeft:'1.5rem'}}>
                    <DashboardLabel variant={'h6'}>{name}</DashboardLabel>
                    <DashboardLabel variant={'h6'} color={'textSecondary'} noWrap>{destination}</DashboardLabel>
                    {/* Type: {type} */}
                </Box>  
              )
    }


}

export default ItemBody