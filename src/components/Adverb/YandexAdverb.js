import React, { useContext, useEffect, useState } from "react";

const YandexAdverb = () => {
  useEffect(() => {
    function appendYaMetrix() {
      //const yaDiv = document.createElement('div')
      const yaDiv = document.getElementById("yandex_rtb_R-A-694352-2");
      //yaDiv.innerHTML = "<h1>ADVERB</h1>"
      //yaDiv.setAttribute('id', 'yandex_rtb_R-A-694352-2')
      //document.body.appendChild(yaDiv)

      const yaScript = document.createElement("script");
      yaScript.setAttribute("type", "text/javascript");
      yaScript.innerHTML = `(function(w, d, n, s, t) {
            w[n] = w[n] || [];
            w[n].push(function() {
                Ya.Context.AdvManager.render({
                    blockId: "R-A-694352-2",
                    renderTo: "yandex_rtb_R-A-694352-2",
                    horizontalAlign: false,
                    async: true
                });
            });
            t = d.getElementsByTagName("script")[0];
            s = d.createElement("script");
            s.type = "text/javascript";
            s.src = "//an.yandex.ru/system/context.js";
            s.async = true;
            t.parentNode.insertBefore(s, t);
        })(this, this.document, "yandexContextAsyncCallbacks");`;

      document.head.appendChild(yaScript);
    }
    appendYaMetrix();
  }, []);

  return <div id="yandex_rtb_R-A-694352-2"></div>;
};

export default YandexAdverb;
