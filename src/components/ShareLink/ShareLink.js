import React from 'react';
import { Grid, Paper, Card, Typography, Box, TextField, Button, FormHelperText } from '@material-ui/core';
import Alert from '@material-ui/lab/Alert';
import {useStyles} from '../../style'

function ShareLink(props) {
    const {username,active} = props
    const query = window.location.search.substring(1); //GET param for alerts
    const classes = useStyles();
    console.log('sharelink sub index')
    console.log(query)
    return (
            
        <Box py={2}>
           <TextField
           variant="outlined"
           label="Ваша ссылка"
           value={'https://'+username+'.sendme.cc/'}
           disabled
           fullWidth={true}
           />
           <FormHelperText>Ваша публичная ссылка, которую вы можете использовать для клиентов</FormHelperText>
           {query=='success=trial' && <Alert severity="success">Вы подключили пробный период!</Alert>}
           {!active && <Alert severity="warning"><Typography>Проверьте Вашу почту для подтверждения регистрации</Typography></Alert>}
        </Box>
             
        
  
    )    
}

export default ShareLink;