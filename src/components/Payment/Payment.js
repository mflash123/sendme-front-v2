import React, { useContext, useEffect, useState } from 'react';
import {ApiContext} from '../../context/ApiContext'
import { useParams } from 'react-router-dom';
import styled from 'styled-components'
import { LinearProgress, Typography } from '@material-ui/core';
import {Alert,AlertTitle} from '@material-ui/lab';

    const Vertcentral = styled.div`
    margin-top: 20%;
    text-align: center;
    `
    

function Payment() {
    let { tarifId } = useParams();
    const [error,setError] = useState(false)
   const { getPaymentLink } = useContext(ApiContext)


    
    useEffect(()=>{
        getPaymentLink(tarifId)
        .then(value=>{
            console.log(value)
            if(typeof(value)!='undefined'){
                console.log(value.data)
                if(value.data.error===1){
                    window.location.replace('/dashboard/?fail=trial&msg='+value.data.msg)
                } else {
                    window.location.replace(value.data.msg+'?success=trial')
                }
                //window.location.replace(value.data.msg)
            } else {
                window.location.replace('/signin/?fail=trial')
            }
        })
        .catch(reason=>{
            console.log(reason)
        })
    },[])



    return (
    <Vertcentral>
    <LinearProgress />
    {/* {error && }
    {!error && <>Через несколько секунд вы будете перенаправлены на страницу оплаты</>} */}
    </Vertcentral>
    )    
}

export default Payment;