import React, {useState, useContext, useEffect} from 'react';
import { useForm, Controller } from 'react-hook-form';
import { Typography, TextField, Button, Divider, Paper, Box, Grid } from '@material-ui/core';
import {ApiContext} from './../../../context/ApiContext'
import {useStyles} from '../../../style'



const Youtube = ({typeId}) => {
    const classes = useStyles();
    const { register, handleSubmit, watch, errors, control, setValue, reset } = useForm();
    const {storeContent} = useContext(ApiContext)

    const onSubmit = data => {
        console.log(typeId,data)
        storeContent(data,typeId)
        .then( data =>{
            window.location.replace("/dashboard")
        })

     }

    return(
        
        <form onSubmit={handleSubmit(onSubmit)}>
            <Paper elevation={0} className={classes.paperfield}>
                <Typography variant="h4" style={{marginBottom:4}}>Youtube</Typography>
                <Divider style={{marginBottom:30}} />

                <Controller
                as={
                        <TextField
                            error={errors.youtube}
                            autoComplete="off"
                            fullWidth={true}
                            label="Youtube video ID"
                            helperText= { errors.youtube && 'This field is required' }
                            /> 
                }
                control={control}
                name="youtube"
                rules={{ required: true }}
            />

            <Grid container justify="flex-end">
                <Button type="submit">Сохранить</Button>
            </Grid>

            </Paper>
        </form>
       
    )
}

export default Youtube