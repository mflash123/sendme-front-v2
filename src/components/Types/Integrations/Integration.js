import React, {useState, useContext, useEffect} from 'react';
import FacebookPixel from './FacebookPixel'
import GoogleTagManager from './GoogleTagManager'
import YandexMap from './YandexMap'
import {ApiContext} from './../../../context/ApiContext'
import { LinearProgress, Breadcrumbs, Link, Typography } from '@material-ui/core';



const Integration = (props) => {
    const{typeId} = props

    if(typeId==33){
        return (
            <>
            <Breadcrumbs aria-label="breadcrumb">
            <Link href="/dashboard">Управление</Link>
            <Link href="/dashboard/add">Добавить элемент</Link>
            <Link href="/dashboard/add/13">Интеграция</Link>
            <Typography color="textPrimary">Goole Tag Manager</Typography>
            </Breadcrumbs>
            <GoogleTagManager typeId={typeId} />
            </>
            )
    }

    if(typeId==32){
        return (
            <>
            <Breadcrumbs aria-label="breadcrumb">
            <Link href="/dashboard">Управление</Link>
            <Link href="/dashboard/add">Добавить элемент</Link>
            <Link href="/dashboard/add/13">Интеграция</Link>
            <Typography color="textPrimary">Facebook Pixel</Typography>
            </Breadcrumbs>
            <FacebookPixel typeId={typeId}  />
            </>
            )
    } 

    if(typeId==34){
        return (
            <>
            <Breadcrumbs aria-label="breadcrumb">
            <Link href="/dashboard">Управление</Link>
            <Link href="/dashboard/add">Добавить элемент</Link>
            <Link href="/dashboard/add/13">Интеграция</Link>
            <Typography color="textPrimary">Яндекс карта</Typography>
            </Breadcrumbs>
            <YandexMap typeId={typeId}  />
            </>
            )
    } 


 


}

export default Integration