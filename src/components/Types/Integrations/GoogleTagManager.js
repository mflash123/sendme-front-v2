import React, {useState, useContext, useEffect} from 'react';
import { TextField, Typography, Button, FormHelperText, Paper, Divider, Grid } from '@material-ui/core';
import { useForm, Controller } from "react-hook-form";
import {ApiContext} from './../../../context/ApiContext'

import {useStyles} from '../../../style'

const GoogleTagManager = (props) => {
    const classes = useStyles();

    const {typeId} = props
    const [content,setContent] = useState(false)

    //console.log(data)
    const defaultValues = {
        gtm: ''
    }



    const { register, handleSubmit, watch, errors, control, setValue, reset } = useForm({defaultValues});

    // if(edit){
    //     console.log(123123)
    //     reset(JSON.parse(data.data).gtm)
    // }


    
    const {storeContent,updateContent,getContent, getIntegrationbyType} = useContext(ApiContext)

    useEffect(()=>{
        async function fetchData(){
                    const result = await getIntegrationbyType(33)
                    //Object.keys(obj).length === 0 && obj.constructor === Object
                    console.log( Object.keys(result.data).length )
                    //console.log( result.data.length != 'undefined' )
                    if( Object.keys(result.data).length >0 ){
                        console.log(123123)
                        console.log(JSON.parse(result.data.data).gtm)
                        reset(JSON.parse(result.data.data))
                        setContent(result.data)
                        

                    }
                    
                }
                fetchData()
    },[])

    console.log(defaultValues)

    const onSubmit = data => {
       console.log(content)
        if(content){
            console.log('update')
           // console.log(content)
            updateContent(data,content.id)
        } else {
            console.log('save')
            
            storeContent(data,typeId)
            .then( data =>{
                console.log(data)
                //window.location.replace("/dashboard")
            })
        }

    }


    return(
        <form onSubmit={handleSubmit(onSubmit)}>
            <Paper elevation={0} className={classes.paperfield}>
            <Typography variant="h4" style={{marginBottom:4}}>Google Tag Manager</Typography>
            <Divider style={{marginBottom:30}} />
            <Controller
                as={
                        <TextField
                            error={errors.gtm}
                            autoComplete="off"
                            // inputRef={ register({ required: true })} 
                            fullWidth={true}
                            // name="gtm"
                            label="Google Tag Manager Идентификатор"
                            helperText= { errors.gtm && 'This field is required' }
                            /> 
                }
                control={control}
                name="gtm"
                rules={{ required: true }}
            />

            <Grid container justify="flex-end">
                <Button type="submit">Сохранить</Button>
            </Grid>
            </Paper>
        </form>
    )
}

export default GoogleTagManager