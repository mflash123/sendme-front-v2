import React, {useState, useContext, useEffect} from 'react';
import { useForm, Controller } from 'react-hook-form';
import { Typography, TextField, Button, Divider, Paper, Box, Grid } from '@material-ui/core';
import {ApiContext} from './../../../context/ApiContext'

//import styled from 'styled-components'
import {useStyles} from '../../../style'


const FacebookPixel = (props) => {
    const classes = useStyles();

    const {typeId} = props
    const [content,setContent] = useState(false)


    //console.log(classes.border)
    const defaultValues = {
        fbpixel: ''
    }


    const { register, handleSubmit, watch, errors, control, setValue, reset } = useForm({defaultValues});

    // if(typeof(data.data)!='undefined'){
    //     console.log(JSON.parse(data.data))
    //     //gtm=JSON.parse(data.data).gtm
    //     //reset(JSON.parse(data.data))
    // }


    
    const {storeContent,updateContent,getContent,getIntegrationbyType} = useContext(ApiContext)

    useEffect(()=>{
        async function fetchData(){
                    const result = await getIntegrationbyType(32)
                    //Object.keys(obj).length === 0 && obj.constructor === Object
                    console.log( Object.keys(result.data).length )
                    //console.log( result.data.length != 'undefined' )
                    if( Object.keys(result.data).length >0 ){
                        console.log(123123)
                        console.log(JSON.parse(result.data.data).gtm)
                        reset(JSON.parse(result.data.data))
                        setContent(result.data)

                    }
                    
                }
                fetchData()
    },[])


    const onSubmit = data => {
        console.log(content)
         if(content){
             console.log('update')
            // console.log(content)
             updateContent(data,content.id)
         } else {
             console.log('save')
             
             storeContent(data,typeId)
             .then( data =>{
                 console.log(data)
                 //window.location.replace("/dashboard")
             })
         }
 
     }

    

    return(
        
        <form onSubmit={handleSubmit(onSubmit)}>
            <Paper elevation={0} className={classes.paperfield}>
                <Typography variant="h4" style={{marginBottom:4}}>Facebook pixel</Typography>
                <Divider style={{marginBottom:30}} />

                <Controller
                as={
                        <TextField
                            error={errors.fbpixel}
                            autoComplete="off"
                            fullWidth={true}
                            label="Facebook pixel"
                            helperText= { errors.fbpixel && 'This field is required' }
                            /> 
                }
                control={control}
                name="fbpixel"
                rules={{ required: true }}
            />

            <Grid container justify="flex-end">
                <Button type="submit">Сохранить</Button>
            </Grid>

                {/* <TextField
                autoComplete="off"
                inputRef={ register({ required: true })} 
                fullWidth={true}
                name="fbpixel"
                label="Facebook pixel"
                variant="outlined"
                />
                <Button type="submit" variant="contained" color="primary" style={{marginTop:10}}>Сохранить</Button> */}
            </Paper>
        </form>
       
    )
}

export default FacebookPixel