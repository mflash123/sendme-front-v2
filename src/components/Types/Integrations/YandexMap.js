import React, {useState, useContext, useEffect, useRef} from 'react';
import { useForm, Controller } from 'react-hook-form';
import { Typography, TextField, Button, Divider, Paper, Box, Grid } from '@material-ui/core';
import {ApiContext} from '../../../context/ApiContext'
import IconButton from '@material-ui/core/IconButton';
import {HelpOutline} from '@material-ui/icons';
import YouTube from 'react-youtube-embed'
import Alert from '@material-ui/lab/Alert';

//import styled from 'styled-components'
import {useStyles} from '../../../style'




const YandexMap = (props) => {
  const DEFAULT_ADDRESS = { lat: 55.74, lng: 37.64 };

  /** dfsdf */
  const classes = useStyles();

  const {typeId} = props
  const [content,setContent] = useState(false)

  const { register, handleSubmit, watch, errors, control, setValue, reset } = useForm();
  const {storeContent,updateContent,getContent,getIntegrationbyType} = useContext(ApiContext)


  const [shownCoords, setShownCoords] = useState(DEFAULT_ADDRESS);
  const [isMapInitialized, setIsMapInitialized] = useState(false);
  const [loading,setLoading] = useState(true)
  //const mapRef = useRef();
  
  //reset(shownCoords)

  // console.log(shownCoords)
  // console.log(!loading)
  // console.log(!isMapInitialized)
  useEffect(() => {

    async function fetchData(){
      const result = await getIntegrationbyType(34)
      //Object.keys(obj).length === 0 && obj.constructor === Object
      console.log( Object.keys(result.data).length )
      //console.log( result.data.length != 'undefined' )
      if( Object.keys(result.data).length >0 ){
        
       //   console.log(123123)
          console.log(result.data.data)
          // reset(JSON.parse(result.data.data))
          setContent(result.data)
          reset(JSON.parse(result.data.data))
          setShownCoords(JSON.parse(result.data.data))
        //  setShownCoords({ lat: 65.74, lng: 37.64 })
     
      }
      setLoading(false)
    }
    fetchData()

    if ( !isMapInitialized && !loading ) {
      console.log(1)
      function init() {
        let myPlacemark;
        const myMap = new window.ymaps.Map("map", {
         // center: [55.76, 37.64],
         center: [shownCoords.lat, shownCoords.lng],
          zoom: 10,
          controls: []
        });

        // Creating an instance of the ymaps.control.SearchControl class.
        const mySearchControl = new window.ymaps.control.SearchControl({
          options: {
            noPlacemark: true
          }
        });
        myMap.controls.add(mySearchControl);

        // Putting the selected result in the collection.
        mySearchControl.events.add("resultselect", function (e) {
          const index = e.get("index");
          mySearchControl.getResult(index).then(function (res) {
            const [lat, lng] = res.geometry._coordinates;
            setShownCoords({ lat, lng });
            reset({ lat, lng })
          });
        });

        myMap.controls.remove("trafficControl");
        myMap.controls.remove("zoomControl");

        myMap.events.add("click", (e) => {
          const coords = e.get("coords");

          setShownCoords({ lat: coords[0], lng: coords[1] });
          reset({ lat: coords[0], lng: coords[1] })

          if (myPlacemark) {
            myPlacemark.geometry.setCoordinates(coords);
          } else {
            myPlacemark = createPlacemark(coords);
            myMap.geoObjects.add(myPlacemark);

            myPlacemark.events.add("dragend", function () {
              getAddress(myPlacemark.geometry.getCoordinates());
            });
          }

          getAddress(coords);
        });

        const createPlacemark = (coords) => {
          return new window.ymaps.Placemark(
            coords,
            {
              iconCaption: "поиск..."
            },
            {
              preset: "islands#violetDotIconWithCaption",
              draggable: true
            }
          );
        };

        const getAddress = (coords) => {
          myPlacemark.properties.set("iconCaption", "поиск...");
          window.ymaps.geocode(coords).then(function (res) {
            const firstGeoObject = res.geoObjects.get(0);

            myPlacemark.properties.set({
              iconCaption: [
                firstGeoObject.getLocalities().length
                  ? firstGeoObject.getLocalities()
                  : firstGeoObject.getAdministrativeAreas(),
                firstGeoObject.getThoroughfare() || firstGeoObject.getPremise()
              ]
                .filter(Boolean)
                .join(", "),
              balloonContent: firstGeoObject.getAddressLine()
            });
          });
        };

        myPlacemark = createPlacemark([
          shownCoords.lat,
          shownCoords.lng
        ]);
        myMap.geoObjects.add(myPlacemark);

        getAddress(myPlacemark.geometry.getCoordinates());
        setIsMapInitialized(true);
      }

      reset(shownCoords)


      const yandexMapScript = document.createElement("script");
      yandexMapScript.src = `https://api-maps.yandex.ru/2.1/?apikey=33a91144-de50-49d3-8a96-7d519c459e97&lang=ru_RU`;
      yandexMapScript.async = false;
      window.document.body.appendChild(yandexMapScript);
      yandexMapScript.addEventListener("load", () => {
        window.ymaps.ready(init);
      });
    }
  },[loading]);


    const onSubmit = data => {
        console.log(data)
         if(content){
             console.log('update')
            // console.log(content)
             updateContent(data,content.id)
         } else {
             console.log('save')
             
             storeContent(data,typeId)
             .then( data =>{
                 console.log(data)
                 //window.location.replace("/dashboard")
             })
         }
 
     }

    

    return(
        
        <form onSubmit={handleSubmit(onSubmit)}>
            <Paper elevation={0} className={classes.paperfield}>
            

                <Typography variant="h4" style={{marginBottom:4}}>Яндекс карта
                </Typography>
            
                <Divider style={{marginBottom:30}} />

                <div id="map" style={{ width: "100%", height: "400px" }}></div>

                {/* <input  ref={register} name="ya_map" rules={{ required: true }} /> */}
                <input hidden  ref={register} name="lat" rules={{ required: true }} />
                <input hidden  ref={register} name="lng" rules={{ required: true }} />
                {/* <input  ref={register} name="ya_map" rules={{ required: true }} value={'['+shownCoords.lat+','+shownCoords.lng+']'} /> */}


            <Grid container justify="flex-end">
                <Button type="submit">Сохранить</Button>
            </Grid>

            </Paper>
            
        </form>
       
    )
}

export default YandexMap