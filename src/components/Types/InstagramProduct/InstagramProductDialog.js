import React from 'react'
import { Button, Dialog, DialogTitle, DialogContent, DialogContentText, DialogActions } from '@material-ui/core'
import InstagramIcon from '@material-ui/icons/Instagram';



const InstagramProductDialog = () => {
return(
<Dialog
        open={true}
        //onClose={handleClose}
      >
        <DialogTitle id="alert-dialog-title">{"Use Google's location service?"}</DialogTitle>
        <DialogContent>
          <DialogContentText id="alert-dialog-description">
            Let Google help apps determine location. This means sending anonymous location data to
            Google, even when no apps are running.
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button color="primary">
            Disagree
          </Button>
          <Button color="primary" autoFocus>
            Agree
          </Button>
        </DialogActions>
      </Dialog>
)
}

export default InstagramProductDialog
