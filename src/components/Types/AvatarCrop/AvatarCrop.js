import React, { useState, useCallback, useRef, useContext, useEffect } from 'react';
import ReactCrop from 'react-image-crop';
import 'react-image-crop/dist/ReactCrop.css';
import { Box,Dialog, Button, DialogContent, DialogTitle, DialogContentText } from '@material-ui/core';

import {ApiContext} from '../../../context/ApiContext'

const AvatarCrop = (props) => {
  const [upImg, setUpImg] = useState();
  const imgRef = useRef(null);
  const [crop, setCrop] = useState({
    unit: '%',
  //  width: 25,    
  //  aspect: 1
  });
  //{ unit: 'px',width:415, minWidth: '415px',maxWidth:415,keepSelection:true, }
  const [previewUrl, setPreviewUrl] = useState();
  const [blobTemp,setBlobTemp] = useState();

  const [circle,setCircle] = useState(true)
  //const {handleCLick} = props

  const {storeContent} = useContext(ApiContext)


  const onSelectFile = e => {
    if (e.target.files && e.target.files.length > 0) {
      const reader = new FileReader();
      reader.addEventListener('load', () => setUpImg(reader.result));
      reader.readAsDataURL(e.target.files[0]);
    }
  };

  const onLoad = useCallback(img => {
    imgRef.current = img;
  }, []);

  const makeClientCrop = async crop => {
    if (imgRef.current && crop.width && crop.height) {
      createCropPreview(imgRef.current, crop, 'newFile.jpeg');
    }
  };

  const createCropPreview = async (image, crop, fileName) => {
    const canvas = document.createElement('canvas');
    const scaleX = image.naturalWidth / image.width;
    const scaleY = image.naturalHeight / image.height;
    canvas.width = crop.width;
    canvas.height = crop.height;
    const ctx = canvas.getContext('2d');

    ctx.drawImage(
      image,
      crop.x * scaleX,
      crop.y * scaleY,
      crop.width * scaleX,
      crop.height * scaleY,
      0,
      0,
      crop.width,
      crop.height
    );

    return new Promise((resolve, reject) => {
      canvas.toBlob(blob => {
        if (!blob) {
          reject(new Error('Canvas is empty'));
          return;
        }
        blob.name = fileName;
        window.URL.revokeObjectURL(previewUrl);

        // let reader = new FileReader();
        // reader.readAsDataURL(blob); 
//console.log(reader.result)
setBlobTemp(blob)
//console.log(blob)

        setPreviewUrl(window.URL.createObjectURL(blob));
      }, 'image/jpeg',1);
    });
  };

  const blob2Base = () => {
    let reader = new FileReader();
    reader.readAsDataURL(blobTemp); 
    console.log(reader)

    reader.onload = function() {
      console.log(reader.result); // url с данными
      let type = circle ? 'avatar' : 'banner'
      storeContent({name:reader.result,type:type},27)
      .then( value =>{
        console.log(value)
        window.location.replace("/dashboard")
    })
    };

 //console.log(blobTemp)
 //storeContent({name:reader.result,type:'avatarTop'},27)


  }




  useEffect(()=>{
    /** Aspect changer */
    const data = () =>{
      circle ?
       setCrop({
        unit: 'px',
        width: 160,    
        aspect: 1
      })
      :
        setCrop({
        //   unit: 'px',
        //   width: 160, 
        //  height: 50,   
        })
    }
    data()
},[circle])

  return (
      <Dialog
      open={true}
      fullWidth={true}
      >
        <DialogTitle>Добавить изображение</DialogTitle>
        <DialogContent>
        <DialogContentText>
            Выберите тип изображения.
          </DialogContentText>

        
    <Box justifyContent="center" item>

            <Button component="label" style={{width:'100%'}} variant="contained" disableElevation>
              Загрузить изображение
                <input type="file" accept="image/*" onChange={onSelectFile} style={{display:'none'}}/>
            </Button>
            

      <ReactCrop
        src={upImg}
        onImageLoaded={onLoad}
        crop={crop}
        onChange={c => setCrop(c)}
        onComplete={makeClientCrop}
        circularCrop={circle}
        //maxWidth={415}
        minWidth={50}
       // maxHeight={415}
        minHeight={50}
        //locked={circle}

      />

{ previewUrl && (
        <>
      <Button component="label" onClick={()=>setCircle(true)} variant="contained" disableElevation style={{width:'50%'}}>
      Круглое изображение
      </Button>
      <Button component="label" onClick={()=>setCircle(false)} variant="contained" disableElevation style={{width:'50%'}}>
      Квадратное изображение
      </Button>
      </>
      )
      }

      {previewUrl && circle && (<center><img style={{ maxWidth: '100%',borderRadius: '100%',border: '1px solid black' }} alt="Crop preview" src={previewUrl} /></center>)}
      {previewUrl && !circle && (<center><img alt="Crop preview" src={previewUrl} /></center>)}
    </Box>
    {/* <Button component="label" onClick={()=>{storeContent({name:blobTemp,type:'avatarTop'},27)}}>Save</Button> */}


    {previewUrl && <Button component="label" onClick={blob2Base} variant="contained" disableElevation>Сохранить</Button>}
    </DialogContent>
    </Dialog>
  );
}

export default AvatarCrop