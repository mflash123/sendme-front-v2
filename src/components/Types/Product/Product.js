import React, { useState, useContext, useEffect } from "react";
import {
  Button,
  TextField,
  Select,
  MenuItem,
  InputLabel,
  Grid,
  Paper,
  Typography,
  Breadcrumbs,
  FormControlLabel,
  Switch,
  FormControl,
  LinearProgress,
  Tabs,
  Tab,
} from "@material-ui/core";
import { Alert } from "@material-ui/lab";
import { useForm, Controller } from "react-hook-form";
import { ProductUpload } from "./";

import { ApiContext } from "../../../context/ApiContext";
import { useStyles } from "./../../../style";
import { SwitchForm } from "./../../../styled";
import BackupIcon from "@material-ui/icons/Backup";
import { Link, useParams, useHistory } from "react-router-dom";

//const ProductActions = React.lazy(() => import("./ProductActions")); React lazy flickering view =(
import ProductActions from "./ProductActions";

const currencies = [
  {
    value: "USD",
    label: "$",
  },
  {
    value: "RUB",
    label: "₽",
  },
  {
    value: "EUR",
    label: "€",
  },
  {
    value: "BTC",
    label: "฿",
  },
  {
    value: "JPY",
    label: "¥",
  },
];

/**
 * 
 * @param {*} param0 
 * Пока отключил, потому что совсем коряво все работает. Не захотел разбираться
 */
const Product = ({ edit }) => {
  const items = false; //tmp
  const { productId } = useParams();
  const history = useHistory();

  const defaultValues = {
    name: null,
    description: null,
    action: {
      type: 1,
      url: null,
    },
    currency: "USD",
    price: null,
    products_ava: null,
  };

  const classes = useStyles();
  const [loading, setLoading] = useState(true);
  const [imgArrFiles, setImgArrFiles] = useState([]);
  const [priceProduct, setPriceProduct] = useState(false);
  const [actionProduct, setActionroduct] = useState(false);
  const [imageError, setImageError] = useState(false);
  const { storeProduct, updateProduct, getProduct } = useContext(ApiContext);
  const {
    register,
    handleSubmit,
    watch,
    control,
    setValue,
    getValues,
    formState: { errors },
  } = useForm({
    defaultValues,
    shouldUnregister: false, //магия для редактирования
  });

  //console.log(defaultValues);
  console.log(items);
  useEffect(() => {
    if (edit) {
      getProduct(productId)
        .then(({ data }) => {
          setValue("name", data.data.name);
          setValue("description", data.data.description);
          if (typeof data.data.action !== "undefined") {
            setActionroduct(true);
            setValue("action.type", data.data.action.type);
            if (data.data.action.type === 1) {
              //  setActionroduct(true);
              //setValue("action.type", data.data.action.type);
              setValue("action.url", data.data.action.url);
            }
            if (data.data.action.type === 5) {
              setValue("action.button", data.data.action.button);
            }
          }
          if (typeof data.data.price !== "undefined") {
            console.log(data.data.price);
            setPriceProduct(true);
            // setValue("price", data.data.price);
            setValue("price", "123");
            setValue("currency", data.data.currency);
          }
          setImgArrFiles(data.data.attach.images);

          console.log(data);
        })
        .catch((error) => console.log(error));
    }
    setLoading(false);
  }, []);

  const onSubmit = (data) => {
    data.images = imgArrFiles;
    console.log(data);
    // console.log(data);

    if (!priceProduct) {
      delete data.price;
      delete data.currency;
    }
    if (!actionProduct) {
      delete data.action;
    } else {
      data.action.type === 1 && delete data.action.button;
      data.action.type === 5 && delete data.action.url;
    }

    if (edit) {
      data.type = "product";
      //updateProduct({ data, type: "product" }, props.items.id);
      /** form kostil because of shouldUnregister */
      console.log(data);
      updateProduct(data, productId)
        .then(() => {
          console.log("ok");
        })
        .catch((error) => {
          console.log("bad", error);
        });
    } else {
      storeProduct(data)
        .then(() => {
          console.log("ok");
        })
        .catch((error) => {
          setImageError(error.response.data.message);
          console.log(error.response.data.message);
        });
    }
   // window.location.replace("/dashboard?result=success");
  };

  if (loading) {
    return <LinearProgress />;
  }

  console.log(loading);

  return (
    <>
      <form onSubmit={handleSubmit(onSubmit)}>
        {/* {error && <Alert severity="error">{error}</Alert>} */}

        {/* register your input into the hook by invoking the "register" function */}

        <Paper
          elevation={0}
          className={classes.paperfield}
          style={{ paddingTop: 0 }}
        >
          {imageError && <Alert severity="error">Images required</Alert>}

          <ProductUpload
            imgArrFiles={imgArrFiles}
            setImgArrFiles={setImgArrFiles}
          />

          <Controller
            control={control}
            name="name"
            label="Название товара"
            rules={{ required: true }}
            render={({ field }) => (
              <TextField
                {...field}
                fullWidth={true}
                error={errors?.name}
                label="Название товара"
                helperText={Boolean(errors?.name) && "Укажите название товара"}
              />
            )}
          />

          <Controller
            control={control}
            name="description"
            label="Описание товара"
            rules={{ required: true }}
            render={({ field }) => (
              <TextField
                {...field}
                fullWidth={true}
                error={errors?.description}
                label="Описание товара"
                helperText={Boolean(errors?.description) && "Укажите описание товара"}
              />
            )}
          />

          {/* <TextField
            inputRef={register({
              required: true,
            })}
            error={errors.name}
            fullWidth={true}
            name="name"
            label="Название товара"
            autoComplete="off"
            defaultValue={defaultValues.name}
            helperText={errors.name && "Укажите название товара"}
          />

          <TextField
            inputRef={register({
              required: true,
            })}
            error={errors.description}
            name="description"
            label="Описание товара"
            multiline
            rows={5}
            fullWidth={true}
            helperText={errors.name && "Укажите описание товара"}
          /> */}

          <SwitchForm
            control={
              <Switch
                checked={priceProduct}
                onChange={() => {
                  setPriceProduct(!priceProduct);
                }}
                //name="price_product"
                color="primary"
              />
            }
            label="Цена товара"
          />

          <Grid container direction="row" spacing={3}>
            {priceProduct && (
              <>
                <Grid item xs={6}>
                  <TextField
                    error={errors.price}
                    type="number"
                    inputRef={register({ required: true })}
                    fullWidth={true}
                    name="price"
                    label="Цена продукта"
                    helperText={
                      errors.price && "Укажите цену или выключите атрибут"
                    }
                  />
                  {/* <TextField inputRef={ register({ required: true })} fullWidth={true} name="action" label="Название кнопки" /> */}
                </Grid>
                <Grid item xs={6}>
                  <TextField
                    error={errors.currency}
                    name="currency"
                    inputRef={register({ required: true })}
                    select
                    label="Валюта"
                    SelectProps={{
                      native: true,
                    }}
                  >
                    {currencies.map((option) => (
                      <option key={option.value} value={option.value}>
                        {option.label}
                      </option>
                    ))}
                  </TextField>
                </Grid>
              </>
            )}
          </Grid>

          <SwitchForm
            control={
              <Switch
                checked={actionProduct}
                onChange={() => {
                  setActionroduct(!actionProduct);
                }}
                //name="action_product"
                color="primary"
              />
            }
            label="Действия товара"
          />
          <Grid container direction="row" spacing={3}>
            {actionProduct && (
              <ProductActions
                control={control}
                errors={errors}
                register={register}
                setValue={setValue}
                getValues={getValues}
                defaultValues={defaultValues}
                watch={watch}
              />
            )}
          </Grid>

          <Grid container justify="flex-end">
            <Button
              component={Link}
              onClick={() => history.goBack()}
              color="primary"
            >
              Назад
            </Button>
            <Button color="primary" type="submit">
              {!edit ? "Сохранить" : "Изменить"}
            </Button>
          </Grid>
        </Paper>
      </form>
    </>
  );
};

export default Product;
