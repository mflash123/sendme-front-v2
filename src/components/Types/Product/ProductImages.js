import React, { useState } from "react";
import { IconButton } from "@material-ui/core";
import Image from "material-ui-image";
import DeleteIcon from "@material-ui/icons/Delete";
import uuid from "react-uuid";

const ProductImages = ({ blobs, removeBlob, width, height }) => {
  console.log(blobs)
  const handleRemove = (index) => {
    const tmp = [...blobs];
    removeBlob(tmp.filter( (v,i) => {return i!=index}))
  };
  const ImageFiles = () =>
    blobs.map((val, index) => (
      <IconButton
        key={uuid()}
        component="label"
        style={{ width: width, height: height, marginRight: 5, padding: 0 }}
        variant="contained"
        onClick={() => handleRemove(index)}
      >
        <DeleteIcon
          style={{
            borderRadius: 5,
            position: "absolute",
            zIndex: 1,
            color: "black",
            backgroundColor: "#e0e0e0",
          }}
        />
        <Image
          src={process.env.REACT_APP_API_URL + val}
          style={{ width: width, height: height }}
        />
      </IconButton>
    ));
  return <ImageFiles />;
};

const arePropsEqual = (prevProps, nextProps) => {
  if (prevProps.blobs === nextProps.blobs) {
    return true;
  }
};

export default React.memo(ProductImages, arePropsEqual);
