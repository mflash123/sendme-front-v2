import React, { Component, useState, useContext } from "react";
import { render } from "react-dom";
import {
  SortableContainer,
  SortableElement,
  sortableHandle,
} from "react-sortable-hoc";
import arrayMove from "array-move";
import {
  List,
  ListItem,
  ListItemText,
  Card,
  Button,
  Grid,
  IconButton,
  Menu,
  MenuItem,
  Paper,
  Typography,
  CardContent,
  Fab,
} from "@material-ui/core";
import {Link} from 'react-router-dom'
import { ApiContext } from "../../../context/ApiContext";
import { MoreVert } from "@material-ui/icons";

import MenuIcon from "@material-ui/icons/Menu";

import {FadeIn} from './../../../styled'

const DragHandle = sortableHandle(() => (
  <IconButton>
    <MenuIcon />
  </IconButton>
));

const SortableItem = SortableElement(({ id, value, onDelete }) => {
  const [anchorEl, setAnchorEl] = useState(null);

  const handleClick = (e) => {
    setAnchorEl(e.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  return (
    <Grid
      container
      direction="row"
      justify="flex-start"
      alignItems="center"
      style={{ paddingBottom: 15 }}
      component="div"
    >
      <Grid item xs={1}>
        <Typography>
          <DragHandle />
        </Typography>
      </Grid>
      <Grid item xs={9}>
        <Paper elevation={0}>
          <Card elevation={0}>
            <CardContent style={{ padding: "1.5rem", wordBreak: "break-all" }}>
              <ListItem disableGutters={true}>
                <ListItemText inset primary={value} />
                <IconButton edge={"end"} onClick={handleClick}>
                  <MoreVert />
                </IconButton>
              </ListItem>

              <Menu
                anchorEl={anchorEl}
                keepMounted
                open={Boolean(anchorEl)}
                onClose={handleClose}
              >
                <MenuItem component={Link} to={`/dashboard/product/${id}`}>
                  Редактировать
                </MenuItem>
                <MenuItem
                  onClick={() => {
                    onDelete(id);
                  }}
                >
                  Удалить
                </MenuItem>
              </Menu>
            </CardContent>
          </Card>
        </Paper>
      </Grid>
      <Grid item xs={1}></Grid>
    </Grid>
  );
});

const SortableList = SortableContainer(({ items, onDelete }) => {
  console.log(items)
  return (
    <ul style={{ padding: 0 }}>
      {items.map((value, index) => (
        <SortableItem
          onDelete={onDelete}
          id={value.id}
          key={`item-${value.id}`}
          index={index}
          value={
            JSON.parse(value.data).name +
            " | " +
            JSON.parse(value.data).description
          }
        />
      ))}
    </ul>
  );
});

const SortableComponent = (props) => {
  const { updateProductWeight, removeProduct } = useContext(ApiContext);

  const [items, setItems] = useState(props.items);
  const onSortEnd = ({ oldIndex, newIndex }) => {
    // console.log('old='+oldIndex)
    // console.log('new='+newIndex)
    // console.log(arrayMove(items,oldIndex, newIndex))
    setItems(arrayMove(items, oldIndex, newIndex));
  };

  function removeItems(id) {
    console.log(id);
    //  let items_tmp = items;
    let items_tmp = items.filter((key, index) => {
      if (key.id != id) return key;
    });
    //API REMOVE PRODUCT
    // setItems(items_tmp)
    // console.log(items_tmp)
    removeProduct(id).then(setItems(items_tmp));
  }

  updateProductWeight(items);

  //
  console.log(items);
  return (
    <FadeIn in={true}>
    <SortableList
      items={items}
      onSortEnd={onSortEnd}
      onDelete={removeItems}
      lockAxis="y"
      useDragHandle={true}
    />
    </FadeIn>
  );
};

export default SortableComponent;
