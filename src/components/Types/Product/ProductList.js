import React, { useEffect, useContext, useState } from "react";
import Sortable from "./Sortable";
import {
  Button,
  Breadcrumbs,
  Typography,
  Box,
  Fab,
  Fade,
} from "@material-ui/core";
import AddIcon from "@material-ui/icons/Add";
import {Link, useParams} from 'react-router-dom'
import { ApiContext } from "./../../../context/ApiContext";



const ProductList = () => {
  const {contentId} = useParams()
  const { getProducts } = useContext(ApiContext);
  const [loading,setLoading] = useState(true)
  const [items,setItems] = useState(false)
  useEffect(()=>{
    getProducts(contentId)
    .then(({data})=>{
      setItems(data)
      setLoading(false)
    })
    .catch(error=>{console.log(error)})
  },[contentId])


  return (
    <>
      {!loading && <Sortable items={items} />}
      <center>
        <Fab component={Link} color="primary" to="/dashboard/product">
          <AddIcon />
        </Fab>
      </center>
    </>
  );
};

export default ProductList;
