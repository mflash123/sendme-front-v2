import React, {
  useState,
  useCallback,
  useRef,
  useContext,
  useEffect,
} from "react";
import ReactCrop from "react-image-crop";
import "react-image-crop/dist/ReactCrop.css";
import {
  Box,
  Dialog,
  Button,
  Grid,
  Typography,
  DialogActions,
  DialogTitle,
  DialogContent,
  IconButton,
  Badge,
} from "@material-ui/core";
import AddAPhotoIcon from "@material-ui/icons/AddAPhoto";
import InstagramIcon from "@material-ui/icons/Instagram";

import { ApiContext } from "../../../context/ApiContext";
import Image from "material-ui-image";
import DeleteIcon from "@material-ui/icons/Delete";
import InstagramProductDialog from "./../InstagramProduct/InstagramProductDialog";
import ProductImages from "./ProductImages";
import { Alert } from "@material-ui/lab";

const ProductUpload = ({ imgArrFiles, setImgArrFiles }) => {
  console.log(imgArrFiles);

  const [dialog, setDialog] = useState(false);
  const [upImg, setUpImg] = useState();
  const [instagramProduct, setInstagramProduct] = useState(false);
  const [blobTemp, setBlobTemp] = useState([]);

  //const [imgArrFiles,setImgArrFiles] = useState([])

  const imgRef = useRef(null);
  const [crop, setCrop] = useState({
    unit: "px",
    width: 500,
    aspect: 1 / 1,
  });

  const [previewUrl, setPreviewUrl] = useState();

  const { storeProduct } = useContext(ApiContext);

  const onSelectFile = (e) => {
    setDialog(true);
    if (e.target.files && e.target.files.length > 0) {
      const reader = new FileReader();
      reader.addEventListener("load", () => setUpImg(reader.result));
      reader.readAsDataURL(e.target.files[0]);
    }
  };

  const onLoad = useCallback((img) => {
    imgRef.current = img;
  }, []);

  const makeClientCrop = async (crop) => {
    if (imgRef.current && crop.width && crop.height) {
      createCropPreview(imgRef.current, crop, "newFile.jpeg");
    }
  };

  const createCropPreview = async (image, crop, fileName) => {
    const canvas = document.createElement("canvas");
    const scaleX = image.naturalWidth / image.width;
    const scaleY = image.naturalHeight / image.height;
    // canvas.width = crop.width;
    // canvas.height = crop.height;
    canvas.width = Math.ceil(crop.width * scaleX);
    canvas.height = Math.ceil(crop.height * scaleY);
    const ctx = canvas.getContext("2d");

    ctx.drawImage(
      image,
      crop.x * scaleX,
      crop.y * scaleY,
      crop.width * scaleX,
      crop.height * scaleY,
      0,
      0,
      // crop.width,
      // crop.height,
      crop.width * scaleX,
      crop.height * scaleY
    );

    return new Promise((resolve, reject) => {
      canvas.toBlob(
        (blob) => {
          if (!blob) {
            reject(new Error("Canvas is empty"));
            return;
          }

          blob.name = fileName;
          window.URL.revokeObjectURL(previewUrl);
          console.log(blob);
          //setBlobTemp([...blobTemp, blob]);
          setBlobTemp(blob);

          setPreviewUrl(window.URL.createObjectURL(blob));
        },
        "image/png",
        0.95
      );
    });
  };

  const blob2Base = () => {
    let reader = new FileReader();
    reader.readAsDataURL(blobTemp);
    console.log(reader);

    reader.onload = function () {
      storeProduct({ base64: reader.result, type: "product_preview" }).then(
        (value) => {
          console.log(value.base64);
          setImgArrFiles(imgArrFiles.concat(value.data)); //это нужно для multiple imgs. Пока я сделал по 1картинке
          //setImgArrFiles( [value.data] )
        }
      );
      setDialog(false);
    };
  };

  console.log(imgArrFiles); //здесь список файлов
  //0: "aa74b74a060a5ac8e9a65241a0a43ac3.png"
  //1: "fbb0a287c8123a59203adf6a5b03cbcf.png"

  return (
    <>
      <ProductImages width={100} height={100} blobs={imgArrFiles} removeBlob={setImgArrFiles} />
      {imgArrFiles.length <= 3 && (
        <Button
          component="label"
          style={{ width: 100, height: 100, marginRight: 5 }}
          variant="contained"
          disableElevation
        >
          <AddAPhotoIcon />
          <input
            name="products_ava"
            type="file"
            accept="image/*"
            onChange={onSelectFile}
            style={{ display: "none" }}
          />
        </Button>
        
      )}
      

      {/* <Button component="label" style={{width:100,height:100}} variant="contained" onClick={()=>setInstagramProduct(true)} disableElevation>
      <InstagramIcon />
    </Button>
    {instagramProduct && <InstagramProductDialog />} */}

      <Dialog onClose={() => setDialog(false)} open={dialog} fullWidth={true}>
        <DialogTitle>Добавить изображение</DialogTitle>
        <DialogContent>
          <Grid container>
            <Grid item xs={6}>
              <Box justifyContent="center" item>


                <ReactCrop
                  src={upImg}
                  onImageLoaded={onLoad}
                  crop={crop}
                  onChange={(c) => setCrop(c)}
                  onComplete={makeClientCrop}
                  // circularCrop={circle}
                  maxWidth={415}
                  minWidth={50}
                  // maxHeight={415}
                  minHeight={50}
                  //locked={circle}
                />
              </Box>
              {/* {previewUrl && <Button component="label" onClick={blob2Base} variant="contained" disableElevation>Сохранить</Button>} */}
            </Grid>
            <Grid item xs={6}>
              {previewUrl && (
                <center>
                  <img
                    style={{ width: "100%", height: "100%" }}
                    src={previewUrl}
                  />
                </center>
              )}
            </Grid>
          </Grid>
        </DialogContent>
        <DialogActions>
          {previewUrl && (
            <Button
              component="label"
              onClick={blob2Base}
              variant="contained"
              disableElevation
            >
              Сохранить
            </Button>
          )}
        </DialogActions>
      </Dialog>
    </>
  );
};

export default ProductUpload;
