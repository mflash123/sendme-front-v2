import React, { useState, useEffect, useContext } from "react";
import { Controller, useForm } from "react-hook-form";
import {
  Button,
  TextField,
  Select,
  MenuItem,
  InputLabel,
  Grid,
  Paper,
  Typography,
  Breadcrumbs,
  FormControlLabel,
  Switch,
  FormControl,
  LinearProgress,
  Tabs,
  Tab,
  FormHelperText,
} from "@material-ui/core";
import { ApiContext } from "./../../../../context/ApiContext";

const Action = ({ type, register, errors, control, data, setValue, getValues }) => {
  console.log(data)
  switch (type) {
    case 1:
      return (
        <TextField
          error={errors.url}
          placeholder="google.ru"
          label="Введите ссылку перехода"
          name="action.url"
          fullWidth={true}
          inputRef={register({
            required: true,
            minLength: {
              value: 5,
              message: "invalid url address",
            },
          })}
          helperText={errors.url && "Укажите ссылку перехода"}
        />
      );

    case 5:
      return (
        <FormControl
          error={errors.action && errors.action.button}
          fullWidth={true}
        >
          <InputLabel>Выберите кнопку действия</InputLabel>
          <Controller
            as={
              <Select fullWidth={true}

              >
                {data.map((value, index) => (
                  <MenuItem key={index} value={value.id}>
                    {value.data.name} - {value.data.destination}
                  </MenuItem>
                ))}
              </Select>
            }
            control={control}
            name="action.button"
            rules={{ required: true }}
          />
          {errors.action && errors.action.button && (
            <FormHelperText>Error</FormHelperText>
          )}
        </FormControl>
      );
  }
};


const ProductActions = ({
  control,
  errors,
  register,
  setValue,
  getValues,
  defaultValues,
  watch
}) => {
  const [type, setType] = useState(getValues("action.type"));
  const [data, setData] = useState([]);
  const { getActions } = useContext(ApiContext);

  //setValue('action.type',type,true)
  console.log(type,getValues("action.type"));

  useEffect(() => {
    if (type == 5) {
      getActions(type)
        .then(({ data }) => setData(data))
        .catch(({ error }) => console.log(error));
    }
  }, [type]);

  useEffect(() => {
    setType(getValues("action.type"))
  }, [getValues("action.type")]);


  console.log(watch("action.type"));


  return (
    <>
      <Grid item xs={3}>
        <Controller
          control={control}
          name="action.type"
          rules={{ required: true }}
          style={{ paddingTop: 16 }}
          render={({ onChange, onBlur, value, name, ref }) => (
            <Select
              fullWidth={true}
              onChange={(e) => {
                setType(e.target.value); //пиздец мне стыдно, но я не придумал как еще отререндерить, с новым типом
                setValue("action.type", e.target.value);
              }}
              value={getValues("action.type")}
              defaultValue={getValues("action.type")}
            >
              <MenuItem value={1}>Ссылка</MenuItem>
              <MenuItem value={5}>Кнопка</MenuItem>
            </Select>
          )}
        />
      </Grid>

      <Grid item xs={9}>
        <Action
          type={type}
          register={register}
          errors={errors}
          control={control}
          data={data}
          setValue={setValue}
          getValues={getValues}
        />
      </Grid>
    </>
  );
};

export default ProductActions;
