import React, {useState, useContext} from 'react'
import { Grid, Paper, Button, TextField, Select, MenuItem, FormHelperText, FormControl, Typography, Slider, FormControlLabel, Box, Card, Divider, Link, Breadcrumbs } from '@material-ui/core'
import Checkbox from '@material-ui/core/Checkbox';
import { TwitterPicker } from 'react-color';
import PhoneInput from 'react-phone-input-2'
import 'react-phone-input-2/lib/style.css'
import Emoji from '../TextArea/Emoji'
import {ApiContext} from '../../../context/ApiContext'


const CustomButton = (props) => {

    const {edit, content, typeId} = props

    let defaultValue = {
        name:'',
        description:false,
       // variant:'contained',

        design:{
            fontColor:'#000',
            brdColor:'#22194D',
            bkgColor:'#22194D',
            type:'text'
        },

        type:'link',
        // link:'',
        // callNumber:'',
        // email:'',
        // emailSubject:'',


    };
    /** Edit preparation */
    if(edit) defaultValue = JSON.parse(content.data)
    const [value,setValue] = useState(defaultValue)
    if(edit) console.log(JSON.parse(content.data))


    const {storeContent, updateContent} = useContext(ApiContext)

    const [slider,setSlider] = useState(value.design.radius ? value.design.radius : 1)
    const [isDescription,setIsDescription] = useState( defaultValue.description ? true : false )
    const [bkgColor,setBkgColor] = useState(value.design.bkgColor)
    const [brdColor,setBrdColor] = useState(value.design.brdColor)
    const [fontColor,setFontColor] = useState(value.design.fontColor)
    const [bkgColorPicker,setBkgColorPicker] = useState(false)
    const [brdColorPicker,setBrdColorPicker] = useState(false)
    const [fontColorPicker,setFontColorPicker] = useState(false)
    const [isEmoji,setIsEmoji] = useState(false)
    const [isEmojiDescript,setIsEmojiDescript] = useState(false)

    const colors = ['#D9E3F0', '#F47373', '#697689', '#37D67A', '#2CCCE4', '#555555', '#dce775', '#ff8a65', '#ba68c8','#61BB46','#963D97','#009DDC', '#fff', '#000']
    console.log(value)

    


    const btnStyle = () => {
        if(value.design.type==='contained') return {borderRadius:slider+'px',backgroundColor:bkgColor, color:fontColor}
        else if(value.design.type==='outlined') return {borderRadius:slider+'px',border:'1px solid '+brdColor, color:fontColor}
        else if(value.design.type==='text') return {color:fontColor}
    }

    const handleSubmit = (e) => {
        console.log('handleSubmit')
        let data = {
            type:value.type,
            
                ...value,
                design:{
                    ...value.design,
                    fontColor:fontColor,
                    radius:slider,
                }

            
        }
        console.log(data)
        storeContent(data,21)
        .then( value =>{
            console.log(value)
            window.location.replace("/dashboard")
        })
        e.preventDefault();
    }
    
    const updateSubmit = (e) => {
        console.log(value)

        let data = {
            type:value.type,
                ...value,
                description: isDescription ? value.description : false,
                design:{
                    //...value.design,
                    type: value.design.type,
                    bkgColor: bkgColor,
                    brdColor: brdColor,
                    fontColor:fontColor,
                    radius:slider,
                }
        }
        console.log(data)
        updateContent(data,content.id)
        .then( value =>{
            window.location.replace("/dashboard")
        })
        e.preventDefault();
    }

    console.log(value)
    

    return(
        <Grid container spacing={3}>
            <Grid item xs={12} sm={8}>


                <form autoComplete="off" onSubmit={ edit ? updateSubmit : handleSubmit }>
                <Grid direction="row" container style={{marginBottom:30}}>
                    <TextField
                    label="Название кнопки"
                    value={value.name}
                    fullWidth={true}
                    name="name"
                    onChange={(e)=>{
                        setValue({
                            ...value,
                            [e.target.name]: e.target.value
                        });
                    }}
                    required
                    autoFocus={true}
                    onFocus={()=>setIsEmoji(true)}
                    onBlur={()=>setTimeout(()=>setIsEmoji(false),500)}
                    
                    //error={true}
                    />
                    { isEmoji && <Emoji callback={(e)=>{
                    setValue({
                    ...value,
                        name: value.name + e.native
                    })
                    }} />}


                    {isDescription && (
                        <TextField
                        label="Описание"
                        value={ value.description ? value.description:''  }
                        fullWidth={true}
                        name="description"
                        onChange={(e)=>{
                            setValue({
                                ...value,
                                [e.target.name]: e.target.value
                                });
                        }}
                        onFocus={()=>setIsEmojiDescript(true)}
                        onBlur={()=>setTimeout(()=>setIsEmojiDescript(false),500)}

                        />
                    )}
                    { isEmojiDescript && <Emoji callback={(e)=>{
                    setValue({
                    ...value,
                        description: value.description + e.native
                    })
                    }} />}

                    <FormControlLabel
                        control={
                        <Checkbox
                        checked={isDescription}
                        onClick={()=>setIsDescription(isDescription ? false : true)}
                        // name="checkedB"
                            color="primary"
                        />
                        }
                        label="Добавить описание"
                    />




                

                </Grid>




                {/* <Typography variant="h4">Action</Typography> */}
                {/* /** Action */}
                <Divider variant='middle' />
                <Grid direction="row" container style={{marginBottom:30,marginTop:30}}>

                    <Grid item sm={3}>
                        <Select
                        value={value.type}
                        //   onChange={handleChange}
                        displayEmpty
                        inputProps={{ 'aria-label': 'Without label' }}
                        name="type"
                        onChange={(e)=>{
                            setValue({
                                ...value,
                                [e.target.name]: e.target.value
                              });
                        }}
                        >
                            <MenuItem value="link">Ссылка</MenuItem>
                            {/* <MenuItem value="form">Form</MenuItem> */}
                            {/* <MenuItem value="call">Call</MenuItem>
                            <MenuItem value="email">Send email</MenuItem> */}
                        </Select>
                        {/* <FormHelperText>Тип  type</FormHelperText> */}
                    </Grid>
                    <Grid item sm={9}>

                        { value.type==='link' && (
                            <>
                            <TextField
                                label="Введите ссылку"
                                fullWidth={true}
                                name="link"
                                value={value.link}
                                onChange={(e)=>{
                                    setValue({
                                        ...value,
                                        [e.target.name]: e.target.value
                                    });
                                }}
                                required={true}
                                autoFocus={true}
                            />
                            <FormHelperText>Пример: https://google.com</FormHelperText>
                            </>
                            )
                        }

                        { value.type==='call' && (
                            <PhoneInput
                            country={'ru'}
                            value={value.callNumber}
                            onChange={phone =>setValue({
                                ...value,
                                callNumber: phone
                            })
                            }
                            inputProps={{
                                required: true,
                                autoFocus: true
                              }}
                            />
                            )
                        }
                        
                        { value.type==='email' && (
                            <>
                            <TextField
                                label="Email"
                                fullWidth={true}
                                name="email"
                                value={value.email}
                                onChange={(e)=>{
                                    setValue({
                                        ...value,
                                        [e.target.name]: e.target.value
                                    });
                                }}
                                required={true}
                                autoFocus={true}
                            />
                            <TextField
                                label="Email subject"
                                fullWidth={true}
                                name="emailSubject"
                                value={value.emailSubject}
                                onChange={(e)=>{
                                    setValue({
                                        ...value,
                                        [e.target.name]: e.target.value
                                    });
                                }}
                            />
                            </>
                            )
                        }
                    </Grid>
                </Grid>




                {/* <Typography variant="h2">Design</Typography> */}
                {/* /** Design */}
                <Divider variant='middle' />
                <Grid direction="row" container style={{marginBottom:30,marginTop:30}}>
                <Grid item sm={3}>
                <Select
                value={value.design.type}
                displayEmpty
                name="variant"
                onChange={(e)=>{
                    setValue({
                        ...value,
                        design: {...value.design, type: e.target.value}
                       // [e.target.name]: e.target.value
                      });
                }}
                >
                    <MenuItem value="text">Текст</MenuItem>
                    <MenuItem value="outlined">Без фона</MenuItem>
                    <MenuItem value="contained">С фоном</MenuItem>
                </Select>
                </Grid>
                <Grid item sm={9}>
                <Typography id="discrete-slider" gutterBottom>Закругление кнопки</Typography>
                <Slider
                  //  defaultValue={30}
                // getAriaValueText={'valuetext'}
                    aria-labelledby="discrete-slider"
                    valueLabelDisplay="auto"
                    step={1}
                    marks
                    min={0}
                    max={20}
                    name="radius"
                    onChange={(e,val)=>{ setSlider(val) }}
                    value={slider}
                />
                </Grid>
                </Grid>

                <Grid direction="row" container spacing={3}>
                    <Grid item sm={6}>
                    {value.design.type==='contained' && <Button variant="contained" fullWidth={true} onClick={()=>setBkgColorPicker(true)}>Цвет фона</Button>}
                    { bkgColorPicker && <TwitterPicker colors={colors} color={bkgColor} onChangeComplete={color=>{setBkgColor(color.hex);setBkgColorPicker(false)}} />}

                    {value.design.type==='outlined' && <Button variant="contained" fullWidth={true} onClick={()=>setBrdColorPicker(true)}>Цвет рамки</Button>}
                    { brdColorPicker && <TwitterPicker colors={colors} color={brdColor} onChangeComplete={color=>{setBrdColor(color.hex);setBrdColorPicker(false)}} />}


                        </Grid>
                        <Grid item sm={6}>
                    <Button variant="contained" fullWidth={true} onClick={()=>setFontColorPicker(true)}>Цвет текста</Button>
                    { fontColorPicker && <TwitterPicker colors={colors} color={fontColor} onChangeComplete={color=>{setFontColor(color.hex);setFontColorPicker(false)}} /> }
                        </Grid>

                </Grid>

                {
                edit
                    ?   <Button type="submit" variant="contained" fullWidth={true}>Обновить</Button>
                    :   <Button type="submit" variant="contained" fullWidth={true}>Сохранить</Button>
                }
                
                </form>
            </Grid>




            <Grid item xs={12} sm={4} style={{textAlign:'center'}}>
                <FormControl>
                <Button
                    variant={value.design.type}
                    style={btnStyle()}
                >
                {value.name === '' ? 'Дизайн кнопки' : value.name}
                </Button>
                {isDescription && <FormHelperText style={{textAlign:'center'}}>{value.description}</FormHelperText>}
                
                </FormControl>
            </Grid>
        </Grid>
    )
}
export default CustomButton