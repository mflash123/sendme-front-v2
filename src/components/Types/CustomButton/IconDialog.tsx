import { Button, Dialog, DialogActions, DialogContent, DialogContentText } from "@material-ui/core";
import React from "react";
import { useTranslation } from 'react-i18next';
import Brands from "./Brands";

interface IProps {
    //contentId?: number;
    open:boolean;
    setOpen: (open: boolean) => void;
    setIcon: (icon: string) => void;
}




const IconDialog = ({ open, setOpen, setIcon }: IProps) => {
    const { t } = useTranslation();

    const handleClick = (props: string) => {
        console.log('handleClick',props)
        setIcon(props)
        setOpen(false)
    }
    return (
        <Dialog
            open={open}
            // onClose={handleClose}
            aria-labelledby="alert-dialog-title"
            aria-describedby="alert-dialog-description"
        >
            <DialogContent>
                <DialogContentText id="alert-dialog-description">

                <Brands handleClick={handleClick} />

          </DialogContentText>
            </DialogContent>
            <DialogActions>
                <Button color="primary" onClick={() => setOpen(false)}>
                {t('dialog_close')}
                </Button>
            </DialogActions>
        </Dialog>
    );
};

export default IconDialog;