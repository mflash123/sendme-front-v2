import React from "react";
import * as Icons from "react-icons/fa";

const PreviewBrand = ({iconname}) => {
  //  const v = 'Fa500Px';
    const Icon = Icons[iconname];

  return (
    <Icon size="2rem" />
  );
};

export default PreviewBrand;
