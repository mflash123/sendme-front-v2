import { ErrorMessage } from '@hookform/error-message';
import { Button, Checkbox, FormControl, FormControlLabel, FormHelperText, Grid, InputAdornment, MenuItem, Paper, Select, Slider, TextField, Typography } from "@material-ui/core";
import KeyboardArrowLeftIcon from "@material-ui/icons/KeyboardArrowLeft";
import SaveIcon from "@material-ui/icons/Save";
//import Emoji from '../TextArea/Emoji';
import { Editor } from '@tinymce/tinymce-react';
import React from "react";
import { Controller, useForm } from "react-hook-form";
import { useTranslation } from 'react-i18next';
//import { Controller, useForm } from "react-hook-form";
import { Link } from "react-router-dom";
import { useStyles } from "../../../style";
import { ApiContext } from './../../../context/ApiContext';
import { BlockLabel } from './../../../styled';
import ColorPicker from './ColorPicker';
import IconDialog from './IconDialog';
import PreviewBrand from './PreviewBrand';


const defaultValues = {
    // name:'sadasd',
    isdescription: false,
    //type: "contained",
    action: {
        type: 'link',
        action: '',
    },
    // actiontype: 'link',
    // link: '',

}
const requiredLength = {
    maxLength: 25,
    minLength: 1,
}

type IFormInputs = {
    //  name:string,
    //type: string,
    // actiontype: string,
    // link: string,
    isdescription: boolean,
    action: {
        type: string,
        action: string,
    }
};



interface IProps {
    contentId?: number;
}



const CustomButton = ({ contentId }: IProps) => {

    //const history = useHistory();
    const { t } = useTranslation();
    
    const { watch, handleSubmit, control, setValue, formState: { errors } } = useForm<IFormInputs>({
        defaultValues
    });

    const { storeContent, getContent, updateContent } = React.useContext(ApiContext)

    const [btnName, setBtnName] = React.useState('Название кнопки')
    const [btnDescription, setBtnDescription] = React.useState('Описание кнопки')
    const [variant, _setVariant] = React.useState("contained")
    const [dialog, setDialog] = React.useState(false);
    const [brandIcon, setBrandIcon] = React.useState('');
    const [dialogBrand, setDialogBrand] = React.useState(false);
    const [isLoading, setLoading] = React.useState(true)
    const [radius, setRadius] = React.useState<number>(1)

    const [color, setColor] = React.useState({
        fontColor: '#fff',
        //  brdColor: '#22194D',
        bkgColor: '#22194D',
    });
    const [typeColor, setTypeColor] = React.useState("fontColor");


    const classes = useStyles();
    const editorRef: any = React.useRef(null);

    React.useEffect(() => {
        if (contentId) {
            getContent(contentId)
                .then(({ data }: any) => {
                    let res = JSON.parse(data.data)
                    console.log(res)
                    if (res.isdescription) {
                        setBtnDescription(res.description)
                    }
                    setBtnName(res.name)
                    setColor(res.design)
                    setRadius(res.design.radius)
                    setBrandIcon(res.design.brand)
                    setLoading(false)

                    setValue('action', res.action)
                    setValue('isdescription', res.isdescription)

                })
        } else {
            setLoading(false)
        }
    }, [])

    const onSubmit = (data: any) => {


        data.name = btnName;
        data.description = btnDescription;
        data.design = color;
        data.design.radius = radius;
        data.design.brand = brandIcon;



        if (data.name.length > 0) {
            if (contentId) {
                //update
                updateContent(data, contentId)
                    .then((value: any) => {
                        console.log(value)
                        window.location.replace("/dashboard")
                    })
            } else {
                //create
                storeContent(data, 37)
                    .then((value: any) => {
                        console.log(value)
                        window.location.replace("/dashboard")
                    })
            }
            //history.push('/dashboard')

        } else {
            console.log('Error', data.name)
        }




    }

    const handleClickDialog = (typeColore: "fontColor" | "brdColor" | "bkgColor") => {
        setTypeColor(typeColore)
        setDialog(true)
    }

    const handleColorChange = (colore: string) => {
        console.log(colore)
        console.log(typeColor)
        switch (typeColor) {
            case 'fontColor':
                color.fontColor = colore
                break;

            case 'brdColor':
                // color.brdColor = colore
                break;

            case 'bkgColor':
                color.bkgColor = colore
                break;
        }

        setColor({ ...color })

    }

    const RenderButton = ({ variant }: any) => {
        return <Button variant={variant}
            startIcon={brandIcon && <PreviewBrand iconname={brandIcon} />}
            style={{
                color: color.fontColor,
                backgroundColor: color.bkgColor,
                borderRadius: radius,
                // borderColor: color.brdColor,
            }}>

            {btnName}
        </Button>
    }

    if (isLoading) {
        return <h1>Load</h1>
    }

    console.log(errors)

    return (
        <form onSubmit={handleSubmit(onSubmit)}>
            <IconDialog open={dialogBrand} setOpen={setDialogBrand} setIcon={setBrandIcon} />
            <ColorPicker open={dialog} setOpen={setDialog} color={color} typeColor={typeColor} handleColorChange={handleColorChange} />

            <Grid container justify="center">
                <FormControl style={{ minHeight: 60 }}>
                    <RenderButton variant={variant} />
                    {watch('isdescription') && <FormHelperText>{btnDescription}</FormHelperText>}
                </FormControl>
            </Grid>
            <Paper elevation={0} className={classes.paperfield}>
                <Grid container spacing={1}>
                    <Grid item sm={12}>
                        <BlockLabel>Название кнопки</BlockLabel>



                        <Editor


                            apiKey="y62arcz5josg30qxd2asp8awkytbvpynk67mjyaqkscpgcuc"
                            //style={{borderBottom: '1px solid black'}}

                            value={btnName}
                            //initialValue="asd sd"
                            onInit={(_evt, editor) => editorRef.current = editor}
                            init={{
                                init_instance_callback: function (editor) {
                                    console.log('editor init ' + editor.id)
                                },
                                entity_encoding: 'raw',
                                forced_root_block: '',
                                inline: true,
                                menubar: false,
                                plugins: [
                                    'emoticons'
                                ],
                                toolbar: 'emoticons',
                                branding: false,
                                statusbar: false,
                                content_style: '.mce-content-body {border-bottom: 1px solid #949494} *:focus {outline:none}'
                            }}
                            outputFormat='text'
                            onEditorChange={
                                (e) => {
                                    console.log(e)
                                    if (e.length <= requiredLength.maxLength) {
                                        console.log(e.length, requiredLength.maxLength)
                                        setBtnName(e)
                                    }
                                }
                            }
                        />
                        <Typography display="block" color="textSecondary">Осталось символов: {requiredLength.maxLength - btnName.length}</Typography>
                        {requiredLength.maxLength === btnName.length && <Typography color="textSecondary">Краткость-сестра таланта🧑‍🏫</Typography>}

                        <FormControlLabel
                            control={
                                <Controller
                                    name="isdescription"
                                    control={control}
                                    render={({ field: props }) => (
                                        <Checkbox
                                            {...props}
                                            onChange={(e) => props.onChange(e.target.checked)}
                                            checked={props.value}
                                        />
                                    )}

                                />
                            }
                            label="Добавить описание"
                            style={{ paddingTop: '1rem', paddingBottom: '1rem' }}
                        />

                        {
                            watch('isdescription') &&
                            <>
                                <Editor
                                    apiKey="y62arcz5josg30qxd2asp8awkytbvpynk67mjyaqkscpgcuc"
                                    value={btnDescription}
                                    onInit={(_evt, editor) => editorRef.current = editor}
                                    init={{
                                        entity_encoding: 'raw',
                                        forced_root_block: '',
                                        skin: 'material-outline',
                                        inline: true,
                                        menubar: false,
                                        plugins: [
                                            'emoticons'
                                        ],
                                        toolbar: 'emoticons',
                                        branding: false,
                                        statusbar: false,
                                        content_style: '.mce-content-body {border-bottom: 1px solid #949494} *:focus {outline:none}'
                                    }}
                                    outputFormat='text'
                                    onEditorChange={
                                        (e) => {
                                            if (e.length <= requiredLength.maxLength) {
                                                setBtnDescription(e)
                                            }
                                        }
                                    }
                                />
                                <Typography display="block" color="textSecondary">Осталось символов: {requiredLength.maxLength - btnDescription.length}</Typography>
                                {requiredLength.maxLength === btnDescription.length && <Typography color="textSecondary">Краткость-сестра таланта🧑‍🏫</Typography>}
                            </>
                        }




                    </Grid>
                    <Grid item sm={12}>
                        <BlockLabel style={{ paddingTop: '1rem' }}>Дизайн кнопки</BlockLabel>


                        {/* <FormControl component="fieldset">
                            <Controller
                                name="type"
                                control={control}
                                render={({ field: props }) => (
                                    <RadioGroup
                                        row
                                        aria-label="position"
                                        name="position"
                                        defaultValue="top"
                                        value={props.value}
                                        onBlur={props.onBlur}
                                        onChange={(e) => {
                                            props.onChange(e)
                                            setVariant(e.target.value)
                                            //console.log(e.target.value)
                                        }}
                                    >
                                        <FormControlLabel
                                            label="С фоном"
                                            value="contained"
                                            control={<Radio color="primary" size='small' />}
                                        />
                                        <FormControlLabel
                                            label="Без фона"
                                            value="outlined"
                                            control={<Radio color="primary" size='small' />}
                                        />
                                        <FormControlLabel
                                            label="Текст"
                                            value="text"
                                            control={<Radio color="primary" size='small' />}
                                        />
                                    </RadioGroup>
                                )}
                            />





                        </FormControl> */}
                    </Grid>
                    <Grid item sm={12}>
                        <Button variant="contained" onClick={() => handleClickDialog('fontColor')} disableElevation size="small" style={{ marginRight: 5 }}>Цвет текста</Button>
                        <Button variant="contained" onClick={() => handleClickDialog('bkgColor')} disableElevation size="small" style={{ marginRight: 5 }}>Цвет фона</Button>
                        <Button variant="contained" onClick={() => setDialogBrand(!dialogBrand)} disableElevation size="small" style={{ marginRight: 5 }}> {t('icon_choose')} </Button>
                        {/* <Button variant="contained" onClick={()=>handleClickDialog('brdColor')} disableElevation size="small">Цвет рамки</Button> */}
                        {/* {watch('type') === 'contained' && <Button variant="contained" onClick={()=>handleClickDialog('bkgColor')} disableElevation size="small" style={{ marginRight: 5 }}>Цвет фона</Button>}
                        {watch('type') === 'outlined' && <Button variant="contained" onClick={()=>handleClickDialog('brdColor')} disableElevation size="small">Цвет рамки</Button>} */}
                        <Typography id="discrete-slider" style={{ marginTop: 15 }}>Края кнопки: {radius}px</Typography>
                        <Slider
                            aria-labelledby="discrete-slider"
                            valueLabelDisplay="auto"
                            step={1}
                            marks
                            min={0}
                            max={20}
                            name="radius"
                            onChange={(_e: any, val: number | number[]) => setRadius(val as number)}
                            value={radius}
                        />


                    </Grid>

                    <BlockLabel style={{ paddingTop: '1rem' }}>Действие кнопки</BlockLabel>
                    <Grid container>
                        <Grid item sm={3}>
                            <Controller
                                name="action.type"
                                defaultValue={defaultValues.action.type}
                                control={control}
                                render={({ field: props }) => (
                                    <Select
                                        {...props}
                                        fullWidth
                                        displayEmpty
                                    >
                                        <MenuItem value="link">Ссылка</MenuItem>
                                    </Select>
                                )}
                            />


                        </Grid>
                        <Grid item sm={9}>
                            <Controller
                                name="action.action"
                                defaultValue={defaultValues.action.action}
                                control={control}
                                rules={{
                                    required: 'Введите ссылку😡',
                                    pattern: {
                                        //value: /(^[^http://])[a-zA-Z0-9-_].*\.[a-zA-Z].*/,
                                        value: /^(?!\/\/)[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()@:%_\+.~#?&//=]*)/,
                                        message: 'Введите корректную ссылку😡'
                                    }
                                }}
                                render={({ field: props }) => (
                                    <TextField
                                        error={!!errors.action?.action}
                                        {...props}
                                        placeholder="Link"
                                        fullWidth
                                        InputProps={{
                                            startAdornment: <InputAdornment position="start">https://</InputAdornment>,
                                        }}
                                    />
                                )}
                            />
                            <ErrorMessage errors={errors} name="action.action" />
                        </Grid>
                    </Grid>

                </Grid>





                <Grid container justify="flex-end">
                    <Button component={Link} to={"/dashboard/add/"} color="primary">
                        <KeyboardArrowLeftIcon /> Back
            </Button>
                    <Button color="primary" type="submit"><SaveIcon />
                        {contentId ? 'Update' : 'Save'}

                    </Button>
                </Grid>
            </Paper>


        </form>
    )
}

export default CustomButton;