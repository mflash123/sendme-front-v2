import React from "react";
import * as Icons from "react-icons/fa";
import { iconsSet } from "./IconSet";
import { Button, Typography, Grid, Box } from "@material-ui/core";

const Brands = ({handleClick}) => {
  return (
    <Grid container>
      {iconsSet.map((v, i) => {
        const name = v.replace(/Fa/gi, "");
        const Icon = Icons[v];
        return (
          <Grid item xs={3}>
            <Box textAlign='center'>
            <Button
      
              onClick={()=>handleClick(v)}
              size="small"
            >
              <Icon key={i} size="2rem" />
            </Button>
            <Typography variant='caption' size=".5pt" display='block'>{name}</Typography>
            </Box>
          </Grid>
        );
      })}
    </Grid>
  );
};

export default Brands;
