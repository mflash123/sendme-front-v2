import { Button, Dialog, DialogActions, DialogContent } from "@material-ui/core";
import React from "react";
import { SwatchesPicker } from "react-color";



interface IProps {
    open: boolean;
    setOpen: (open: boolean) => void;

    color?: any;
    typeColor?: any;
  //  setColorPicker: (color: string) => void;  //any- TS noob me =(
    handleColorChange: (color:string)=>void

}



const ColorPicker = ({ open, setOpen, color, typeColor, handleColorChange }: IProps) => {

   console.log(color,color[typeColor])
    //const [color, setColor] = React.useState("#fff");

    return (
        <Dialog open={open}>
            <DialogContent>
            <SwatchesPicker
                    color={color[typeColor]}
                    onChangeComplete={col=>handleColorChange(col.hex)}
                    //onChangeComplete={colore => setColorPicker(colore.hex)}
                />
            </DialogContent>
            <DialogActions>
            <Button variant="contained" color="primary" style={{backgroundColor:color[typeColor]}} onClick={() => setOpen(false)}>👌</Button>
            </DialogActions>
            
        </Dialog>
    )
}

export default ColorPicker;