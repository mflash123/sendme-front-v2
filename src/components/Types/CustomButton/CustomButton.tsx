import { Button, Grid, Paper, TextField } from "@material-ui/core";
import KeyboardArrowLeftIcon from "@material-ui/icons/KeyboardArrowLeft";
import SaveIcon from "@material-ui/icons/Save";
import React from "react";
import { Controller, useForm } from "react-hook-form";
import { Link } from "react-router-dom";
import { useStyles } from "./../../../style";
import Emoji from './../TextArea/Emoji';

const CustomButton = () => {
   // const [value, setValue] = React.useState({})
    const [isEmoji, setIsEmoji] = React.useState(false)
    const classes = useStyles();
    const { control } = useForm();

    return (
        <form>
            <Grid container justify="center">
                Preview
</Grid>
            <Paper elevation={0} className={classes.paperfield}>
                <Controller
                    control={control}
                    name="name"
                    // defaultValue={value.name}
                    rules={{ required: "Укажите название кнопки" }}

                    render={({ field }) => (
                        <TextField
                            {...field}
                            label="Придумайте название кнопки"
                            helperText="Это будет название кнопки"
                            fullWidth={true}
                            autoComplete="off"
                            required
                            autoFocus={true}
                            onFocus={() => setIsEmoji(true)}
                            onBlur={() => setIsEmoji(false)}

                        />
                    )}
                />

                <div hidden={!isEmoji}>
                    <Emoji callback={ (e: { native: any; }) => {
                        console.log(e.native)
                        // setValue({
                        //    ...value,
                        //    name: value.name + e.native
                        // })
                    }}
                    />
                </div>






                <Grid container justify="flex-end">
                    <Button component={Link} to={"/dashboard/add/"} color="primary">
                        <KeyboardArrowLeftIcon /> Back
            </Button>
                    <Button color="primary" type="submit">
                        <SaveIcon /> Save
            </Button>
                </Grid>
            </Paper>
        </form>
    )
}

export default CustomButton;