import React, { useState, useContext, useEffect } from "react";

import {
  TextField,
  Typography,
  Button,
  Paper,
  Divider,
  Grid,
  Breadcrumbs,
  InputAdornment,
} from "@material-ui/core";

import { useForm, Controller } from "react-hook-form";

import FooterItem from "./../../../layout/FooterItem";
import { ApiContext } from "./../../../context/ApiContext";
import { useStyles } from "../../../style";
import { Link } from "react-router-dom";
import KeyboardArrowLeftIcon from "@material-ui/icons/KeyboardArrowLeft";
import SaveIcon from "@material-ui/icons/Save";

const TypeItemFields = (props) => {
  const { storeContent, updateContent, getContent } = useContext(ApiContext);
  const { edit, content, typeId } = props;
  const { register, handleSubmit, watch, errors, control } = useForm();
  const classes = useStyles();

  let defaultValue = {
    name: "Укажите название кнопки ",
    destination: "",
  };
  /** Edit preparation */
  if (edit) defaultValue = JSON.parse(content.data);
  const [value, setValue] = useState(defaultValue);

  console.log(value);


  const label = () => {
    //мне оч не нравится эта конструкция label().name
    switch (typeId) {
      case 5:
        return {
          name: "Укажите название кнопки Instagram",
          helper: "Введите ссылку",
          helperText: "Укажите ссылку",
          helperTextName: "Укажите название кнопки",
          pattern: /https*:\/\/(www.)*instagram.com\/.*/,
        };
      case 6:
        return {
          name: "Укажите название кнопки Whatsapp",
          helper: "Пример: +79551234567",
          helperText: "Введите номер в международном формате",
          helperTextName: "Укажите название кнопки",
          pattern: /\+.[0-9]{3,}/,
        };
      case 7:
        return {
          name: "Укажите название кнопки Telegram",
          helper: "Пример: https://t.me/sendmecc",
          helperText: "Укажите полную ссылку",
          helperTextName: "Укажите название кнопки",
          pattern: /https*:\/\/(www.)*t.me\/.*/,
        };
      case 8:
        return {
          name: "Укажите название кнопки Skype",
          helper: "Введите ссылку",
          helperText: "Укажите ссылку",
          helperTextName: "Укажите название кнопки",
          //pattern: /https*:\/\/(m.)*facebook.com\/.*/,
        };
      case 9:
        return {
          name: "Укажите название кнопки Vkontankte",
          helper: "Введите ссылку",
          helperText: "Укажите ссылку",
          helperTextName: "Укажите название кнопки",
          pattern: /https*:\/\/(m.)*(www.)*vk.com\/.*/,
        };
      case 10:
        return {
          name: "Укажите название кнопки Facebook",
          helper: "Введите ссылку",
          helperText: "Укажите ссылку",
          helperTextName: "Укажите название кнопки",
          pattern: /https*:\/\/(m.)*(www.)*facebook.com\/.*/,
        };
      case 11:
        return {
          name: "Укажите название кнопки Web",
          helper: "Введите ссылку",
          helperText: "Укажите ссылку",
          helperTextName: "Укажите название кнопки",
          pattern: /https*:\/\//,
        };
      case 15:
        return {
          name: "Укажите название кнопки Viber",
          helper: "Введите ссылку",
          helperText: "Укажите ссылку",
          helperTextName: "Укажите название кнопки",
          pattern: /\+.[0-9]{3,}/,
        };
      case 16:
        return {
          name: "Укажите название кнопки Phone",
          helper: "Введите ссылку",
          helperText: "Укажите ссылку",
          helperTextName: "Укажите название кнопки",
          pattern: /\+.[0-9]{3,}/,
        };
      case 19:
        return {
          name: "Укажите название кнопки Line",
          helper: "Введите ссылку",
          helperText: "Укажите ссылку",
          helperTextName: "Укажите название кнопки",
          //pattern: /https*:\/\/(m.)*(www.)*facebook.com\/.*/,
        };
      case 20:
        return {
          name: "Укажите название кнопки Youtube",
          helper: "Введите ссылку",
          helperText: "Укажите ссылку",
          helperTextName: "Укажите название кнопки",
          pattern: /https*:\/\/(www.)*youtube.com\/.*/,
        };
      case 24:
        return {
          name: "Укажите название кнопки Odnoklassniki",
          helper: "Введите ссылку",
          helperText: "Укажите ссылку",
          helperTextName: "Укажите название кнопки",
          pattern: /https*:\/\/(m.)*(www.)*ok.ru\/.*/,
        };
      case 26:
        return {
          name: "Укажите название кнопки TextBlock",
          helper: "Введите ссылку",
          helperText: "Укажите ссылку",
          helperTextName: "Укажите название кнопки",
          //pattern: /https*:\/\/(m.)*facebook.com\/.*/,
        };
      case 38:
        return {
          name: "Укажите название кнопки TikTok",
          helper: "Введите ссылку",
          helperText: "Укажите ссылку",
          helperTextName: "Укажите название кнопки",
          pattern: /https*:\/\/(www.)*tiktok.com\/.*/,
        };

      default:
        return {
          name: "Укажите название кнопки Uncategory type: " + typeId,
          helper: "Uncategory",
        };
    }
  };

  const onSubmit = (data, e) => {
    console.log(data, e);
    if (edit) {
      console.log("update");
      updateContent(data, content.id).then(() => {
        window.location.replace("/dashboard");
      });
    } else {
      console.log("save");
      storeContent(data, typeId).then((data) => {
        window.location.replace("/dashboard");
      });
    }

    console.log(data);
  }; // your form submit function which will invoke after successful validation

  const onError = (errors, e) => console.log(errors, e);

  return (
    <form onSubmit={handleSubmit(onSubmit, onError)}>
      <Paper elevation={0} className={classes.paperfield}>
        <Controller
          control={control}
          name="name"
          label={label().name}
          defaultValue={value.name}
          rules={{ required: "Укажите название кнопки" }}
          render={({ field }) => (
            <TextField
              {...field}
              label={label().name}
              helperText={label().helperTextName}
              fullWidth={true}
              autoComplete="off"
            />
          )}
        />

        <Controller
          control={control}
          name="destination"
          label={label().helper}
          defaultValue={value.destination}
          rules={{ required: "Укажите название кнопки" }}
          render={({ field }) => (
            <TextField
              {...field}
              label={label().helper}
              helperText={label().helperText}
              fullWidth={true}
              autoComplete="off"
            />
          )}
        />

      

        <Grid container justify="flex-end">
          <Button component={Link} to={"/dashboard/add/"} color="primary">
            <KeyboardArrowLeftIcon /> Back
          </Button>
          <Button color="primary" type="submit">
            <SaveIcon /> Save
          </Button>
        </Grid>
      </Paper>
    </form>
  );
};

// TypeItemFields.propTypes = {
//     type: PropTypes.number
//   };

// TypeItemFields.defaultProps = {
//     name: 'empty',
//     destination: 'empty'
// };

export default TypeItemFields;
