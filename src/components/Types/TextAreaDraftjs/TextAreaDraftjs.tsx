import createLinkPlugin from '@draft-js-plugins/anchor';
import { BoldButton, CodeBlockButton, HeadlineOneButton, HeadlineThreeButton, HeadlineTwoButton, ItalicButton, OrderedListButton, UnderlineButton, UnorderedListButton } from '@draft-js-plugins/buttons';
import createToolbarPlugin, { Separator } from '@draft-js-plugins/static-toolbar';
import { Button, Grid, Paper } from '@material-ui/core';
import KeyboardArrowLeftIcon from "@material-ui/icons/KeyboardArrowLeft";
import SaveIcon from "@material-ui/icons/Save";
import { Alert } from '@material-ui/lab';
import { convertFromRaw, convertToRaw, EditorState } from "draft-js";
import createEmojiPlugin from "draft-js-emoji-plugin";
import Editor from "draft-js-plugins-editor";
import React from "react";
import { Link, useHistory } from 'react-router-dom';
import "../../../../node_modules/@draft-js-plugins/static-toolbar/lib/plugin.css";
import "../../../../node_modules/draft-js-emoji-plugin/lib/plugin.css";
import "../../../../node_modules/draft-js/dist/Draft.css";
import { ApiContext } from './../../../context/ApiContext';
import { TextContext } from './../../../context/textContext';
import { useStyles } from "./../../../style";
import { EditorDraftStyled } from './../../../styled';

interface IProps {
    contentId?: number;
}

interface IState {
    //textAlignment: "left" | "center" | "right" | undefined;
    isError?: boolean;
    isSuccess?: boolean;
}




const emojiPlugin = createEmojiPlugin({
    useNativeArt: true,
    // selectButtonContent:'😃'
});
const linkPlugin = createLinkPlugin({
    placeholder:'asdadasdas'
});

const toolbarPlugin = createToolbarPlugin();
const { Toolbar } = toolbarPlugin;

const { EmojiSuggestions, EmojiSelect } = emojiPlugin;


const TextAreaDraftjs = ({ contentId }: IProps) => {
    //const { setText } = React.useContext(TextContext) as DraftjsTextContextType;
    

    const classes = useStyles();
    const history = useHistory();
    // const [editorState, setEditorState] = React.useState(() =>
    //     EditorState.createEmpty()
    // );
    const [isLoading, setLoading] = React.useState(true)
    const [state, setState] = React.useState<IState>({ isError: false, isSuccess: false })
    //const [textAlignment, setTextAligment] = React.useState<IState>();
    
    const {editorState, setEditorState, setContentState} = React.useContext(TextContext) as DraftjsTextContextType;
    const { storeContent, getContent, updateContent } = React.useContext(ApiContext)

    console.log(editorState.getCurrentContent().getPlainText())

    const handleChange =(val: EditorState) => {
        //call context
       setEditorState(val)
       //console.log(val.getCurrentContent().getPlainText())
       //setText(val.getCurrentContent().getPlainText())
    }

    
    React.useEffect(() => {
        if (contentId) {
            getContent(contentId)
                .then(({ data }: any) => {
                    let raw = convertFromRaw(JSON.parse(data.data))
                    setEditorState(EditorState.createWithContent(raw))
                    setContentState(contentId=contentId)
                    setLoading(false)
                })
        } else {
            setLoading(false)
        }
       // editorState.EditorState.createEmpty()
    }, [])

    if (isLoading) {
        return <h1>Load</h1>
    }

    const onSubmit = () => {
        if (editorState.getCurrentContent().hasText()) {
            setState({ isError: false })
        } else {
            setState({ isError: true })
            return false;
        }

        if (contentId) {
            updateContent(convertToRaw(editorState.getCurrentContent()), contentId)
                .then(({ data }: any) => {
                    console.log(data)

                })
        } else {
            storeContent(convertToRaw(editorState.getCurrentContent()), 39)
                .then(({ data }: any) => {
                    console.log(data, data.request.id)
                    history.push("/dashboard/edit/" + data.request.id);
                })
        }
        setState({ isSuccess: true })
    }


    return (
        <>
            {/* <h1>TextAreaContent</h1> */}
            {/* <label>contentId: {contentId}</label> */}

            <Paper elevation={0} className={classes.paperfield}>
                <Toolbar>
                    {
                        // may be use React.Fragment instead of div to improve perfomance after React 16
                        (externalProps: any) => (
                            <div>
                                <BoldButton {...externalProps} />
                                <ItalicButton {...externalProps} />
                                <UnderlineButton {...externalProps} />


                                <UnorderedListButton {...externalProps} />
                                <OrderedListButton {...externalProps} />
                                
                                <Separator {...externalProps} />
                                <HeadlineOneButton {...externalProps} />
                                <HeadlineTwoButton {...externalProps} />
                                <HeadlineThreeButton {...externalProps} />
                                <CodeBlockButton {...externalProps} />
                                {/* <linkPlugin.LinkButton {...externalProps} /> */}
                                {/* <div style={{ display: 'inline-block' }}>
                                <EditorButton onClick={() => setTextAligment({ textAlignment: 'left' })}>
                                    <FormatAlignLeft />
                                </EditorButton>
                            </div>
                            <div style={{ display: 'inline-block' }}>
                                <EditorButton onClick={() => setTextAligment({ textAlignment: 'center' })}>
                                    <FormatAlignCenter />
                                </EditorButton>
                            </div>
                            <div style={{ display: 'inline-block' }}>
                                <EditorButton onClick={() => setTextAligment({ textAlignment: 'right' })}>
                                    <FormatAlignRight />
                                </EditorButton>
                            </div>
                            <Separator {...externalProps} /> */}
                                {/* <div style={{ display: 'inline-block' }}>

                                    <EmojiSelect />

                                </div> */}

                            </div>
                        )
                    }
                </Toolbar>
                <EditorDraftStyled>
                    <Editor
                        editorState={editorState}
                        onChange={handleChange}
                        plugins={[emojiPlugin, toolbarPlugin, linkPlugin]}
                    // textAlignment={textAlignment?.textAlignment}
                    />
                </EditorDraftStyled>
                <EmojiSelect />
                <EmojiSuggestions />

                {state.isError && <Alert severity="error">Text can not be empty!</Alert>}
                {state.isSuccess && <Alert severity="success">Done 👌</Alert>}

                {/* <pre>
                {JSON.stringify(editorState.getCurrentContent(), null, 2)}
            </pre> */}
                <Grid container justify="flex-end">
                    <Button component={Link} to={"/dashboard/add/"} color="primary"><KeyboardArrowLeftIcon /> Back</Button>
                    {contentId ? <Button onClick={onSubmit} color="primary" type="submit"><SaveIcon /> Update</Button> : <Button onClick={onSubmit} color="primary" type="submit"><SaveIcon /> Save</Button>}
                </Grid>
            </Paper>


        </>

    )
};

export default TextAreaDraftjs;



/**
 * подумать про позиционирование текста, Оно не сохраняется в общий json. Для этого нало хранить отдельно
 */