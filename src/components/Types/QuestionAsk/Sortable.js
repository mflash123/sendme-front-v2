import React, { useState, useCallback, useRef, useContext, useEffect } from 'react';
import { Box,Dialog, Button, DialogContent, DialogTitle, DialogContentText, Grid } from '@material-ui/core';
import {SortableContainer, SortableElement, arrayMove} from 'react-sortable-hoc';
import SortableList from './SortableList'

import {ApiContext} from '../../../context/ApiContext'



const Sortable = ({items,setItems,editCallback}) => {


  const onSortEnd = ({oldIndex, newIndex}) => {
    setItems(arrayMove(items, oldIndex, newIndex))
  };

  const deleteCallback = (props) =>{
    let items_copy = [...items]
    items_copy.splice(props,1)
    setItems(items_copy)
  }

  return (
    <Grid container>
    <SortableList items={items} setItems={setItems} onSortEnd={onSortEnd} deleteCallback={deleteCallback} editCallback={editCallback} lockAxis='y' useDragHandle  />
    </Grid>
  );
}

export default Sortable