import React, {
  useState,
  useCallback,
  useRef,
  useContext,
  useEffect,
} from "react";
import {
  Box,
  Dialog,
  Button,
  DialogContent,
  DialogTitle,
  DialogContentText,
  TextField,
  IconButton,
  FormGroup,
  LinearProgress,
  FormLabel,
  ButtonGroup,
  Divider,
  Breadcrumbs,
  Link,
  Typography,
  Paper,
} from "@material-ui/core";

import { ApiContext } from "../../../context/ApiContext";
import Sortable from "./Sortable";
import {
  FormStyled,
  FormGroupStyled,
  FormLabelStyled,
} from "./../../../styled";

import TextAreaEmoji from "./TextAreaEmoji";
import { useForm, Controller } from "react-hook-form";

const QuestionAsk = (props) => {
  const { edit, contentId } = props;
  const { storeContent, getContent, updateContent } = useContext(ApiContext);
  //const [error,setError] = useState(false)
  const [textareaValue, setTextareaValue] = useState();
  const [sortableItems, setSortableItems] = useState([]);
  const [loading, setLoading] = useState(true);

  const {
    register,
    handleSubmit,
    watch,
    control,
    setValue,
    reset,
    formState: { errors },
  } = useForm();

  const saveForm = () => {
    console.log(sortableItems);
    if (edit) {
      updateContent(sortableItems, contentId).then((value) => {
        console.log(value);
        window.location.replace("/dashboard");
      });
    } else {
      storeContent(sortableItems, 35).then((value) => {
        console.log(value);
        window.location.replace("/dashboard");
      });
    }
  };

  const onSubmit = (data, e) => {
    console.log(data);
    let items = sortableItems;
    items.push({ name: data.question, text: textareaValue });
    setSortableItems(items);

    //Clear form
    reset({
      question: "",
      textarea: "",
    });
    setTextareaValue("");
  };

  const editCallback = (props) => {
    console.log("editCallback");
    console.log(sortableItems[props]);
    setTextareaValue(sortableItems[props].text);
    setValue("question", sortableItems[props].name);
  };

  useEffect(() => {
    if (edit) {
      getContent(contentId).then((value) => {
        console.log(value.data.data);
        setSortableItems(JSON.parse(value.data.data));
        setLoading(false);
      });
    } else {
      setLoading(false);
    }
    // let items = [
    //   {
    //     name:'item 1',
    //     text:'some data 1'
    //   },
    //   {
    //     name:'item 2',
    //     text:'some data 2'
    //   }
    // ]

    // setSortableItems(items)
  }, []);

  console.log(textareaValue)
  return (
    <>
      {loading ? (
        <LinearProgress />
      ) : (
        <Sortable
          items={sortableItems}
          setItems={setSortableItems}
          editCallback={editCallback}
        />
      )}

      <FormStyled onSubmit={handleSubmit(onSubmit)}>
        <Paper elevation={0} style={{ padding: "2rem", marginTop: "2rem" }}>
          <Typography variant="h4" style={{ marginBottom: 4 }}>
            Вопрос - ответ
          </Typography>
          <Divider style={{ marginBottom: 30 }} />

          <FormGroupStyled>
            <FormLabelStyled component="legend">Заголовок</FormLabelStyled>

            <Controller
              control={control}
              name="question"
              rules={{ required: true, minLength: 5, maxLength: 100 }}
              render={({ field }) => (
                <TextField
                  {...field}
                  variant="outlined"
                  fullWidth={true}
                  error={errors?.question}
                  helperText={Boolean(errors?.question) && "Field is required"}
                />
              )}
            />

            {/* <TextField
          error={errors.question}
          variant="outlined"
          inputRef={register({
              required: true,
              maxLength: 100,
              minLength: 5
          })}
          name="question"
          helperText= { errors.question && 'Field is required' }
      /> */}
          </FormGroupStyled>
          <FormGroupStyled>
            <TextAreaEmoji value={textareaValue} setValue={setTextareaValue} />
            <textarea
              {...register("textarea")}
              hidden
              // name="textarea"
              value={textareaValue}
              // ref={register}
            />
          </FormGroupStyled>
          <FormGroupStyled>
            <Box>
              <Button
                size="small"
                color="primary"
                variant="contained"
                type="submit"
                disableElevation
              >
                Добавить элемент
              </Button>
              <Button
                size="small"
                color="primary"
                variant="contained"
                //type="submit"
                disableElevation
                onClick={saveForm}
                style={{ float: "right" }}
              >
                Сохранить
              </Button>
            </Box>
          </FormGroupStyled>
        </Paper>
      </FormStyled>
    </>
  );
};

export default QuestionAsk;
