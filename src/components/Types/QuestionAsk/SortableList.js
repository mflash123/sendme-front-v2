import React, { useState, useCallback, useRef, useContext, useEffect } from 'react';
import { Box,Dialog, Button, DialogContent, DialogTitle, DialogContentText, Paper, Card, CardContent, IconButton, Grid, CardActions, ButtonGroup } from '@material-ui/core';
import {SortableContainer, SortableElement, arrayMove, sortableHandle} from 'react-sortable-hoc';
import DehazeIcon from '@material-ui/icons/Dehaze';
import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from '@material-ui/icons/Edit';
import {ApiContext} from '../../../context/ApiContext'

const DragHandle = sortableHandle(() => <DehazeIcon /> );
//const onSortStart= onSortStart(()=>console.log(1))
const SortableItem = SortableElement(({value,value_ind,deleteCallback,editCallback}) => {
return (
<>
<Grid 
container
direction="row"
justify="center"
alignItems="center"
spacing={3}
>
  <Grid item xs={1}>
    <IconButton>
      <DragHandle />
    </IconButton>
  </Grid>

  <Grid item xs={10}>
    <Card elevation={0}>
<CardContent>{value}</CardContent>
    </Card>
  </Grid>

  <Grid item xs={1}>
    <ButtonGroup variant="text" size="small">
    <IconButton onClick={()=>editCallback(value_ind)}>
      <EditIcon fontSize="small" />
    </IconButton>
    <IconButton onClick={()=>deleteCallback(value_ind)}>
      <DeleteIcon fontSize="small" />
    </IconButton>
    </ButtonGroup>
  </Grid>
</Grid>
</>
)
})

const SortableList = SortableContainer(({items,deleteCallback,editCallback}) => {

  return (
    <ul style={{width:'100%',paddingLeft:0}}>
      {items.map((value, index) =>{
        return <SortableItem key={`item-${index}`} index={index} value_ind={index} value={value.name} deleteCallback={deleteCallback} editCallback={editCallback} />
      }
      )}
    </ul>
  );
});



export default SortableList