import React, { useState } from "react";
import { TextField } from "@material-ui/core";
import "emoji-mart/css/emoji-mart.css";
import { Picker } from "emoji-mart";
import styled from "styled-components";
import InsertEmoticonIcon from '@material-ui/icons/InsertEmoticon';
import IconButton from '@material-ui/core/IconButton';
import {FormStyled,FormGroupStyled,FormLabelStyled} from './../../../styled'



const Textarea = styled(TextField)`
  width: 100%;
  height: 200px !important;
`;

const PickerBtn = styled(IconButton)`
  position: absolute;
  right: 0;
  padding: 10px;
`;

const PickerWrapper = styled.div`
  position: absolute;
  top: -333px;
  right: 0;
`;


const TextAreaEmoji = (props) => {
  //console.log(props)
  const {value, setValue} = props

  const [emojiPickerState, SetEmojiPicker] = useState(false);
  //const [value, setValue] = useState();
  const [cursorPostion, setCursorPosition] = useState([0, 0]);


  const setEmoji = (emoji) => {
    const start = value.substring(0, cursorPostion[1]);
    const end = value.substring(cursorPostion[1]);
    setValue(start + emoji.native + end);
    SetEmojiPicker(!emojiPickerState);
  };

  let emojiPicker;
  if (emojiPickerState) {
    emojiPicker = (
      <PickerWrapper>
        <Picker
        set='apple'
        showSkinTones={false}
        showPreview={false}
        useButton={false}
        theme='light'
        sheetSize={32}
        native={true}
        onSelect={(emoji) => setEmoji(emoji)}
        />
      </PickerWrapper>
    );
  }
  function triggerPicker(event) {
    event.preventDefault();
    SetEmojiPicker(!emojiPickerState);
  }

  return (
    <div style={{position:'relative'}}>
      <FormLabelStyled component="legend">Ваш текст</FormLabelStyled>
      <TextField
        //label="Ваш текст"
        //placeholder="Ваш текст"
        variant="outlined"
        value={value}
        multiline
        rows={6}
        autoFocus={true}
        fullWidth={true}
        onChange={(event) => {
          setValue(event.target.value);
        }}
        onBlur={(e) => {
          setCursorPosition([e.target.selectionEnd, e.target.selectionStart]);
        }}
      />
      <PickerBtn onClick={triggerPicker}>
        
        <InsertEmoticonIcon />
      </PickerBtn>
      {emojiPicker}
    </div>
  );
}

export default TextAreaEmoji