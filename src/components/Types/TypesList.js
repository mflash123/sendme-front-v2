import React, { useState, useContext, useEffect } from 'react'
import { Button, Box, CircularProgress, Paper, Breadcrumbs, Typography } from '@material-ui/core'
import {ChevronRight} from '@material-ui/icons';


import {ApiContext} from '../../context/ApiContext'
import { useParams, Link } from 'react-router-dom';

const TypesList = () => {
    let { groupId } = useParams();

    const [types,setTypes] = useState(false)
    const {typesGroup} = useContext(ApiContext);

    useEffect(()=>{
        const data = async () =>{
            const resp = await typesGroup(groupId)
            if(typeof(resp)=='undefined') return
            console.log(resp)
            setTypes(resp.data)
        }
        data()
    },[])

    console.log(types)

    const TypeList = () => types.map((value,index)=>(
        <Box my={1} key={index}>
            <Paper elevation={0}>
                <Button component={Link} to={"/dashboard/add/"+groupId+'/'+value.id} fullWidth={true} style={{paddingTop:20,paddingBottom:20}} size="large">{value.humanName} <ChevronRight /></Button>
        </Paper>
        </Box>
    ))

    if(types){
    // var groupName='%GroupName%';
    // if(groupId==1){
    //     groupName='Социальные сети';
    // } else if(groupId==2){
    //     groupName='Мессенджеры';
    // }
    // else if(groupId==7){
    //     groupName='Изображение';
    // }
    // else if(groupId==13){
    //     groupName='Интеграции';
    // }
        return(
            <TypeList />
        )
    }
    
    return <center><CircularProgress /></center>
}

export default TypesList