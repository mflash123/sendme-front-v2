import React, { useContext, useState, useEffect } from 'react'
import { useParams, useHistory } from 'react-router-dom';


import { TextField, Typography, Button, Link, Breadcrumbs } from '@material-ui/core';
import ArrowBackIosIcon from '@material-ui/icons/ArrowBackIos';

import TypeItemFields from './Buttons/TypeItemFields'

import {DragItemOrderContext} from '../../context/DragItemOrderContext'
import {ApiContext} from '../../context/ApiContext'
import FooterItem from './../../layout/FooterItem'
import Product from './../Types/Product/Product'

const TextAreaContentDeprecated = React.lazy(() => import('./TextArea/TextAreaContent'));
const AvatarCrop = React.lazy(() => import('./AvatarCrop/AvatarCrop'));
const CustomButton = React.lazy(() => import('./CustomButton'));
const ProductList = React.lazy(() => import('./Product/ProductList'));
const Integration = React.lazy(() => import('./Integrations/Integration'));
const QuestionAsk = React.lazy(() => import('./QuestionAsk/QuestionAsk'));
const Youtube = React.lazy(() => import('./Youtube/Youtube'));
const TextAreaContent = React.lazy(() => import('./TextAreaDraftjs'));





const Type = (props) => {
    let { typeId,contentId,productId } = useParams();
    const {productedit} = props

    console.log(contentId)
    /** option: new/edit */
    

    typeId = parseInt(typeId)
    productId = parseInt(productId)

    const edit = isNaN(productId) ?
        typeof(contentId)==='undefined' ?
            false
            :
            true
        :
            true
 

    console.log(edit)
   // const {userResponse,storeContent} = useContext(ApiContext) //only for count
    const {updateContent,getContent,getProducts,getProduct} = useContext(ApiContext) //ed

    

 //   const [input,setInput] = useState(false); //false was
  //  const [count,setCount] = useState()
    const [content,setContent] = useState(false);
    const [items,setItems] = useState(false)
 //   const {items, setItems} = useContext(DragItemOrderContext) //needs for Guest rerender

    console.log(content)
    if(edit){
        if(content){
            typeId = content.type_id
            //console.log(typeId)
            if(typeId===29){
                //productlist
                console.log(content)
            }
        }
    }

    console.log(items)
    
    /** Edit data fetch */
    useEffect(()=>{
        if(edit){
            /**
             * searching index number for edit
             *  */    
            // userResponse.data.items.map((value,index)=>{
            //     if(value.id===contentId) setCount(index);
            // })
            if(isNaN(productId)){
                getProducts(contentId).then(value=>{
                    if(typeof(value)!='undefined'){
                    if(!items){
                        setItems(value.data)
                    }
                    
                    }
                // console.log(value)
                })
            } else {
                getProduct(productId).then(value=>{
                    console.log(value)
                    if(typeof(value)!='undefined'){
                       if(!items){
                         setItems(value.data)
                       }
                      }
                })
            }

            const data = async () =>{
                let resp = await getContent(contentId)
                isNaN(productId) && setContent(resp.data)
                //console.log(resp.data)
            }
            data()
        }

    },[])


    let history = useHistory();

    return(
        <>
        {/* <Link style={{
            display: 'flex',
            alignItems: 'center',
            flexWrap: 'wrap'
        }} href="#" onClick={()=>history.goBack()}><ArrowBackIosIcon />Назад</Link> */}
        { [21].includes(typeId) && <CustomButton content={content} edit={edit} typeId={typeId} /> }
        { [26].includes(typeId) && <TextAreaContentDeprecated content={content} edit={edit} typeId={typeId} /> }
        { [27].includes(typeId) && <AvatarCrop content={content} edit={false} typeId={typeId} /> } {/** NO EDIT */}
        { [5,6,7,8,9,10,11,15,16,17,19,20,24,38].includes(typeId) && <TypeItemFields content={content} edit={edit} typeId={typeId} /> }
        { [29].includes(typeId) && <ProductList items={items} content={content} edit={edit} typeId={typeId} /> }
        { productedit && <Product items={items} edit={edit} /> }
        { [32,33,34].includes(typeId) && <Integration typeId={typeId} /> }
        { [35].includes(typeId) && <QuestionAsk typeId={typeId} edit={edit} contentId={contentId} /> }
        { [36].includes(typeId) && <Youtube typeId={typeId} edit={edit} contentId={contentId} /> }
        { [37].includes(typeId) && <CustomButton contentId={contentId} /> }
        { [39].includes(typeId) && <TextAreaContent contentId={contentId} /> }
        </>
    )
}



export default Type