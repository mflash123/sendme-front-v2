import React, { useState } from 'react';

import 'emoji-mart/css/emoji-mart.css'
//import {Picker} from 'emoji-mart';
import { Picker } from 'emoji-mart/dist-modern/index.js'

const Emoji = ({callback}) => {
    //const {callback} = props

   // const [open,setOpen] = useState(true)
   console.log(callback)

    return (
        <>

            <Picker
                set='apple'
                showSkinTones={false}
                showPreview={false}
                useButton={false}
                theme='light'
                onSelect={callback}
                sheetSize={32}
                native={true}
              //  onClick={()=>{setOpen(false)}}

            />
        
        {/* <button onClick={()=>{setOpen(true)}}>emoji</button> */}
        </>
    );
}

// const arePropsEqual = (prevProps, nextProps) => {
//     return true
//   };

export default React.memo(Emoji)
