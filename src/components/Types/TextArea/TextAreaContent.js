import React, { useState, useContext } from "react";

import NativeSelect from "@material-ui/core/NativeSelect";
import {
  Box,
  Button,
  Breadcrumbs,
  Typography,
  IconButton,
  TextField,
  TextareaAutosize,
  Paper,
  Grid,
} from "@material-ui/core";
import Emoji from "./Emoji";
import FooterItem from "../../../layout/FooterItem";
import { ApiContext } from "./../../../context/ApiContext";
import "emoji-mart/css/emoji-mart.css";
import { Picker } from "emoji-mart";
import InsertEmoticon from "@material-ui/icons/InsertEmoticon";
import "./../../../textfieldemoji.css";
import { useStyles } from "../../../style";
import { Link } from "react-router-dom";
import KeyboardArrowLeftIcon from "@material-ui/icons/KeyboardArrowLeft";
import { Alert } from '@material-ui/lab';

const TextAreaContent = (props) => {
  const classes = useStyles();
  const { storeContent, updateContent, getContent } = useContext(ApiContext);
  const { edit, content, typeId } = props;

  let defaultValue = "";
  /** Edit preparation */
  if (edit) defaultValue = JSON.parse(content.data).name;

  const [value, setValue] = useState(defaultValue);

  const [emojiPickerState, SetEmojiPicker] = useState(false);
  //const [value, setValue] = useState("");
  const [cursorPostion, setCursorPosition] = useState([0, 0]);

  const setEmoji = (emoji) => {
    const start = value.substring(0, cursorPostion[1]);
    const end = value.substring(cursorPostion[1]);
    setValue(start + emoji.native + end);
    SetEmojiPicker(!emojiPickerState);
  };

  let emojiPicker;
  if (emojiPickerState) {
    emojiPicker = (
      <div className="picker-wrapper">
        <Picker
          set="apple"
          showSkinTones={false}
          showPreview={false}
          useButton={false}
          theme="light"
          sheetSize={32}
          native={true}
          onSelect={(emoji) => setEmoji(emoji)}
        />
      </div>
    );
  }
  function triggerPicker(event) {
    event.preventDefault();
    SetEmojiPicker(!emojiPickerState);
  }

  const handleSave = () => {
    if (edit) {
      console.log("updateBD");
      updateContent({ name: value }, content.id).then(() => {
        window.location.replace("/dashboard");
      });
    } else {
      console.log("saveBD");
      console.log({ name: value });
      storeContent({ name: value }, typeId).then((value) => {
        console.log(value);
        window.location.replace("/dashboard");
      });
    }
  };

  return (
    <>
      <Paper elevation={0} className={classes.paperfield}>
      <Alert severity="warning">This is deprecated feature. <Button component={Link} to={"/dashboard/add/"} color="primary">Use a new one from Dashboard 👌</Button></Alert>
        <Box className="wrapper">
          <TextField
            name="name"
            label="Ввести текст"
            placeholder="Ваш текст"
            className="textfield"
            value={value}
            multiline
            autoFocus={true}
            fullWidth={true}
            onChange={(event) => {
              setValue(event.target.value);
            }}
            onBlur={(e) => {
              setCursorPosition([
                e.target.selectionEnd,
                e.target.selectionStart,
              ]);
            }}
          />

          <IconButton className="picker-btn" onClick={triggerPicker}>
            <InsertEmoticon />
          </IconButton>

          {emojiPicker}
        </Box>
        {/* <FooterItem handleSave={handleSave} /> */}
        <Grid container justify="flex-end">
          <Button component={Link} to={"/dashboard/add/"} color="primary">
            <KeyboardArrowLeftIcon /> Back
          </Button>
        </Grid>
      </Paper>
    </>
  );
};

export default TextAreaContent;
