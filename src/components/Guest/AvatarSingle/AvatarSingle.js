import React from 'react'
import Avatar from '@material-ui/core/Avatar';
import PropTypes from 'prop-types';


const AvatarSignle = ({fileName}) => {
    return(
        <Avatar src={process.env.REACT_APP_STORAGE_URL+fileName} style={{margin:'auto',width:100,height:100}} />
    )
}


export default AvatarSignle

AvatarSignle.defaultProps = {
    fileName: ''
  };

  AvatarSignle.propTypes = {
    fileName: PropTypes.string
  };