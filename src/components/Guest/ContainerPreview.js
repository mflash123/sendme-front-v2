import React from 'react'

import { Grid } from '@material-ui/core';


const ContainerPreview = props => {
    const {adminView} = props

    if(adminView){
        return(
            <Grid
            container
            direction="row"
            justify="center"
        //   alignItems="center"
        //   style={{height:'100%'}}
            style={{top: '5rem',position: 'sticky'}} 
            >
                <Grid item
                style={{
                    pointerEvents: 'none',
                    //backgroundColor: 'red',
                    position: 'relative',
                    margin: '0 auto',
            //     height: '100%',
                    minHeight: 500,
                    width: 415,
                    display: 'inline-block',
                    textAlign: 'left',
                    borderRadius: '1rem',
                    padding: '.7rem',
                    background: 'linear-gradient(45deg,#444,#111)',
                   // boxShadow: '0 0px 30px rgba(0,0,0,0.20)',
                    border: '.1rem solid #444546'
                }}
                >
                    <div style={{
                            backgroundColor: '#fafafa',
                            overflow: 'hidden',
                            width: '100%',
                            height: '100%',
                            borderRadius: '1rem'

                    }}
                    >
                        {props.children}
                    </div>
                </Grid>
            </Grid>
        )} else {
            return (
                <Grid
               container
               direction="row"
                justify="center"
                >
                    <Grid item
                    style={{width:415,
                    // boxShadow: '0 16px 24px 2px rgba(0, 0, 0, 0.14), 0 6px 30px 5px rgba(0, 0, 0, 0.12), 0 8px 10px -5px rgba(0, 0, 0, 0.2)'
                }}
                    >
                        {props.children}
                    </Grid>
                </Grid>
                )
        }
}

export default ContainerPreview