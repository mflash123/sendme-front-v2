import { Button, Dialog, DialogActions, DialogContent, DialogTitle, TextField, Typography } from '@material-ui/core';
import { Alert } from '@material-ui/lab';
import React from 'react';
import { Controller, useForm } from 'react-hook-form';
import { ApiContext } from "./../../../context/ApiContext";

interface IProps {
    open: boolean;
    setOpen: (open: boolean) => void,
    username: string,
}

type IFormInputs = {
    email: string,
    message: string,
    username: string
};

export const Report = ({ open, setOpen, username }: IProps) => {

    const [isSuccess, setSuccess] = React.useState(false)
    const [isError, setError] = React.useState(false)

    const defaultValues = {
        email: '',
        message: '',
        username: username,
    }

    const { register, control, handleSubmit, formState: { errors } } = useForm<IFormInputs>({ defaultValues });
    const { userReport } = React.useContext(ApiContext);

    const onSubmit = (data: IFormInputs) => {
        userReport(data)
            .then((data: any) => {
                console.log(data)
                setSuccess(true)
                setTimeout(() => setOpen(false), 5000);
            })
            .catch(() => {
                setError(true)
            })
    }




    return (
        <Dialog open={open}>
            {isError && <Alert severity="error">Oops, что-то пошло не так. Напишите об этом в поддержку 💔</Alert>}
            {isSuccess && <Alert severity="success">Спасибо, мы скоро отреагируем на обращение 👌</Alert>}

            <form onSubmit={handleSubmit(onSubmit)}>
                <input {...register("username", { required: true })} hidden />

                <DialogTitle disableTypography={true} style={{ fontWeight: 'bold' }}>Сообщить о нарушении 🤯</DialogTitle>
                <DialogContent>
                    <Typography>Вам кажется, что эта страница нарушает правила или контент вызывает сомнения?</Typography>
                    <Typography>Опишите нарушение в свободной форме и не забудьте оставить Ваш email, куда мы сообщим о результате проверки.</Typography>
                </DialogContent>
                <DialogContent>

                    <Controller
                        name="message"
                        control={control}
                        rules={{
                            required: 'Опишите нарушение',
                            minLength: {
                                value: 25,
                                message: "Опишите нарушение подробнее..."
                            }
                        }}
                        render={({ field: props }) => (
                            <TextField
                                required
                                error={!!errors.message?.message}
                                {...props}
                                label="Опишите нарушение"
                                fullWidth
                                multiline
                                onChange={(e) => props.onChange(e.target.value)}
                                helperText={errors.message?.message}
                            />
                        )}

                    />

                    <Controller
                        name="email"
                        control={control}
                        rules={{
                            required: 'Введите email',
                        }}
                        render={({ field: props }) => (
                            <TextField
                                required
                                error={!!errors.email?.message}
                                {...props}
                                label="Ваш email"
                                type="email"
                                onChange={(e) => props.onChange(e.target.value)}
                                helperText={errors.email?.message}
                            />
                        )}

                    />

                </DialogContent>
                <DialogActions>
                    <Button type="submit">Сообщить о нарушении ➡️</Button>
                    <Button onClick={() => setOpen(false)}>Передумал</Button>
                </DialogActions>


            </form>
        </Dialog>)
}