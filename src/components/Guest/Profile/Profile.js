import React, { useContext, useCallback } from "react";

import { ApiContext } from "../../../context/ApiContext";
import { DragItemOrderContext } from "../../../context/DragItemOrderContext";
import styled from "styled-components";

import Block from "../Blocks/Blocks";
import { Avatar, Box } from "@material-ui/core";
import YandexAdverb from "../../Adverb/YandexAdverb";
import { LazyLoadComponent } from "react-lazy-load-image-component";
import QuestionAsk from "../Blocks/QuestionAsk/QuestionAsk";

import YouTube from "react-youtube";

const Profile = ({ items, adv }) => {
  const AvatarTopArray = items.filter((value) => {
    if (value.type_id === 27) {
      console.log(value);
      if (JSON.parse(value.data).type === "avatarTop") return true;
    }
  });

  const BannerTopArray = items.filter((value) => {
    if (value.type_id === 27) {
      console.log(value);
      if (JSON.parse(value.data).type === "bannerTop") return true;
    }
  });

  const AvatarTop = (props) =>
    AvatarTopArray.map(
      (value, index) => {
        /** banner->setup marginTop */
        const { banner } = props;
        let mt = 100;
        if (banner) mt = 0;

        let fileName = JSON.parse(value.data).fileName;
        return (
          <Box
            justifyContent="center"
            display="flex"
            style={{
              position: "relative",
              top: -100,
              marginTop: mt,
              marginBottom: -100,
            }}
          >
            <Avatar
              style={{ width: 200, height: 200 }}
              src={process.env.REACT_APP_STORAGE_URL + fileName}
            />
          </Box>
        );
      }
      // console.log(value)
    );

  const StyledImg = styled.img`
    width 100%;
    align-self: flex-start;  /* height ratio */
    `;

  const BannerTop = () =>
    BannerTopArray.map((value, index) => {
      let fileName = JSON.parse(value.data).fileName;
      return (
        <Box justifyContent="center" display="flex">
          <StyledImg src={process.env.REACT_APP_STORAGE_URL + fileName} />
        </Box>
      );
    });

  console.log(BannerTopArray.length);
  const bannerMargin = BannerTopArray.length > 0;

  const Blocks = useCallback(
    () =>
      items.map((value, index) => {
        /** avatarTop remove from list, because exist external component */
        if (value.type_id === 27) {
          if (JSON.parse(value.data).type === "avatarTop") {
            return false;
          }
        }
        /** avatarTop */

        return <Block value={value} key={index} />;
      }),
    [items]
  );

  console.log(bannerMargin);
  return (
    <LazyLoadComponent>
      <BannerTop />
      <AvatarTop banner={bannerMargin} />

      <Blocks />
      {adv && <YandexAdverb />}

      <QuestionAsk />
    </LazyLoadComponent>
  );
};

const arePropsEqual = (prevProps, nextProps) => {
  console.log(prevProps, nextProps);
  if (prevProps.items === nextProps.items) {
    return true;
  }
};

export default React.memo(Profile, arePropsEqual);
