import React, { useContext, useEffect, useState } from "react";
import { useParams, Switch, Route, useRouteMatch } from "react-router-dom";
import ReactPixel from "react-facebook-pixel";
import { Helmet } from "react-helmet";
import TagManager from "react-gtm-module";
import CloseIcon from "@material-ui/icons/Close";
import styled from "styled-components";

import {
  Grid,
  Paper,
  Avatar,
  LinearProgress,
  Button,
  CssBaseline,
  Breadcrumbs,
  Link,
  Fab,
  ButtonGroup,
  Modal,
  Fade,
  Backdrop,
  IconButton,
} from "@material-ui/core";

import ContainerPreview from "./ContainerPreview";
import { Profile } from "./Profile";
import AvatarSignle from "./AvatarSingle/AvatarSingle";

import { DragItemOrderContext } from "../../context/DragItemOrderContext";
import useScript from "../../hook/useScript";

import { ThemeProvider } from "@material-ui/core/styles";
import getTheme from "./../../Themes";

/** Unauthorized zone  */
import { ApiContext } from "../../context/ApiContext";
/** Unauthorized zone  */

import Products from "./Blocks/Products";
import ProductView from "./Blocks/ProductView";
import { Alert, AlertTitle } from "@material-ui/lab";
import { ReportOutlined } from "@material-ui/icons";
import { Report } from "./Report";

const GuestThemePicker = React.lazy(() =>
  import("./../Dashboard/GuestThemePicker")
);

const ModalActive = () => {
  return (
    <Modal
      style={{
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
      }}
      open={true}
      closeAfterTransition
      BackdropComponent={Backdrop}
      BackdropProps={{
        timeout: 500,
      }}
    >
      <Fade in={true}>
        <Alert severity="warning">
          <AlertTitle>🙅‍♂️</AlertTitle>
          Проверьте Вашу почту —{" "}
          <strong>Вам нужно подтвердить Ваш email</strong>
        </Alert>
      </Fade>
    </Modal>
  );
};

const Guest = ({ guest, user, username }) => {
  //guest indicates admin/quest view

  console.log(user);

  const [themeState, setThemeState] = useState(getTheme("DefaultTheme"));
  const [isReport,setReport] = React.useState(false)
  //const [themeState,setThemeState] = useState(getTheme(theme))
  /**
   * Google Tag Manager for sendme use only if quest==true
   */
  const tagManagerArgs = {
    gtmId: "GTM-K8BXDRX",
  };

  const { getIntegration, getUser, setTheme, getThemeApi } = useContext(
    ApiContext
  );
  const [fbpixel, setFbpixel] = useState(false);
  const [gtm, setGtm] = useState(false);
  const [active, setActive] = useState(true);
  const [avatar, setAvatar] = useState({ fileName: "", type: "" });

  let { path, url } = useRouteMatch();
  console.log(path);
  //const { username } = useParams(); //username = undefined -> dashboard

  /** Unauthorized zone  */

  /** FBPIXEL */
  const advancedMatching = {}; // optional, more info: https://developers.facebook.com/docs/facebook-pixel/advanced/advanced-matching
  const options = {
    autoConfig: true, // set pixel's autoConfig. More info: https://developers.facebook.com/docs/facebook-pixel/advanced/
    debug: true, // enable logs
  };

  const { items, setItems } = useContext(DragItemOrderContext);
  //const [frame,setFrame] = useState( typeof(username)==='undefined' ? true : false)
  const [adv, setAdv] = useState(guest ? true : false);

  /**
   * Theme apply - хочу вынести выше
   */
  // useEffect(()=>{
  //   let theme_name = 'DefaultTheme'
  //   setThemeState( getTheme(theme_name) )
  // },[])

  useEffect(() => {
    if (guest) {
      TagManager.initialize(tagManagerArgs);

      async function fetchData(username) {
        const result = await getIntegration(username);
        console.log(result);

        if (Object.keys(result.data).length > 0) {
          result.data.map((val) => {
            if (val.type_id == 32) setFbpixel(val.data);
            if (val.type_id == 33) setGtm(val.data);
            //  if(val.type_id==34) setYamap(val.data)
          });
          console.log(result.data);
          // setFbpixel(result.data[0].data)
        }
      }
      fetchData(username);
    } else {
      getThemeApi().then((val) => {
        setThemeState(getTheme(val.data.guest_theme));
      });
    }
  }, [guest]);

  if (guest && fbpixel) {
    let px = JSON.parse(fbpixel);

    // console.log(px.fbpixel)
    ReactPixel.init(px.fbpixel, advancedMatching, options);
    ReactPixel.pageView(); // For tracking page view
  }

  if (guest && gtm) {
    let px = JSON.parse(gtm);
    TagManager.initialize({ gtmId: px.gtm });
    console.log(px.gtm);
  }

  /** Unauthorized zone  */
  if (guest) {
    getUser(username).then((value) => {
      console.log(username);
      console.log(value);
      if (typeof value != "undefined") {
        if (!items) {
          /** проверяет доступные эллементы в тарифе */
          const elements = value.data.items.filter((val) => {
            if (val.enabled) {
              return val;
            }
          });

          //  console.log(elements)
          //console.log(value.data.isActive)
          setActive(value.data.isActive);
          setItems(elements);
          console.log(value.data.avatar);
          if (value.data.avatar !== null) {
            setAvatar(value.data.avatar.data);
          }
        }
        if (value.data.tarif.id != 1) setAdv(false);
        // console.log(value.data.tarif.id)

        //Применяю тему
        setThemeState(getTheme(value.data.theme.guest));
      }
    });
  }

  const handlePickGuestTheme = (e) => {
    setTheme("guest", e.target.name).then(
      setThemeState(getTheme(e.target.name))
    );
  };

  const logo = () => {
    if (themeState.palette.type === "light") {
      return "https://sendme.cc/static/light.png";
    } else {
      return "https://sendme.cc/static/dark.png";
    }
  };

  return (
    <>
      {!guest && (
        <GuestThemePicker handlePickGuestTheme={handlePickGuestTheme} />
      )}

      <ThemeProvider theme={themeState}>
        {guest && !active && <ModalActive />}
        {/* 
          {guest && <CssBaseline />} - примениет стили глобально, даже на родительский компонент
          Чтобы ибежать этого, сделал такой костыль
        */}
        {guest && <CssBaseline />}
        <Switch>
          <Route exact={guest} path={path}>
            {guest && (
              <Helmet>
                <title>{username}</title>
              </Helmet>
            )}
            <ContainerPreview adminView={!guest}>
              <Paper style={{ padding: 15 }} elevation={10}>
                {guest
                  ? avatar.fileName && (
                      <AvatarSignle fileName={avatar.fileName} />
                    )
                  : user.avatar && (
                      <AvatarSignle fileName={user.avatar.data.fileName} />
                    )}

                {items ? (
                  <Profile adv={adv} items={items} />
                ) : (
                  <LinearProgress />
                )}
              </Paper>
              {guest && (
                <Grid container direction="row" justify="space-between">
                  <a href="https://sendme.cc/?utm_source=profile&utm_medium=logo&utm_campaign=footer">
                    <img
                      style={{ width: 100, padding: "15px 0 0 15px" }}
                      src={logo()}
                    />
                  </a>

                  <IconButton onClick={()=>setReport(true)}>
                    <ReportOutlined fontSize="small" />
                  </IconButton>
                  <Report open={isReport} setOpen={setReport} username={username} />
                </Grid>
              )}
            </ContainerPreview>
          </Route>

          {/* <Route exact path={`${match.path}/product/:productId`}> */}
          <Route path={`${path}/product/:productId`}>
            {guest && (
              <Helmet>
                <title>Магазин {username}</title>
              </Helmet>
            )}
            <ContainerPreview>
              {/* <Grid
                justify="space-between"
                container
                spacing={24}
              >
                <Grid item></Grid>
                <Grid item>
                  <Button size="small" href={"/" + username}>
                    <CloseIcon />
                  </Button>
                </Grid>
              </Grid> */}

              <ProductView />
              <a href="https://sendme.cc/?utm_source=profile&utm_medium=logo&utm_campaign=footer">
                <img
                  style={{ width: 100, padding: "15px 0 0 15px" }}
                  src={logo()}
                />
              </a>
              <span
                style={{ fontSize: ".1em", display: "block", paddingLeft: 15 }}
              >
                {process.env.REACT_APP_VERSION}
              </span>
            </ContainerPreview>
          </Route>
        </Switch>
      </ThemeProvider>
    </>
  );
};

export default Guest;

// const arePropsEqual = (prevProps, nextProps) => {
//   console.log(12313000)
//   console.log(prevProps,nextProps)
//   // if (prevProps.blobs === nextProps.blobs) {
//   //   return true;
//   // }
// };

// export default React.memo(Guest, arePropsEqual);
