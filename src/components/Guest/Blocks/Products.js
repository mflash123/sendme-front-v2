import React, { useContext, useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import GridList from "@material-ui/core/GridList";
import GridListTile from "@material-ui/core/GridListTile";
import GridListTileBar from "@material-ui/core/GridListTileBar";
import ListSubheader from "@material-ui/core/ListSubheader";
import IconButton from "@material-ui/core/IconButton";
import InfoIcon from "@material-ui/icons/Info";
//import tileData from './tileData';
import { ApiContext } from "./../../../context/ApiContext";
import { useParams } from "react-router-dom";
import { LinearProgress, Grid, Button } from "@material-ui/core";
import Image from "material-ui-image";
import VisibilityIcon from "@material-ui/icons/Visibility";
import { Link } from "react-router-dom";

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
    flexWrap: "wrap",
    justifyContent: "space-around",
    overflow: "hidden",
    backgroundColor: theme.palette.background.paper,
  },
  gridList: {
    width: 500,
    height: 450,
  },
  icon: {
    color: "rgba(255, 255, 255, 0.54)",
  },
}));

const Preload = () => {
  return <LinearProgress />;
};

const Products = (props) => {
  const { username } = useParams(); //username = undefined -> dashboard

  const [items, setItems] = useState(false);
  const classes = useStyles();

  const { content_id } = props;

  const { getProducts } = useContext(ApiContext);

  getProducts(content_id).then((value) => {
    if (typeof value != "undefined") {
      if (!items) {
        setItems(value.data);
      }
    }
    // console.log(value)
  });
  // if(items){
  // items.map((item)=>{
  //   console.log(item)
  // })}

  return (
    <>
      {items ? (
        <Grid container spacing={1}>
          {items.map((item, index) => (
            <Grid item xs={6} key={index}>
              <Link to={username + "/product/" + item.id}>
                <Image
                  src={
                    process.env.REACT_APP_API_URL +
                    JSON.parse(item.data).attach.images[0]
                  }
                  style={{ width: "100%", height: "100%" }}
                />
              </Link>
            </Grid>
          ))}
        </Grid>
      ) : (
        <Preload />
      )}
    </>
  );
};

export default Products;
