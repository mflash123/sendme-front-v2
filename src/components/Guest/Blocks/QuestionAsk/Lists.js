import React, { useState, useEffect } from 'react';
import { Button, FormHelperText,FormControl, Fade, List, ListSubheader, ListItem, ListItemText, Collapse, ListItemIcon,LinearProgress, Typography } from '@material-ui/core';
import ExpandLess from '@material-ui/icons/ExpandLess';
import ExpandMore from '@material-ui/icons/ExpandMore';

const Lists = ({value}) => {

    const [open, setOpen] = useState(false);

    const handleClick = () => {
        setOpen(!open);
      };

    return(
 <>
        <ListItem button onClick={()=>handleClick()}>
        <ListItemText
        disableTypography
        primary={
          <Typography style={{fontWeight:'bolder'}}>{value.name}</Typography>
          }/>
        {open ? <ExpandLess /> : <ExpandMore />}
        </ListItem>
        <Collapse in={open} timeout="auto" unmountOnExit>
          <List component="div" disablePadding>
            <ListItem>
              <Typography style={{whiteSpace:'pre-line'}}>{value.text}</Typography>
            {/* <ListItemText primary={value.text} style={{whiteSpace:'break-spaces;'}} /> */}
            </ListItem>
          </List>
        </Collapse>
        </>
      


    )
}

export default Lists