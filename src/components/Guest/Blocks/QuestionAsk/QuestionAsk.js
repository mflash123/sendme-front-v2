import React, { useState, useEffect } from "react";
import {
  List,
} from "@material-ui/core";

import Lists from "./Lists";

const QuestionAsk = (props) => {
  const { data } = props;
  //const {getContent} = useContext(ApiContext)
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    if (typeof data !== "undefined") {
      setLoading(false);
    }
  }, [data]);

  console.log(loading, data);

  if (!loading) {
    console.log(JSON.parse(data));
  }
  return (
    <>
      {!loading && (
        <List component="div" style={{ width: "100%" }}>
          {JSON.parse(data).map((val, index) => {
            return <Lists key={index} value={val} />;
          })}
        </List>
      )}
    </>
  );
};

export default QuestionAsk;
