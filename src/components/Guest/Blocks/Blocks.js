import React, { useCallback, Suspense } from "react";

import {
  Grid,
  Avatar,
  Typography,
  Box,
  Button,
  LinearProgress,
} from "@material-ui/core";
import {
  Instagram,
  WhatsApp,
  Telegram,
  Facebook,
  Link,
  Phone,
  Mail,
  YouTube as YouTubeIcon,
} from "@material-ui/icons";
import YouTube from "react-youtube";
import styled from "styled-components";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faTiktok } from "@fortawesome/free-brands-svg-icons";
import Editor from "draft-js-plugins-editor";
import { convertFromRaw, EditorState } from "draft-js";

import Ava from "./Avatar";

import CustomButton from "./CustomButton";

import Products from "./Products";
import CustomButtonV2 from "./CustomButtonv2";
import {TextAreaDraftjs} from "./TextAreaDraftjs";
//import YandexMap from './YandexMap'
const YandexMap = React.lazy(() => import("./YandexMap"));
const QuestionAsk = React.lazy(() => import("./QuestionAsk/QuestionAsk"));

/**
 *
 * @param {data:string} props
 * Этот компонент возвращает уже готовый блок для Guest component. Подбирает блок самостоятельно, исходя из типа
 */

const CenterIcon = styled.div`
  display: flex;
  align-items: center;
  flex-wrap: wrap;
`;

const Blocks = (props) => {
  try {
    const { id, data } = props.value;
    console.log(props);
    const { name, destination } = JSON.parse(data);
    const { fileName, type } = JSON.parse(data); //for ava
    const opts = {
      //height: '100%',
      width: "100%",
      playerVars: {
        // https://developers.google.com/youtube/player_parameters
        //autoplay: 1,
        frameborder: 0,
        controls: 0,
        mute: 1,
        showinfo: 0,
        modestbranding: 1,
        autohide: 1,
        disablekb: 1,
      },
    };

    const Item = () => {
      switch (props.value.type_id) {
        case 5:
          //instagram
          return (
            <Button
              href={destination}
              variant="contained"
              color="primary"
              size="large"
              style={{ width: "100%", backgroundColor: "#e1306c" }}
            >
              <CenterIcon>
                <Instagram /> <Typography>{name}</Typography>
              </CenterIcon>
            </Button>
          );
        case 6:
          //whatsapp
          return (
            <Button
              href={"https://api.whatsapp.com/send?phone=" + destination}
              variant="contained"
              color="primary"
              size="large"
              style={{ width: "100%", backgroundColor: "#128c7e" }}
            >
              <CenterIcon>
                <WhatsApp />
                <Typography> {name}</Typography>
              </CenterIcon>
            </Button>
          );
        case 7:
          //telegram
          return (
            <Button
              href={destination}
              variant="contained"
              color="primary"
              size="large"
              style={{ width: "100%", backgroundColor: "#0088cc" }}
            >
              <CenterIcon>
                <Telegram />
                <Typography> {name}</Typography>
              </CenterIcon>
            </Button>
          );
        case 8:
          //skype
          return (
            <Button
              href={destination}
              variant="contained"
              color="primary"
              size="large"
              style={{ width: "100%", backgroundColor: "#00aff0" }}
            >
              <CenterIcon>
                <Typography>
                  <i className="fab fa-skype"></i> {name}
                </Typography>
              </CenterIcon>
            </Button>
          );
        case 9:
          //vk
          return (
            <Button
              href={destination}
              variant="contained"
              color="primary"
              size="large"
              style={{ width: "100%", backgroundColor: "#45668e" }}
            >
              <CenterIcon>
                <Typography>
                  <i className="fab fa-vk"></i> {name}
                </Typography>
              </CenterIcon>
            </Button>
          );
        case 10:
          //fb
          return (
            <Button
              href={destination}
              variant="contained"
              color="primary"
              size="large"
              style={{ width: "100%", backgroundColor: "#3b5998" }}
            >
              <CenterIcon>
                <Facebook />
                <Typography> {name}</Typography>
              </CenterIcon>
            </Button>
          );
        case 11:
          //site
          return (
            <Button
              href={destination}
              variant="contained"
              color="primary"
              size="large"
              style={{ width: "100%" }}
            >
              <CenterIcon>
                <Link />
                <Typography> {name}</Typography>
              </CenterIcon>
            </Button>
          );
        case 15:
          //viber
          return (
            <Button
              href={"viber://chat?number=" + destination}
              variant="contained"
              color="primary"
              size="large"
              style={{ width: "100%", backgroundColor: "#59267c" }}
            >
              <CenterIcon>
                <Typography>
                  <i className="fab fa-viber"></i> {name}
                </Typography>
              </CenterIcon>
            </Button>
          );
        case 16:
          //tel
          return (
            <Button
              href={"tel:" + destination}
              variant="contained"
              color="primary"
              size="large"
              style={{ width: "100%", backgroundColor: "#68217a" }}
            >
              <CenterIcon>
                <Phone />
                <Typography> {name}</Typography>
              </CenterIcon>
            </Button>
          );
        case 17:
          //email
          return (
            <Button
              href={"mailto:" + destination}
              variant="contained"
              color="primary"
              size="large"
              style={{ width: "100%", backgroundColor: "#128c7e" }}
            >
              <CenterIcon>
                <Mail />
                <Typography> {name}</Typography>
              </CenterIcon>
            </Button>
          );
        case 18:
          //buy btn
          return (
            <Button
              href={destination}
              variant="contained"
              color="primary"
              size="large"
              style={{ width: "100%", backgroundColor: "#128c7e" }}
            >
              <CenterIcon>
                <Mail />
                <Typography> buy btn</Typography>
              </CenterIcon>
            </Button>
          );
        case 19:
          //line
          return (
            <Button
              href={destination}
              variant="contained"
              color="primary"
              size="large"
              style={{ width: "100%", backgroundColor: "#00c300" }}
            >
              <CenterIcon>
                <Mail />
                <Typography> {name}</Typography>
              </CenterIcon>
            </Button>
          );
        case 20:
          //youtube
          return (
            <Button
              href={destination}
              variant="contained"
              color="primary"
              size="large"
              style={{ width: "100%", backgroundColor: "#ff0000" }}
            >
              <CenterIcon>
                <YouTubeIcon /> <Typography> {name}</Typography>
              </CenterIcon>
            </Button>
          );
        case 21:
          //CustomButton
          return <CustomButton data={props} />;
        case 24:
          //ok
          return (
            <Button
              href={destination}
              variant="contained"
              color="primary"
              size="large"
              style={{ width: "100%", backgroundColor: "#ed812b" }}
            >
              <CenterIcon>
                <Typography>
                  <i className="fab fa-odnoklassniki-square"></i> {name}
                </Typography>
              </CenterIcon>
            </Button>
          );
        case 26:
          //text
          //console.log(destination)

          return (
            <Typography
              style={{
                whiteSpace: "pre-line",
                width: "100%",
                paddingLeft: 15,
                paddingRight: 15,
                textAlign: "center",
              }}
            >
              {name}
            </Typography>
          );
        case 27:
          // const ava = JSON.parse(name)
          // console.log(value.data.fileName)
          //avatar
          console.log(type, fileName);
          // return (
          //     <Avatar style={{width:200,height:200}} alt="Remy Sharp" src={'https://apie.sendme.cc/upload/'+ava.fileName} />
          // )
          return <Ava fileName={fileName} type={type} />;

        case 29:
          //product
          return <Products content_id={id} />;

        case 34:
          //yandex map
          return (
            <Suspense fallback={<LinearProgress />}>
              <YandexMap data={data} />
            </Suspense>
          );

        case 35:
          //question-ask map
          return <QuestionAsk data={data} />;

        case 36:
          //youtube
          console.log(JSON.parse(data).youtube);
          return (
            <div style={{ width: "100%" }}>
              <YouTube videoId={JSON.parse(data).youtube} opts={opts} />
            </div>
          );

        case 37:
          //CustomButtonV2
          return <CustomButtonV2 data={props} />;

        case 38:
          //tiktok
          return (
            <Button
              href={destination}
              variant="contained"
              color="primary"
              size="large"
              style={{ width: "100%", backgroundColor: "#ff0000" }}
            >
              <CenterIcon>
                <FontAwesomeIcon icon={faTiktok} />{" "}
                <Typography> {name}</Typography>
              </CenterIcon>
            </Button>
          );

        case 39:
          //textfield draftjs
          console.log(data);
          return <TextAreaDraftjs raw={data} id={id} />
          // return <Editor editorState={ EditorState.createWithContent(convertFromRaw(JSON.parse(data))) } readOnly />;

        default:
          return (
            <CenterIcon>
              <Typography>Uncategorized: {destination}</Typography>
            </CenterIcon>
          );
      }
    };

    // const Item= useCallback(
    //     () =>{

    //     if(props.value.type_id===27){
    //         return <Ava name={name} />
    //     } else {
    //         return (<h1>asdasd</h1>)
    //     }
    // },[])

    return (
      <Box justifyContent="center" display="flex" mb={1} mx={0}>
        <Item />
      </Box>
    );
  } catch (error) {
    console.warn(error);
  }
};

export default Blocks;
