import createLinkPlugin from '@draft-js-plugins/anchor';
import { convertFromRaw, convertToRaw, EditorState } from "draft-js";
import Editor from "draft-js-plugins-editor";
import React from "react";
import { TextContext } from './../../../../context/textContext';

type IProps = {
    raw: any;
    id: number;

}

interface IDraftjsTextContextType {
    editorState: EditorState;
    setEditorState: (value: EditorState) => void;
    contentState?: any;
    setContentState: (contentId?: number) => void;
}

const TextAreaDraftjs = ({ raw, id }: IProps) => {
    const linkPlugin = createLinkPlugin();
    console.log(raw, id)
    //const { editorState, setEditorState,contentState } = React.useContext(TextContext) as DraftjsTextContextType;
    const {   editorState, setEditorState, contentState } = React.useContext(TextContext) as IDraftjsTextContextType;
// const [editorState, setEditorState] = React.useState(
//     () => EditorState.createEmpty(),
//   );
    console.log(editorState)
    // console.log(contentState,editorState.getCurrentContent().hasText())
    setEditorState(editorState)

    /**
     * editorState.getCurrentContent().hasText() - trigger, understand preview or prod mode
     */

    // return JSON.stringify(editorState.getCurrentContent(),null,2)
    if (id == contentState) {
        //edit
        if (editorState.getCurrentContent().hasText()) {
            return <Editor
                editorState={
                    EditorState.createWithContent(convertFromRaw(convertToRaw(editorState.getCurrentContent())))
                }
                plugins={[linkPlugin]}
                onChange={() => { }}
                readOnly
            />;
        } else {
            return <Editor
                editorState={
                    EditorState.createWithContent(convertFromRaw(JSON.parse(raw)))
                }
                plugins={[linkPlugin]}
                onChange={() => { }}
                readOnly
            />;
        }
    } else {
        return <Editor
            editorState={
                EditorState.createWithContent(convertFromRaw(JSON.parse(raw)))
            }
            plugins={[linkPlugin]}
            onChange={() => { }}
            readOnly
        />;
    }


}

export default TextAreaDraftjs