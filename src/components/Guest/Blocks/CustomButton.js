import React from 'react';
import { Button, FormHelperText,FormControl } from '@material-ui/core';

const CustomButton = (props) => {
    console.log(props)
    console.log(JSON.parse(props.data.value.data))
    const {name,type,design,description,link} = JSON.parse(props.data.value.data)

    console.log(design)

    const btnStyle = () => {
        console.log(design)
        if(design.type==='contained') return {borderRadius:design.radius+'px',backgroundColor:design.bkgColor, color:design.fontColor}
        else if(design.type==='outlined') return {borderRadius:design.radius+'px',border:'1px solid '+design.brdColor, color:design.fontColor}
        else if(design.type==='text') return {color:design.fontColor}
    }

    return(
    <FormControl fullWidth={true}>
        <Button
        variant={design.type}
        style={btnStyle()}
        fullWidth={true}
        size='large'
        href={link}
        >
            {name}
        </Button>
        {description && <FormHelperText style={{textAlign:'center'}}>{description}</FormHelperText>}
    </FormControl>
    )
}

export default CustomButton