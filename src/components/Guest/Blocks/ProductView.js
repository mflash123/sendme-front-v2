import React, { useContext, useState, useEffect } from "react";
import { makeStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardActionArea from "@material-ui/core/CardActionArea";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import CardMedia from "@material-ui/core/CardMedia";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";
import Avatar from "@material-ui/core/Avatar";
import IconButton from "@material-ui/core/IconButton";
import ShareIcon from "@material-ui/icons/Share";

//fa-ruble-sign
import { faRubleSign } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

import { ApiContext } from "../../../context/ApiContext";
import { useParams } from "react-router-dom";
import { LinearProgress, CircularProgress } from "@material-ui/core";
import NavigateBeforeIcon from "@material-ui/icons/NavigateBefore";
import ChevronLeftIcon from "@material-ui/icons/ChevronLeft";
import CloseIcon from '@material-ui/icons/Close';
import AwesomeSlider from "react-awesome-slider";
import "react-awesome-slider/dist/styles.css";
import "react-awesome-slider/dist/custom-animations/scale-out-animation.css";
import styled from "styled-components";
import styles from "./../Blocks/ProductViewSlider.css";
import { Link } from "react-router-dom";
import Block from "./Blocks";

const useStyles = makeStyles({
  content: {
    padding: 20,
    textAlign: "start",
  },
  rounded: {
    color: "#fff",
    height: 28,
    backgroundColor: "#3f51b5",
    padding: "0px 8px",
    margin: "auto 12px",
    verticalAlign: "middle",
    fontWeight: 300,
    fontSize: 14,
  },
  productName: {
    padding: "0 0 10px",
    //borderBottom: "1px solid #dadada"
  },
  productInfo: {
    padding: "10px 0 10px",
    borderBottom: "1px solid #dadada",
  },
  productOrder: {
    padding: "0px 20px 20px 20px",
    display: "flex",
    justifyContent: "space-between",
    marginBottom: 12,
  },
  font: {
    fontFamily: '"Poppins", sans-serif;',
  },
  svg: {
    fontSize: "1.1rem",
    height: "0.9em",
    marginTop: "5px",
  },
  boldFont: {
    fontFamily: '"Poppins", sans-serif;',
    fontWeight: "bold",
  },
  shareButton: {
    position: "absolute",
    right: 15,
    top: 15,
    background: "white",
    borderRadius: "50%",
    padding: 4,
  },
  backButton: {
    position: "absolute",
    left: 15,
    top: 15,
    background: "white",
    borderRadius: "50%",
    padding: 4,
  },
  price: {
    display: "flex",
    alignItems: "flex-start",
  },
  heading: {
    display: "flex",
  },
  cardAction: {
    position: "relative",
    zIndex:9
  },
});

const SliderStyled = styled(AwesomeSlider)`
  --slider-height-percentage: 100%;
  --slider-transition-duration: 575ms;
  --organic-arrow-thickness: 4px;
  --organic-arrow-border-radius: 0px;
  --organic-arrow-height: 15px;
  --organic-arrow-color: #538bd5;
  --control-button-width: 10%;
  --control-button-height: 25%;
  --control-button-background: transparent;
  --control-bullet-color: #62a4fa;
  --control-bullet-active-color: #538bd5;
  --loader-bar-color: #851515;
  --loader-bar-height: 6px;
`;
export default function ProductView() {
  const { getProduct, getAction } = useContext(ApiContext);
  const { productId, username } = useParams();
  const [items, setItems] = useState(false);
  const [loading, setLoading] = useState(true);
  const classes = useStyles();

  const [kostilblock, setKostilblock] = useState(false)
  // const kostilblock={
  //   id: items && items.data.action.button,
  //   data:items && JSON.stringify(items.data),
  //   type_id:items && items.data.action.type,
  // }



  useEffect(() => {
    getProduct(productId)
      .then((value) => {
        console.log(value);
        if (typeof value != "undefined") {
          if (!items) {
            setItems(value.data);
          }
        }
        getAction(value.data.data.action.button)
        .then(({data})=>{
          console.log(data)
          setKostilblock({
                id: data && data.id,
                data: data && JSON.stringify(data.data),
                type_id: data && data.type_id
              })
        })
        .catch((error) => {});
      })
      .catch((error) => {});

    setLoading(false);
  }, []);

  console.log(kostilblock)

  if (loading || !items) {
    return <LinearProgress />;
  }

  console.log(items)
 
  return (
    <Card className={classes.root} square>
      <CardActionArea className={classes.cardAction}>
        <div className={classes.backButton}>
<Link to={'/'+username}>
          <IconButton aria-label="share" size="small">
            <CloseIcon color="primary" />
          </IconButton>
          </Link>
        </div>

        {/* <div className={classes.shareButton}>
          <IconButton aria-label="share" size="small">
            <ShareIcon color="primary" />
          </IconButton>
        </div> */}
      </CardActionArea>

      <SliderStyled
        animation="scaleOutAnimation"
        cssModule={styles}
        play={true}
      >
        {items.data.attach.images.map((val) => (
          <div data-src={items.root_image + val} />
        ))}
      </SliderStyled>

      <CardContent className={classes.content}>
        <div className={classes.productName}>
          <div className={classes.heading}>
            <Typography
              variant="h5"
              component="h2"
              className={classes.boldFont}
            >
              {items.data.name}
            </Typography>
            {/* <Avatar variant="rounded" className={classes.rounded}>
               Категория
            </Avatar> */}
          </div>

          <Typography component="h5" className={classes.font}>
            {items.data.description}
          </Typography>
        </div>
      </CardContent>
      {kostilblock && <Block style={{width:'100%',marginLeft: '20px',marginRight: 20}} value={kostilblock} />}
      
      <CardActions className={classes.productOrder} style={{width:'100%'}}>
        {/* {typeof items.data.action !== "undefined" &&
          items.data.action.type === 1 && (
            <Button
              variant="contained"
              color="primary"
              href={items.data.action.url}
            >
              Подробнее
            </Button>
          )} */}
          
          

        <div className={classes.price}>
          <Typography variant="h5" component="h2" className={classes.boldFont}>
            {items.data.currency} {items.data.price}
          </Typography>
        </div>
      </CardActions>
    </Card>
  );
}
