import React from 'react'
import { YMaps, Map, Placemark } from 'react-yandex-maps';


const YandexMap = (props) => {
    const {data} = props

    let cords =  JSON.parse(data) //for ava
console.log(cords)
    return (
        <YMaps>
            <Map defaultState={{ center: [cords.lat, cords.lng], zoom: 15 }} width="100%">
                <Placemark geometry={[cords.lat,cords.lng]} />
            </Map>
        </YMaps>
    )
}

export default YandexMap