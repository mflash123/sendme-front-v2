import { Button, FormControl, FormHelperText } from '@material-ui/core';
import React from 'react';
import PreviewBrand from './../../Types/CustomButton/PreviewBrand';


const CustomButtonV2 = ({data}:any) => {
    const res = JSON.parse(data.value.data)
    console.log(res)
    return(
        <FormControl fullWidth={true}>
        <Button variant="contained"
        startIcon={res.design.brand && <PreviewBrand iconname={res.design.brand} />}
            style={{
                color: res.design.fontColor,
                backgroundColor: res.design.bkgColor,
                borderRadius: res.design.radius,
                // borderColor: color.brdColor,
            }}
            size='large'
            href={'//'+res.action.action}
            >
            {res.name}
            
        </Button>
        {res.isdescription && <FormHelperText style={{textAlign:'center'}}>{res.description}</FormHelperText>}
        </FormControl>
    
    )
}

export default CustomButtonV2