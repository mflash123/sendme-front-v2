import React from "react";
import { Draggable } from "react-beautiful-dnd";
import { Grid, Avatar, Typography, Box, Button } from "@material-ui/core";

import { useStyles } from "../../style";
import { Blocks, Products, BlocksAdmin } from "../Blocks";

function DraggableItems(props) {
  const classes = useStyles();

  console.log(props);
  const { items } = props;

  return (
    <>
      {Object.values(items).map((value, index) => (
        <Draggable
          draggableId={"item-" + value.id}
          index={index}
          key={value.id}
        >
          {(provided) => (
            <div ref={provided.innerRef} {...provided.draggableProps}>
              <BlocksAdmin provided={provided} value={value} />
            </div>
          )}
        </Draggable>
      ))}
    </>
  );
}

export default DraggableItems;
