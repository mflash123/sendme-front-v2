import React, { useContext, useEffect, useState } from "react";

import { DragDropContext, Droppable } from "react-beautiful-dnd";
import DraggableItems from "./DraggableItems";
import { AddElement } from "../Dashboard/";

import {
  Grid,
  Paper,
  Avatar,
  ThemeProvider,
  Card,
  Fab,
  Box,
  Button,
  CardContent,
} from "@material-ui/core";
import { Link } from "react-router-dom";
import AddIcon from "@material-ui/icons/Add";
import { Alert } from "@material-ui/lab";
import { AdverbBlockAdmin } from "./../Blocks/AdverbBlockAdmin";

function DragWorkSpace(props) {
  const { handleDrag, items, tarif } = props;
  console.log(handleDrag);

  const [openAddElement, setOpenAddElement] = useState(false);

  const handleClose = () => {
    setOpenAddElement(false);
  };

  return (
    <>
      {/* {tarif.id===1 && <Alert severity="info">У вас Бесплатный тариф. Доступны вс хватает возможностей? <Button variant="contained" component="a" href="/payment/2" size="small" color="primary">Пробная версия на 10 дней!</Button></Alert>
} */}
      <DragDropContext
        onDragEnd={(result) => {
          handleDrag(result);
        }}
      >
        <Droppable droppableId="droppable-1">
          {(provided, snapshot) => (
            <div
              ref={provided.innerRef}
              style={{
                transition: "background-color .3s linear",
                backgroundColor: snapshot.isDraggingOver
                  ? "#dce8ff"
                  : "transparent",
                position: "relative",
              }}
              {...provided.droppableProps}
            >
              <DraggableItems items={items} />
              {provided.placeholder}
            </div>
          )}
        </Droppable>
      </DragDropContext>

      {items.length > 0 && tarif.id === 1 && <AdverbBlockAdmin />}

      <AddElement handleClose={handleClose} open={openAddElement} />

      <Box justifyContent="center" display="flex">
        <Link to="/dashboard/add">
          <Fab color="primary" aria-label="add">
            <AddIcon />
          </Fab>
        </Link>
      </Box>
    </>
  );
}

export default DragWorkSpace;
