import React, { useState } from "react";
import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/core/styles";
import {
  Container,
  Grid,
  Paper,
  Button,
  TextField,
  FormGroup,
  InputAdornment,
  IconButton,
} from "@material-ui/core";
import { Visibility, VisibilityOff } from "@material-ui/icons";
import { useForm, Controller } from "react-hook-form";
import Axios from "axios";
import Alert from "@material-ui/lab/Alert";
import { useStyles } from "./../../style";

const Auth = () => {
  const {
    register,
    handleSubmit,
    watch,
    control,
    setValue,
    formState: { errors },
  } = useForm();
  const [error, setError] = useState(false);
  const [hidePassword, setHidePassword] = useState(true);
  const query = window.location.search.substring(1);
  // const verify = query.get('verify')  signin?code=true
  console.log(query); //123

  const onSubmit = (data, e) => {
    //data.email data.password
    console.log(data.email);
    //мне не нужно доставать значение во внешку функции, потому что я здесь прост ставлю local storage+redirect
    Axios.post(process.env.REACT_APP_API_URL + "/api/v2/auth", {
      data,
    })
      .then((res) => {
        localStorage.setItem("username", res.data.username);
        localStorage.setItem("token", res.data.token);
        window.location.replace("/dashboard");
        console.log(res);
      })
      .catch((error) => {
        console.log(error);
        setError("Login or Password error");
      });
  };

  const onError = (errors, e) => console.log(errors, e);

  const classes = useStyles();

  console.log(Boolean(errors?.email));
  return (
    <Container component="main" maxWidth="xs" className={classes.root}>
      <Grid
        container
        direction="row"
        justify="center"
        alignItems="center"
        style={{ paddingTop: 50 }}
      >
        <Paper
          elevation={0}
          style={{ backgroundColor: "#fafafa", width: "100%" }}
        >
          <FormGroup className={classes.formCenter}>
            <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGQAAABkCAYAAABw4pVUAAAAAXNSR0IArs4c6QAAALBlWElmTU0AKgAAAAgABQEaAAUAAAABAAAASgEbAAUAAAABAAAAUgExAAIAAAAYAAAAWgEyAAIAAAAUAAAAcodpAAQAAAABAAAAhgAAAAAAAABIAAAAAQAAAEgAAAABRmx5aW5nIE1lYXQgQWNvcm4gNi4zLjMAMjAyMTowMToxMCAxNzo0MToyMgAAA6ABAAMAAAABAAEAAKACAAQAAAABAAAAZKADAAQAAAABAAAAZAAAAADlA8wEAAAACXBIWXMAAAsTAAALEwEAmpwYAAACcmlUWHRYTUw6Y29tLmFkb2JlLnhtcAAAAAAAPHg6eG1wbWV0YSB4bWxuczp4PSJhZG9iZTpuczptZXRhLyIgeDp4bXB0az0iWE1QIENvcmUgNS40LjAiPgogICA8cmRmOlJERiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiPgogICAgICA8cmRmOkRlc2NyaXB0aW9uIHJkZjphYm91dD0iIgogICAgICAgICAgICB4bWxuczp0aWZmPSJodHRwOi8vbnMuYWRvYmUuY29tL3RpZmYvMS4wLyIKICAgICAgICAgICAgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIj4KICAgICAgICAgPHRpZmY6Q29tcHJlc3Npb24+NTwvdGlmZjpDb21wcmVzc2lvbj4KICAgICAgICAgPHRpZmY6WFJlc29sdXRpb24+NzI8L3RpZmY6WFJlc29sdXRpb24+CiAgICAgICAgIDx0aWZmOllSZXNvbHV0aW9uPjcyPC90aWZmOllSZXNvbHV0aW9uPgogICAgICAgICA8eG1wOk1vZGlmeURhdGU+MjAyMS0wMS0xMFQxNzo0MToyMjwveG1wOk1vZGlmeURhdGU+CiAgICAgICAgIDx4bXA6Q3JlYXRvclRvb2w+Rmx5aW5nIE1lYXQgQWNvcm4gNi4zLjM8L3htcDpDcmVhdG9yVG9vbD4KICAgICAgPC9yZGY6RGVzY3JpcHRpb24+CiAgIDwvcmRmOlJERj4KPC94OnhtcG1ldGE+Cq9m/mQAABp7SURBVHgB7V0LcFzVeT7n3ru7sh6WDdiAAVs2htYmTQFZNoSH5IRHQgIhBQnZNKSZzOBpmmkmSZnS6TSW05lkMiXJtJNkSsoMgQQkS8DwpqRhvCYh2MgLFIqaAmNLNsaAYhvZeqx2772n33f23tXqsfbq3rtCdnTs1b17H+f85//P/zj//5+zQsyVOQzMYWAOA3MYmMPAHAZOCgzIE7MXajzcbVvwffPkrmwWSox/Es/oC2ryw7PjyiRwZwdYHhRKyeYuYfT3JKVobNIXk9uFC9yPIXQLiNHW5haFu00Z458XsqlRGLnnk2JRfxPq6hJdLS1O0Tpm8MasI0hzZ6e5+3CzIeqFSK0RNkb0GPKLIObyB/oWZoV9moqZtspK3SfXxHHgg72pTWuyRV4bd7lpm7KOvpnS79YcOKqSbevR9syX2UEQcEJTUphCJEVy/XhEXND5RrxaVK90bLXKMOQoqPMpJVQdAE8DXXhHZHFtLb6fDWmUVRBSIAUeETHcexmfPbiWwEVwgOK194V0n1GuSLhKvHnkD87/1SYG3EmEa2szmho3G4v6hepqAVeWMDBQd+jy0RHEE0cTxUV956Fayx1Y5Up5GZB6KZD4MSnkYllRuVA4jpDxBDAOiQMs59SBFG4mLZQNRsgxRx4pRrxCSNMa9yzf4/O87qQHj+Kl/XjviHDV80qq/ag6KZz4we4NZ+3LV8QTiL7m1UJ29QCqNllcRI57afpfZp4gIET9z4SV2iTzomTdL9+a78Ril0M4fQFduAoIWmZWVAE2DHXXAbIzQjkOkKCVOUXJRDFmAM8GuGDCdfDBpGdZhwSVUJlpGdKKsxlNIGXbwh0dQYPuQcDwmpLyt1K6j1UtEr9Prl9OjtSlads2iLcamdpUX5JI9d8r5TijBKm/e1csdTs64SGuYWvfVUBtM3BzDUZzHZGjsmnhcrQrQSULBGscE06wBREOJR26cIRrwvCIOkkRtoa6JQSjYQoDnCgMQzhDRygS+3D7N65wO1KtK37lN9/cqcz+RUIm18vI9A3BKH/x2b2FchwCv7P3k64rvmWY1nXmvBqMymHhZkc5mvkhwgnXzMCGhvIlp3vwVROehKLZbBmxhDAS80gccuzvcP1ZpdQTuzbUvcJ3SZioRFnZO625wrN01m3tXQ9FegdG4GeMeVW6g+hPBh8qZ35mYwFh9EDhYIqb86p9zsmAWh2ma/xg58ZzXtOA08QOqV/KShCfGPV3v1tp1I5+V5qxrxtxjLQR6FKlshBdJEIEIkijYyb+uIAblgWKYVhGRbVwho/YkKodRsz8zs6bz3mLyj8MUcqDDCruXSpGU/LSzn2NRm0mZdWc8nUF2xXEoCJ00SmaoOVpv3ykgY7RcJvKdV1nmH0RVmzBor9Utnshm60/MxWK06PnEBBDeLPntVv3XquU+wz0hHTSR0cxkqApT6riwBgx3czIm+7Kuo+l1tBy1MYCxVygEu0I1cSAGoQrY93Wvd/B7OwZmJYeMeTJRgwi3JVWjKq/ncSgcs8ZBIFooV+KlCD1P0tZlJ8NnXv/2lqw6J8gbyXEFOx6EiPwoAneu7K+KR0YJzGIraxrqvZcU12hW4xMZPkKfO3WPRBT8nHIWkwqoABnr/UUDnkwSqzqBbHs0MCju1rrOKGloQKnAk3m4CUSDskTo2P3nwOmp2C3n9zEoBlsGDF7aAASSt5F9BMHYYnBeuhCCFdAgZSU2aa2bdawNH8MO920h4+OgvVORp3h40rRfHczw68ul0t37MLVFQt3uyn/bohjaIJQbwCQ7OCfLP9SrHrB5faRQ3CAn5QKvADNGG50Kbvinq4N0qGJ36UtrIJHAp6GIwgmQXQS1ne+u1Q6mX92RgYBKBxAJ50CH4ddOCVN00kPDSrTeYJ3ap5MhtIbhbWH0iH+JMhws9+05p9yJryy9OCGmhgVAjdLzx0jUUlH2/2plnP30tSNMpgVnEPgGaXuoFtEicw1bnqIcyJT++VmKSbDg6U9DBbcJQ5G3Y9z9YU3dQvhCswhF3T1aGKatdmbrYqqVdpbC6OvsPLynWvE0G1BN4z3KTzX7vAJ1+nOh7eZHt3ApqnrIE6DasT2Ha11/8v+RR2LD8YhsKzegCLXCJfiesYNUOg6D1afrqiUP9KB21saMQtGZ4Kxi1JewjMQ8fD3w42jo4U4AVJBNNSFU9ggeiAdbzDhJU8/Ou7dbJiBqokh5xIBKvpYIAQyE6QLsY219/cuRycb3cwoGzheh4oCUeINW1qWZSCIBfH4nmNnfw9k/hfck+8BqwkEDDkgxhUEd0mxNPB9Mdw4awAiw4MXMlJoVFQihuvkwr/wE2qKSQP8o4rgRDpGLG7BcNkbq6hMsqFcxgrPoitFGj92A4yS8QmVkOcaVmIRxBW/lpMgDhGoRkcOqWzmH12VeCi1cckf2GiJ5V4+V3+3isn5fR+Tyl5iDx35JLBPIl2CuVMcgx9OQsTmsxnEZ3TQfgJuQHLE6PFM++/+4owPmKXSFWGk0O/HhEb9y8c+MhODT8BRtUowiSA7ShdJqfLj2JVPvkurxkSsO2VY5o07b1r2jn5EZ4U0GoxtT35l/BVO2rpampFZouP4jPLx8xSfqu/ovViODF6M0yvh+LjOqll4KgeYmx5miIByLmc1SsPCzBwyTj7G9/yUIZ5HWQIRpKtniyYIxswnzBhFyCAVZhnMXTjwYPMjxDskYrJl501nv7Py6bcSFw2utCkykzm9dVx85GfQ0BnaAeq9wXhNqrWOqUL83HNJ+54658ihW0CYm4xEVQOmVAbDy4h9pMEdFTjf+tLGuhcZhEptohERfQlEkFzaZhvYRCEdhLQ57iANCLlytCvm6IePdd+0bLcOeq2Ro28HrI3WFb0K+deBWH8uxeSLHVL24t73kQv2o6qMWA+H1QZ8v9WsrEE+EfqYVffx3fozhZkSY1kzvBZVCUiQXPMMn0UFyBT1kNKWm81Aokud6bFit4jEX5RvC6ECEChnDNwOvgCBmGaKLBLG+Z/lp75z3w/E8FAbHjvdrUo8z3dTt4M7NvEs+hKQIFuih2RyjSR2FpZN3M6MUMbft3sF9BRt18DziMmN5K/k6lTJHIEkvLfWioX10D3ydTxzEyfAqdYlw5z9lqV9D5ByKeJ8P0OemM4IPQDqry59aP9FjMphQhqjqzsXnQtZe/HXFfUL9RTFJNpHUhyIgSNNmeKvhb8TkEPCN1xaDdJUTpZx6/lONv3rdZ191+xsWaZ1NP/oBOkaIVfs7sJILk/2uo6TgxBMAu/S+cGlQR70qYAE2Yz22iB8fZswaPPHe4+DUXJ+YIMop7jZ9La1HX3/7ir36eq02AFZr9M7NYVYFfxrsKLM1IF6WH0Qq21trCD8iIY4g8eKpn3ZS0CC5HQIhClmviwacbnTyP/qui0QhSZwDTJY7lCDA3cMzVM9azt6n4LGfy7ritdf2bD0Xcj2MSWtwYKI2SJ0knT/oqTkzDqqDMPIu+lVSMU57dKGkdiGzje0933XrKr+Bwb6UQnzrMpdOEp1ThdnzczBZdY7XDcHcJ1Z7K9i0vACUqd+lzVGDr/Scl7/VADpcCsWoNScL1RyvXY6hueiqRoKcC0QQSi7mWDc0Lnv04YVewajlx3iZ6aMBJqqPhKRwR6TTNQmgXgZAwQKWAxgerQdHUSytHpRKrPbOWL1aOWMp/IF+qEpiRjOdqxNaWviBNevN//ITJ4EE1kAngU+uQ9ExhkFIhLM5BvDkb5dzj/jCA8ucfHxBgUEqWlVAqZKcFEruAZLGewvIsJny9rMW2s6et/Dte3QNy8hmfVVxHQOJLUb3wOXcxGRNJKiyQ2TEhq084E4pDB/taFjz3azovpKdhhABCNwUOiLvAfnAdb7aA7yFbEJN4gpYxRzoCU+LsLNyGSHSDNexsnLeOGhmhHVk/xywToQJG4cPRPrQAqWUBRpMrLLwQiC5uk5pbMOBLnTrKr9HlL12fky+LMi6SvCKJjRwR/v1cZ+m8w6pDsfHCXgOLTBYr1wHr4gDfdR3Hz+xZZzDunnPRfLpGVvkYA2vpLABPGDMw0P9K0QpkrBGboAI45ia5w4Gd/crPrmiTjNSQxT5daBYC0IFwzBkbgXxLkXVHyqu3VpNyHXc5Ge5tm7pI3WCkdNQ0fvXchu/5Z99CB8QAZMYfb1hCsEmgMK0ytpGokKQ4I4ztAArsmHlWHctavlnJfYK23UNME6K4MLJ9Rozk3AoBGN7PezRw72IW7BeQlj2YT7RCtajAF2ZCBivQQmoyAGlt9B+VRWN0vl7sSk9D4srzjFW8LGpPJQ+JsKQeEqhLeUXJKz982/5QJNT2T5snqqNk+EaySOBcKYMCVz60CghMyq+bfZrvPa2vbeL4A7sIHBZhW1Ty0cQQA1RRaJsmvD0sdVJvPtWO0i1KlocZ3oRKHkBa8I4ojWo4Liz0KMnWVWz3+koaPvexRZdEBGSZRoZAusENrszAi5tGvv47Jq4efso4dBFK3gQxMdyJglhejCYAPnwNSXSHj46RlV8TueuH7JMIlC4oQFNBpkgRgECPk06tM9Sz/vDh56FHIXLA9DkxE6fQgL6mx4XxsrFngfS/MGszBkvvrBcHZH04PvnhYVp0TDIR6uCkdJQ8ferxqJxE94C0u+TsblbBRp6djCRRX2QH/7S7fUbdRogJSgKPNQMu1DNBziNZsbJZ16cgjb/aciM3Kzmx45YFUtSAB4mo+0wE583eKjWYqK7If9tkxUbsAmCN/Vl7dAoIUooV4u2q6e2SIRADP5dQ/uPh02/L9gr5IvUkPCxcLX4B2mHCuWlFa05ll4AwMMip+bCyD+39h9y9LnvWSMsWSKaUAdKYfk24VO0csUYH3t3Lji/Zda625zRtKfx2Yvv6FXFuZjDG4LWC46B5eAn8BcA8NFqQzdL9Asf0ccpOoxFws4RykPh+QpM9ndsKaz72rYYzfikVuR4lOLiZfeWkMvDoXsVa4ywDv8RxlddvgKQA1zSnGMPVJImyy4ZMXzfohiupXOWIe579UbLRcwvUaXhvb95wiZuQra7wYYY01momoBvbA64ITUH7rzQRi9awJ6SU4mrDMGbw7Kaf3NgPPjztDRJ7tbl12fe3P6SREz20FftxwAoiHW/O7CHXGW4zqfwTzmajjz/gxAnY+0f2zJAwKBOEztzLnUOeHERU0YL8XTr+SjPyLEbCEpwz5iGeZqeIr3a7F1rO0Hp4CZM9CZK9Qt1Bc0DTcro2kLA0HbXQ08UjkByD0ff/a9qsTAyHnIcP8kPOYYafI86J2zDCS+c3UvcnwZcAIn2VSmnIiBajjLzahnri+TWzK4pxdgnW87WeaR7W8Sjejf9PTjzBLE70TOTveT0gQDXkzp1Ilp10qaYa96nx9e1L53iZXNrhJ2Zrk9OtIIYq7FvbPgja3iGhFNHBAol4HvLcjRuicv5vxWy3yEgFWubVZUGtnBD9ehsSf8jTun0/DMiqwSIMtvgilS2k828ZUL792zQMZVrWmYSIZW7Di3AlwMcXEuTU9KNK5XYVgXgxM66FhrPibWHvq7l6k/tKO7dfmlXm3EcckTxVlHkHEoAedwn0OuR9GbURbZ75BEilWqjwtHXga+wIaY8hKEAs5AAoZwKOKyo3TfUBqUu79c8oYVuoOFBBnXpeN9KR+AtIy2oPkC5X08YEq6T/2zRei9d3M7hk526F3U/vslppj3KUjG60CIq62q2lP1rgvIsCuzrqEOMbAaee9oxm547baVHxTmH5TSv/LoEIxsIEJbUbTHiyGuFAAnPTNR//ABTwfxlEGzVzbId3H6C37WPfzO2fbQ4a8JZfw9JqPQOdlyxv5JDIV2lsakuADtf5BbulC6Yi8DQTCCwRVcmRQ3zPcRXdsPwHRSxIqFgtnkoV3UrG9c8a03XgQHNTUqvSly6sCTDhf54OqdCDO/jgDaLzFQkF0HTqF1FnXhYEH7sLYEtv6Dic6ST3TNfT3O32gJQnECoJiRYoi+dkTXToPTbUvaFr9I3SoPEzQGs7w0/+gJw86i/aSfZ0XiiEarf/Vio7ul7gEElc4351V9G67zkpUsqyy5sP8ojDpglhSI4JESxN/3xKzt+5JRWXM+4tLCTFT+a8XQwDcatvb+xJXOf2D3gwF/zOQibdh3vacHmRxt+YliyQg43oMecThA9KOu6MQWg3ciiS4OTwCJUhYdqhnQRRpegBIdQB53YNY9L+s6KTNesQoEGQVMJjIILToVsQNCL3DwK8wM7/bW9uVB9jcnznEPZW7wmEK+Uu/Ed9s0PNh3AVZPv1w+gmDpdAJb/o2O/Le7clmDv5SBXDsRpmLfI+MQLqR5A1u+Zm33ciMRJzEIBCYGmC5lRm2RSWPBf6IOW4XfLoaOfGXN1t4XDVc+6ZjG06mWc173F+CTe8g5uw/riR22Pcqv/eDgKbljeFYXGhXvvP52TpSYaqNRUROHyCqTYgd4FFdYuqCJQQimQQz9uIY67B9PdrLxNe29XVZl9c06tVQVxDsIJ1gDQoLZ6xa3qMBIRcbgh2BtYwcmcU/DPfK0ffD9t5E4gaVjBcXjPu7JxdTOgjtFT3WK0ma2lxudl3b03eYY8j6mMJZRqWOmXmXZ6aGndt2y7HNoCLCWzh3sTEmdK9pr74afMFff2Xe5ocRvdLUQ0rhdTLFxpHOUYp8MM8bddTjDdgYH0ujCHrzfjYH2Wzh8X3OFuU+v/fDamu6BXmVlZL9hCOMbSE3gDL5M3KEh09v+OYMffgsxoB/6eJkOzOFFFkZvaotGLkw9cbuJEIc9+CHd7EyaK1Y4EHTboJsD3QKuQRgEuRyYXa+SsfgqeHlvg/8KIt/dD5P1LdCuQirjWTDZXgz6KbfSYGOoBRMgN4ORcBmovgrByXNjVbWL7UFsx4dtg/GIDjHz2YgLmpMx9B0YkTtZd5Bd5sJzCCeBmAd84pH3Fmcz6VegJ5YglAkbvEBcldZzPyGaHcOH3IVgFYwBrMTFKS7p9R+lVYYX8J+kgV/LgffRq6/Ut6f/nMSyuwRXer2fcOSq39667DDnJL7ILLW+0BySW0sBZ2s2vQnbUizhFn8AIshqKprv3gAhPVDACpz5OvjkLhDLxcYQH/Hv+ed6XkCxGbqfXvvHOAD7GDwqk35AE4MD1fNWHOOlSbeKyfhJD055AXFj7qZW/4veM/GDKF/jEmZo0ehEAkeYHtlazLBeIJacN9Wn8J5/rt/zqTRlFyK6SEMlxk3cTGF1sc6x37maXguhCHLB6mY98kxTXYPw5WIoTBujPFSd0wN/1jytNzaDa+bXO1vPhsUohP7xsgDgBWdljN43pN6CApLe+Ar9Nxi5MzEaA3SzrK9QPlq6/676AVvKTURzuJluy4FHMyeCbAw7Wd8IZXYF9rAlYIHrmy7gs+N5Pf5g6tZiU9bh+7s3Lv9Pep7faFkdyG3CPgVDILhjtVhNlqAhcQOVGZQHv/8RcQi7qrihQRwm9X7HsHVOVvPqLtyY3mSQePRLIAT6OUf1D+35U8PGxtZY9QpTqJw2vg/vbDpSkUvMzJkFf1N3a90j/rrLMEAG4hBfYRlZ+U1sSE9icCIYnXUVpkcz8a7OUxauJsbw0a+TGByk3o51oSCYPkFoX2MieFHnW9x5ET4r7Xb64yEGXT7wy8UWLsYekEOPdW9Y/m+kgD9IQ1EDL0+bIJwIslHLjjXDxbFQ72ad+y2psLDM7vdhz6Nk4XvDium46Rzu//nief16CYKO62CQRtGBaeoQmrU5hdXQvufN+GlLzsscPKDtXQAT3ISOoiflrYMWZAZuIfx+HnORR/4GYuqnuklPYkTV/DQ5ZMx6cA25KXPw3ScQ0DexJyFnz4TJZsonjvpLVEB+hPWwH/SDyVjtaQkk4/Xa2fQVmhgkBDNrIuIMv4/T5BD/tbFjw4N7GrGY7Q5Dmp818Bt/ehdP7eJmIoHeevpE1C8gBPKI+TsIVTUGNrMhYX4+T9mbn9+wcp+2Msu0Tj0wQRgsSm5uYna65gZs1fQpSLNbAfh1cKOcTu+sO4qNiZ0sRxg9ufxB7Wly5BjhZ+KMIANGwmthqYROU4X3dhcIc2f3hmXPEYYwi3FK6UNggviVU6HpfXy9JAXufetK43r07bNwNF5mVc2v1sTR6Z302jINSOsitI0jIWAc8aMpHEyEiWIWcUwLEZkqb2uNIWzpJH/0UuuynLOQ+7xvbyr7DkGRIYJsTJwiD4sjTJeLO/etNBznWnT1BuyEcCGsssX8qaB8nIIbDTCySDe15jPihnTyOUn7xiKAkQjXG+97G9CwSj0oYDFZ2AxkHsBAwnZ6+H1c3wZ2fnjXhuUPAxgNlT8R9rpV1kMEnR0Pn589MnHnHM5bYipxMTqLBGl1Bbjm41Axp+JHfzVSmMWuiQIiQXlqROQQojchGN8I3fJ6n6Wxy6gXP4dKwuokuMIbDBJhsGC8Y6dqDAq8CrUG9QbdgOcUiCC2YQw8ht32n3ul5cx+/2U9856wlsW/V65j5ATJA4pYSfPq1bK/Z5Gc6hdo9AoqM3Mq0N0ED8RZiL1eCWTWAqAKGa9YRjFHE1NntPuV8ho4iFt4cJ3I+AK5Z2C041eddfGeJZWpyxhPBwfgJbUPdezGMy+AbV6wnHk9OzeeTqLkCqwnzrWSovziyW+y8Fg+ghS2wnOM4KZkzr2S3L4FsriNcjtfmBBQEz/VzNQ68zN2/BL8ggEHfRWI9VkSCV8c8IAJIYKtrtUixNd1uj9lHUhESsXw5xCefQHnuOxSP1m4exiL/57EzlEmjn2Vaed/CjcpIwC5tKOUwdTTiXDlAZyhk5kjyIQOFab01Jxfj80ox3TPhEen/IrfZF/OvSMEBj9IAUbDrv1D9mD3l5e/N+ULEy7qDZJTKVFz4KiaioMnPD5jXz8ygkzsIRfqYKzqy/7vk/BLkvZ+YfF++Ljw0rhz6pfCguebGjd75nZS/wjLbN4qdjzwhR2ZNecTEEy4IJu4RmRKENv03Qm3xjwME27MfZ3DwBwG5jAwh4E5DMxhYA4D5cHA/wO5P8jgmYGj0wAAAABJRU5ErkJggg==" />
            <Typography variant="h5" gutterBottom>
              Войти
            </Typography>
          </FormGroup>

          <div className={classes.alert}>
            {error && <Alert severity="error">{error}</Alert>}
            {query === "verify=1" && (
              <Alert severity="success">Ваш аккаунт активирован</Alert>
            )}
            {query === "code=true" && (
              <Alert severity="success">Новый пароль Вам выслан на почту</Alert>
            )}
          </div>

          <form onSubmit={handleSubmit(onSubmit, onError)}>
            <FormGroup>
              <Controller
                control={control}
                name="email"
                label="Email"
                //defaultValue={value.name}
                rules={{ required: true, pattern: "/S+@S+.S+/" }}
                render={({ field }) => (
                  <TextField
                    {...field}
                    label="Email"
                    helperText={
                      Boolean(errors?.email) && "Email field is required"
                    }
                  />
                )}
              />
            </FormGroup>
            <FormGroup>
              <Controller
                control={control}
                name="password"
                label="Пароль"
                //defaultValue={value.name}
                rules={{ required: true, minLength: 4 }}
                render={({ field }) => (
                  <TextField
                    {...field}
                    type={hidePassword ? "password" : "text"}
                    label="Пароль"
                    helperText={
                      Boolean(errors?.password) && "Password field is required"
                    }
                    InputProps={{
                      endAdornment: (
                        <InputAdornment position="end">
                          <IconButton
                            onClick={() => {
                              setHidePassword(!hidePassword);
                            }}
                          >
                            {hidePassword ? <VisibilityOff /> : <Visibility />}
                          </IconButton>
                        </InputAdornment>
                      ),
                    }}
                  />
                )}
              />
            </FormGroup>

            <FormGroup>
              <Button
                color="primary"
                className={classes.buttonMargin}
                variant="contained"
                type="submit"
                disableElevation
              >
                Войти
              </Button>
            </FormGroup>
          </form>
        </Paper>
      </Grid>
      <ul className={classes.ulList}>
        <li>
          <a href="/signup">Создать аккаунт</a>
        </li>
        <li>
          <a href="/recovery">Восстановить пароль</a>
        </li>
      </ul>
    </Container>
  );
};

export default Auth;
