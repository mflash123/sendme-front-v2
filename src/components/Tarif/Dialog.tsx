import { Button, Dialog, DialogContent, DialogTitle, FormControl, FormControlLabel, FormLabel, Grid, IconButton, Radio, RadioGroup } from '@material-ui/core';
import CloseIcon from '@material-ui/icons/Close';
import React from 'react';
import styled from 'styled-components';
import './style.css';









interface IProps {
    open: boolean;
    setDialogTarif: (value: boolean) => void;
}


const CloseButton = styled(IconButton)`
top: 5px;
position: absolute;
right: 5px;
`



export const DialogTarif = ({ open, setDialogTarif }: IProps) => {
    const [value, setValue] = React.useState('1');

    let finalprice = 0;
    let mprice = 0;
    if (value == "13") {
        finalprice = 90;
        mprice = 90;
    } else if (value == "14") {
        finalprice = 252;
        mprice = 84;
    }
    else if (value == "15") {
        finalprice = 799;
        mprice = 66;
    }


    const handleClickTarif = () => {
        //console.log('https://sendme.cc/payment/' + value)
        window.location.replace('https://sendme.cc/payment/' + value);
    }

    const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        setValue((event.target as HTMLInputElement).value);
    };

    return (
        <Dialog open={open} fullWidth={true} maxWidth="md" onClose={() => setDialogTarif(false)} >
            <DialogTitle>
                Выберите подходящую подписку 🤑
            <CloseButton onClick={() => setDialogTarif(false)}><CloseIcon /></CloseButton>
            </DialogTitle>

            <DialogContent>
                <Grid
                    container
                    direction="row"
                    justify="space-between"
                    alignItems="stretch"
                    spacing={1}
                >
                    <Grid item xs={6}>
                        <div className="s_pricing-item">
                            {/* <img className="shape_img" src="/land_assets/img/price_line2.png" alt="" />
                                <div className="tag_label">БЕСПЛАТНО</div>
                                <div className="s_price_icon p_icon2">
                                    <i className="flaticon-mortarboard"></i>
                                </div> */}
                            <h5 className="f_p f_600 f_size_20 t_color mb-0" style={{ margin: 0 }}>FREE навсегда 🤩</h5>

                            <div className="price f_size_40 f_p f_700" style={{ lineHeight: 'initial' }}>
                                <span style={{ color: '#36a9e1' }}>0 Руб <sub className="f_400 f_size_16">/ mo</sub></span>
                            </div>
                            <div style={{ textAlign: 'left' }}>
                                Пользуйтесь и изучайте платформу бесплатно сколько хотите.<br />
                                Здесь почти нет ограничений.
                                </div>
                            <Button className="price_btn btn_hover mt_30" fullWidth disabled>Подключено</Button>
                            {/* <a href="/signup" className="price_btn btn_hover mt_30" style={{display:'block'}}>Подключить</a> */}
                        </div>
                    </Grid>

                    <Grid item xs={6}>
                        <div className="s_pricing-item">
                            <h5 className="f_p f_600 f_size_20 t_color mb-0" style={{ margin: 0 }}>PRO 🤩😎</h5>

                            <div className="price f_size_40 f_p f_700" style={{ lineHeight: "initial" }}>
                                <span style={{ color: '#36a9e1' }}>{mprice} Руб <sub className="f_400 f_size_16">/ mo</sub></span>
                            </div>
                            <FormControl component="fieldset" style={{ marginTop: 10 }}>
                                <FormLabel component="legend">Выберите срок подписки:</FormLabel>
                                <RadioGroup row aria-label="position" name="position" value={value} onChange={handleChange}>
                                    <FormControlLabel
                                        value="13"
                                        control={<Radio color="primary" />}
                                        label="1 месяц"
                                        labelPlacement="top"
                                    />
                                    <FormControlLabel
                                        value="14"
                                        control={<Radio color="primary" />}
                                        label="3 месяца"
                                        labelPlacement="top"
                                    />
                                    <FormControlLabel
                                        value="15"
                                        control={<Radio color="primary" />}
                                        label="12 месяцев"
                                        labelPlacement="top"
                                    />
                                </RadioGroup>
                            </FormControl>
                            <div style={{ textAlign: 'left' }}>
                                Все возможности без ограничений.<br />
                                Личный помощник, который может все сделать за Вас.<br />
                                Красивая ссылка на вашу страницу, которую можно менять.<br />
                                И никакой рекламы.<br />
                            </div>


                            <Button onClick={handleClickTarif} className="price_btn btn_hover mt_30" fullWidth>Подключить за {finalprice} Руб</Button>
                        </div>

                    </Grid>




                </Grid>


            </DialogContent>


        </Dialog>
    )
}