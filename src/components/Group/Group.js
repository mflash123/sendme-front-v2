import React, { useContext, useEffect, useState } from "react";
import {
  Button,
  Box,
  CircularProgress,
  Paper,
  Breadcrumbs,
  Typography,
} from "@material-ui/core";

import { ApiContext } from "../../context/ApiContext";
import { Link } from "react-router-dom";

const Group = (props) => {
  const { typeGroups } = useContext(ApiContext);
  const { id, name } = props.tarif;

  const [groups, setGroups] = useState(false);

  useEffect(() => {
    const data = async () => {
      const resp = await typeGroups();
      console.log(resp);
      setGroups(resp.data);
    };
    data();
  }, []);

  const GroupList = () =>
    groups.map((value, index) => {
      const href = () => {
        switch (value.id) {
          case 5:
            /** txt block */
            return "/dashboard/add/5/26";
          // case 6:
          //     /** img deprecade */
          //     return '6/27'
          case 9:
            /** custom btn */
            return "/dashboard/add/9/21";
          case 10:
            /** product */
            return "/dashboard/product";

          case 14:
            /** question-ask */
            return "/dashboard/add/14/35";

          case 15:
            /** textfield draftjs */
            return "/dashboard/add/15/39";

          default:
            return "/dashboard/add/" + value.id;
        }
      };
      return (
        <Box my={1} key={index}>
          <Paper elevation={0} style={{ textAlign: "center" }}>
            <Button
              to={href()}
              component={Link}
              fullWidth={true}
              style={{ paddingTop: 20, paddingBottom: 20 }}
              size="large"
              //disabled={ id!=2 ? value.tarif_id>id : false }
            >
              <Typography variant="label">
                <i className={value.classIcon}></i> {value.humanName}{" "}
                <i className="fal fa-angle-right"></i>
              </Typography>
            </Button>

            {/* {
                    id!=2
                    ?
                        value.tarif_id>id
                        && <Link href="/tarif"><Typography variant="overline" display="block" gutterBottom>Недоступно на этом тарифе</Typography></Link>
                    :
                        false
                    } */}
          </Paper>
        </Box>
      );
    });

  if (groups) return <GroupList />;

  return <CircularProgress />;
};

export default Group;
