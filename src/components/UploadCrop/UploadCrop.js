import React, { useState, useCallback, useContext } from "react";
import ReactDOM from "react-dom";
import Cropper from "react-easy-crop";
import Button from "@material-ui/core/Button";
import {
  Typography,
  Slider,
  Backdrop,
  Card,
  CardContent,
  Avatar,
  Fab,
  IconButton,
} from "@material-ui/core";
import { getCroppedImg, getRotatedImage } from "./canvasUtils";
import ImgDialog from "./ImgDialog";
import { makeStyles } from "@material-ui/core/styles";
import { ApiContext } from "../../context/ApiContext";
import DeleteIcon from "@material-ui/icons/Delete";
import styled from "styled-components";

const useStyles = makeStyles((theme) => ({
  backdrop: {
    zIndex: theme.zIndex.drawer + 1,
    color: "#fff",
    height: "100%",
  },
}));

//animation example
const FabStyled = styled(Fab)`
  transition: width 1s, height 1s;
  transition-timing-function: ease-in-out;
  ${(props) => props.size === "small" && "width:100px;height:100px"}
  ${(props) => props.size === "medium" && "width:150px;height:150px"}
  ${(
    props
  ) => props.size === "large" && "width:200px;height:200px"}
`;

const UploadCrop = ({ user }) => {
  const classes = useStyles();
  const { storeContent, deleteContent } = useContext(ApiContext);
  const [size, setSize] = useState("large");

  const [imageSrc, setImageSrc] = React.useState(null); //null
  const [crop, setCrop] = useState({ x: 0, y: 0 });
  const [rotation, setRotation] = useState(0);
  const [zoom, setZoom] = useState(1);
  const [croppedAreaPixels, setCroppedAreaPixels] = useState(null);
  const [croppedImage, setCroppedImage] = useState(null);

  function readFile(file) {
    return new Promise((resolve) => {
      const reader = new FileReader();
      reader.addEventListener("load", () => resolve(reader.result), false);
      reader.readAsDataURL(file);
    });
  }

  const onCropComplete = useCallback((croppedArea, croppedAreaPixels) => {
    setCroppedAreaPixels(croppedAreaPixels);
  }, []);

  const showCroppedImage = useCallback(async () => {
    try {
      const croppedImage = await getCroppedImg(
        imageSrc,
        croppedAreaPixels,
        rotation
      );
      console.log("donee", { croppedImage });

      setCroppedImage(croppedImage);

      storeContent({ name: croppedImage, type: "avatarSingle" }, 1).then(() => {
        document.location.reload(true);
      });
    } catch (e) {
      console.error(e);
    }
  }, [imageSrc, croppedAreaPixels, rotation]);

  const onClose = useCallback(() => {
    setCroppedImage(null);
  }, []);

  const onFileChange = async (e) => {
    if (e.target.files && e.target.files.length > 0) {
      const file = e.target.files[0];
      let imageDataUrl = await readFile(file);

      setImageSrc(imageDataUrl);
    }
  };

  const removeAvatar = () => {
    console.log("removeAvatar=" + user.avatar.id);
    deleteContent(user.avatar.id).then(() => window.location.reload());
  };

  return (
    <div>
      {imageSrc ? (
        <React.Fragment>
          {/* <Backdrop open={imageSrc} className={classes.backdrop}> */}
          <Card className="card_cropper">
            <CardContent>
              <Cropper
                image={imageSrc}
                crop={crop}
                //rotation={rotation}
                zoom={zoom}
                aspect={1 / 1}
                onCropChange={setCrop}
                onRotationChange={setRotation}
                onCropComplete={onCropComplete}
                onZoomChange={setZoom}
                cropShape="round"
                classes={
                  {
                    //   containerClassName: 'string',
                    //   mediaClassName: 'stringss',
                    //cropAreaClassName: 'strsdsd'
                  }
                }
                style={{
                  containerStyle: {
                    height: 400,
                    zIndex: 9,
                    position: "relative",
                  },
                  mediaStyle: { backgroundColor: "white" },
                  //cropAreaStyle: {backgroundColor:'white'}
                }}
              />

              <Typography variant="overline">Zoom</Typography>
              <Slider
                value={zoom}
                min={1}
                max={3}
                step={0.1}
                aria-labelledby="Zoom"
                onChange={(e, zoom) => setZoom(zoom)}
              />
              <Button
                onClick={showCroppedImage}
                variant="contained"
                color="primary"
              >
                Сохранить
              </Button>
            </CardContent>
          </Card>

          {/* </Backdrop> */}
          {/* <ImgDialog img={croppedImage} onClose={onClose} /> */}
        </React.Fragment>
      ) : (
        <div style={{ textAlign: "center" }}>
          <input
            accept="image/*"
            hidden
            id="contained-button-file"
            type="file"
            onChange={onFileChange}
          />
          <label htmlFor="contained-button-file">
            <Fab
              component="span"
              style={{
                boxShadow: "none",
                width: 200,
                height: 200,
                backgroundColor: "white",
              }}
            >
              {user.avatar ? (
                <Avatar
                  src={
                    process.env.REACT_APP_STORAGE_URL +
                    user.avatar.data.fileName
                  }
                  style={{ width: "100%", height: "100%" }}
                />
              ) : (
                <Avatar
                  src="/upload.png"
                  style={{
                    width: "100px",
                    height: "100px",
                    border: "1px dashed #d6d6d6",
                  }}
                />
              )}
            </Fab>
          </label>
          {user.avatar && (
            <IconButton onClick={removeAvatar}>
              <DeleteIcon />
            </IconButton>
          )}
        </div>

        // <input type="file" onChange={onFileChange} accept="image/*" />
      )}
    </div>
  );
};

export default UploadCrop;
