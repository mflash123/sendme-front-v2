import React, { useState } from 'react';
import { Alert } from '@material-ui/lab';
import { Snackbar } from '@material-ui/core';


// interface NotificationProps {
//     open: boolean;
// }



export const NotificationInstagramSuccess = () => {

    let isShow: boolean = false;
    isShow = (localStorage.getItem("notification_connect_instagram") =="true");

    const [open, setOpen] = useState(isShow)

    const handleClose = () => {
        localStorage.removeItem("notification_connect_instagram")
        setOpen(false)
    }
    
    return (
        <Snackbar open={open} onClose={ handleClose } autoHideDuration={6000} anchorOrigin={{ vertical: 'top', horizontal: 'center' }} >
            <Alert severity="success">Instagram подключен!</Alert>
        </Snackbar>
    )
}
