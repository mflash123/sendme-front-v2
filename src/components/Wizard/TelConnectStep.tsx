import { Box, Button, Checkbox, DialogActions, DialogContent, Divider, FormControlLabel, Typography } from '@material-ui/core';
import React from 'react';
import { Controller, useForm } from "react-hook-form";
import PhoneInput from 'react-phone-input-2';
import 'react-phone-input-2/lib/style.css';
import { WizardContext } from './../../context/wizardContext';

interface IProps {
    setStep: (step:number) =>void
}

const defaultValues = {
    iswhatsapp: false,
    isphone: false,
    phone: '',
}

type IFormInputs = {
    phone: string,
    iswhatsapp: boolean,
    isphone: boolean,
};

//Step3
export const TelConnectStep:React.FC<IProps> = ({setStep}) => {
    const { saveWizard } = React.useContext(WizardContext) as ContextType;

    const { control, handleSubmit, formState: { errors } } = useForm<IFormInputs>({
        defaultValues
    });

    const onSubmit = (data: any) => {
        //save data
        console.log(data)
        saveWizard(data)
        setStep(4)
    }

    console.log(Boolean(errors?.phone))
    return (
        <form onSubmit={handleSubmit(onSubmit)}>
            <DialogContent>
                <Typography>Напишите ваш телефон, по которому Ваши клиенты могу с вами связаться. И отметье галочкой, что у Вас привязано к Вашему номеру телефона.</Typography>
                <Divider />



                <FormControlLabel
                    control={
                        <Controller
                            name="iswhatsapp"
                            control={control}
                            render={({ field: props }) => (
                                <Checkbox
                                    {...props}
                                    onChange={(e) => props.onChange(e.target.checked)}
                                />
                            )}

                        />
                    }
                    label="WhatsApp"
                />

                <FormControlLabel
                    control={
                        <Controller
                            name="isphone"
                            control={control}
                            render={({ field: props }) => (
                                <Checkbox
                                    {...props}
                                    onChange={(e) => props.onChange(e.target.checked)}
                                />
                            )}

                        />
                    }
                    label="Телефон"
                />




                <Box component="span" display="block">
                  
                    <Controller
                        name="phone"
                        control={control}
                        defaultValue=""
                        render={({ field: { onChange } }) => (
                            <PhoneInput
                                onChange={onChange}
                                inputProps={{
                                    required: true,
                                    autoFocus: true
                                }}
                            />
                        )}
                        rules={{ required: true,minLength:6 }}
                    />
                    {Boolean(errors?.phone) && <Typography color='error'>Проверьте правильность номера</Typography>}
                    
                </Box>

            </DialogContent>
            <DialogActions>
            
                <Button type="submit">Дальше ➡️</Button>
            </DialogActions>
        </form>
    )
}