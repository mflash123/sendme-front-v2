import { Dialog, DialogContent, DialogTitle, Typography } from '@material-ui/core';
import React, { useContext, useState } from 'react';
import 'react-phone-input-2/lib/style.css';
import { ApiContext } from "./../../context/ApiContext";
import WizardProvider from './../../context/wizardContext';
import { FinalStep, Step1, Step2, Step3, Step4, Step5 } from './index';








export interface DialogWizardProps {
    open: boolean;
}





export const DialogWizard = ({ open }: DialogWizardProps) => {
    const [step, setStep] = useState(1) //1

    const { isInstagramConnect } = useContext(ApiContext);

    React.useEffect(() => {
        isInstagramConnect()
            .then(({ data }: any) => {
                if (data) setStep(2) //не забудь поставить 2
            })
    }, [])


console.log(open)

    return (
        <Dialog open={open}>
            <DialogTitle>Быстрая настройка 🤯</DialogTitle>
            <DialogContent>
                <Typography>Чтобы Вы могли быстро начать пользоваться платформой, ответье на вопросы, и система все настроит за Вас.</Typography>
            </DialogContent>
            <WizardProvider>
                {step === 1 && <Step1 setStep={setStep} />}
                {step === 2 && <Step2 setStep={setStep} />}
                {step === 3 && <Step3 setStep={setStep} />}
                {step === 4 && <Step4 setStep={setStep} />}
                {step === 5 && <Step5 setStep={setStep} />}
                {step === 6 && <FinalStep />}
            </WizardProvider>

        </Dialog>
    )
}