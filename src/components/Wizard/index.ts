import { FinalStep } from './FinalStep'
import { InstagramConnectStep as Step1 } from './InstagramConnectStep'
import { InstagramStep as Step2 } from './InstagramStep'
import { IsEmailStep as Step4 } from './IsEmailStep'
import { TelConnectStep as Step3 } from './TelConnectStep'
import { CategoryStep as Step5 } from './CategoryStep'

export { Step1, Step2, Step3, Step4,Step5, FinalStep }
