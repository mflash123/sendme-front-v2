import { Button, DialogActions, DialogContent, Divider, Grid, Switch, Typography } from '@material-ui/core';
import React from 'react';
import { Controller, useForm } from "react-hook-form";
import 'react-phone-input-2/lib/style.css';
import { WizardContext } from './../../context/wizardContext';

interface IProps {
    setStep: (step:number) => void,
}

const defaultValues = {
    isemail: false,
}

type IFormInputs = {
    isemail: boolean,
};

//Step4
export const IsEmailStep:React.FC<IProps> = ({setStep}) => {
    const { saveWizard } = React.useContext(WizardContext) as ContextType;
    const { control, handleSubmit } = useForm<IFormInputs>({
        defaultValues
    });

    const onSubmit = (data: any) => {
        //save data
       // console.log(data)s
        saveWizard(data)
        setStep(5)

    }

    return (
        <form onSubmit={handleSubmit(onSubmit)}>
            <DialogContent>
                <Typography>При регистрации Вы указали email, хотите подключить его сразу на Ваш сайт для клиентов?</Typography>
                <Divider />

                <Typography component="div">
                    <Grid component="label" container alignItems="center" spacing={1}>
                        <Grid item>Нет, не хочу 💩</Grid>
                        <Grid item>
                            <Controller
                                name="isemail"
                                control={control}
                                render={({ field: props }) => (
                                    <Switch
                                        {...props}
                                        onChange={(e) => props.onChange(e.target.checked)}
                                    />
                                )}

                            />
                        </Grid>
                        <Grid item>Да, подключить 👌</Grid>
                    </Grid>
                </Typography>

            </DialogContent>
            <DialogActions>
                <Button type="submit">Дальше ➡️</Button>
            </DialogActions>
        </form>
    )
}