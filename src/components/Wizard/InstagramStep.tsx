import { Avatar, Button, Checkbox, DialogActions, DialogContent, FormControlLabel, Grid, LinearProgress, TextField, Typography } from '@material-ui/core';
import { Alert } from '@material-ui/lab';
import React from 'react';
import { Controller, useForm } from "react-hook-form";
import { useTranslation } from 'react-i18next';
import 'react-phone-input-2/lib/style.css';
import { ApiContext } from "./../../context/ApiContext";
import { WizardContext } from './../../context/wizardContext';

interface IProps {
    setStep: (step: number) => void,
}

const defaultValues = {
    description: '',
    //avatar: '',
    isinstagram: true,
    isavatar: true,
    isdescription: true,
    username: '',
    avatar: '',
}

type IFormInputs = {
    description: string,
    //avatar: string,
    isinstagram: boolean,
    isavatar: boolean,
    isdescription: boolean,
    username: string,
    avatar: string,
};




//Step2
export const InstagramStep: React.FC<IProps> = ({ setStep }) => {
    const { t } = useTranslation();
    const { saveWizard } = React.useContext(WizardContext) as ContextType;
    // export const InstagramStep = ({...props}:IProps) => {
    const { getInstagram } = React.useContext(ApiContext);
    const [image, setImage] = React.useState('')
    const [isloading, setLoading] = React.useState(true)
    const [isError, setError] = React.useState(false)

    const { control, handleSubmit, setValue, register } = useForm<IFormInputs>({
        defaultValues
    });


    const onSubmit = (data: any) => {
        //save data
        saveWizard(data)
        setStep(3)
        console.log(data);
    }

    console.log(defaultValues)

    React.useEffect(() => {
        getInstagram()
            .then(({ data }: any) => {
                console.log(data.profilePicUrlHd)
                setImage(data.profilePicUrlHd)
                setValue('description', data.Biography)
                setValue('username', '@' + data.Username)
                setValue('avatar', data.profilePicUrlHd)
                setLoading(false)
            })
            .catch((e: any) => {
                console.log(e)
                setError(true)
            })
    }, [])

    if (isloading) {
        return (
            <>
                {isError && <Alert severity="error">{t('error')}</Alert>}
                <LinearProgress style={{ paddingTop: 20, paddingBottom: 20 }} />
            </>)
    }
    return (
        <form onSubmit={handleSubmit(onSubmit)}>
            <DialogContent>
                <Grid container spacing={3}>
                    <Grid item xs={12}>
                        <Typography>Мы подключили вашу инсту, хотите мы перенесем <b>описание</b>, <b>аватарку</b> и сделаем <b>ссылку на Ваш Инстаграм</b>😎?</Typography>
                    </Grid>

                    <Grid item xs={6}>
                        <FormControlLabel
                            control={
                                <Controller
                                    name="isinstagram"
                                    defaultValue={defaultValues.isinstagram}
                                    control={control}
                                    render={({ field: props }) => (
                                        <Checkbox
                                            {...props}
                                            onChange={(e) => props.onChange(e.target.checked)}
                                            checked={props.value}
                                        />
                                    )}

                                />
                            }
                            label="Добавить мой Instagram"
                        />
                    </Grid>
                    <Grid item xs={6}>
                        <Controller
                            name="username"
                            defaultValue={defaultValues.username}
                            control={control}
                            render={({ field: props }) => (
                                <TextField
                                    disabled
                                    {...props}
                                    onChange={(e) => props.onChange(e.target.value)}
                                    variant="outlined"
                                />
                            )}

                        />
                    </Grid>

                    <Grid item xs={6}>
                        <FormControlLabel
                            control={
                                <Controller
                                    name="isavatar"
                                    defaultValue={defaultValues.isavatar}
                                    control={control}
                                    render={({ field: props }) => (
                                        <Checkbox
                                            {...props}
                                            onChange={(e) => props.onChange(e.target.checked)}
                                            checked={props.value}
                                        />
                                    )}

                                />
                            }
                            label="Добавить мой аватар"
                        />
                        <input {...register("avatar")} hidden />
                    </Grid>
                    <Grid item xs={6}>
                        {/* {image && <Avatar src={process.env.REACT_APP_API_URL+image} />} */}
                        <Avatar src={process.env.REACT_APP_API_URL + image} />

                    </Grid>

                    <Grid item xs={6}>
                        <FormControlLabel
                            control={
                                <Controller
                                    name="isdescription"
                                    defaultValue={defaultValues.isdescription}
                                    control={control}
                                    render={({ field: props }) => (
                                        <Checkbox
                                            {...props}
                                            onChange={(e) => props.onChange(e.target.checked)}
                                            checked={props.value}
                                        />
                                    )}

                                />
                            }
                            label="Добавить описание"
                        />
                    </Grid>
                    <Grid item xs={6}>
                        <Controller
                            name="description"
                            defaultValue={defaultValues.description}
                            control={control}
                            render={({ field: props }) => (
                                <TextField
                                    disabled
                                    {...props}
                                    onChange={(e) => props.onChange(e.target.value)}
                                    multiline
                                    rows={4}
                                    variant="outlined"
                                />
                            )}

                        />
                    </Grid>

                </Grid>
            </DialogContent>
            <DialogActions>
                <Button type="submit">Дальше ➡️</Button>
            </DialogActions>
        </form>
    )
}