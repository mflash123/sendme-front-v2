import { Button, DialogContent } from '@material-ui/core';
import React from 'react';
import { ButtonMarginStyled } from '../../styled';
import { Instagram } from '../Dashboard/Settings/Instagram';

interface IProps {
    setStep: (step: number) => void,
}

export const InstagramConnectStep: React.FC<IProps> = ({ setStep }) => {
    return (
        <DialogContent>
            <ButtonMarginStyled>
                <Instagram />
            </ButtonMarginStyled>
            <ButtonMarginStyled>
                <Button variant="contained" disableElevation={true} onClick={() => { setStep(3) }}>Не хочу ничего подключать 💩</Button>
            </ButtonMarginStyled>
        </DialogContent>
    )
}