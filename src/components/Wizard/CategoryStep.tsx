import { Button, DialogActions, DialogContent, Divider, LinearProgress, TextField, Typography } from '@material-ui/core';
import { Autocomplete } from '@material-ui/lab';
import React from 'react';
import { useForm } from "react-hook-form";
import 'react-phone-input-2/lib/style.css';
import { ApiContext } from "./../../context/ApiContext";
import { WizardContext } from './../../context/wizardContext';


interface IProps {
    setStep: (step: number) => void,
}

const defaultValues = {
    category: '',
}

type IFormInputs = {
    category: string,
};



export const CategoryStep = ({ setStep }: IProps) => {
    const [isloading, setLoading] = React.useState(true)
    const [categories, setCategories] = React.useState<any[]>([])
    const [categoriesSelected, setCategoriesSelected] = React.useState<any[]>([])

    //const [categoryIds, setCategoryIds] = React.useState<string[]>([]);
    //const [categoryIds, setCategoryIds] = React.useState<any[]>([]);

    const { getAvailableCategory } = React.useContext(ApiContext);
    const { saveWizard } = React.useContext(WizardContext) as ContextType;
    const { handleSubmit } = useForm<IFormInputs>({
        defaultValues
    });

    const onSubmit = () => {
        saveWizard({ categories: categoriesSelected }, true)
        setStep(6)
    }

    React.useEffect(() => {
        getAvailableCategory()
            .then(({ data }: any) => {
                setCategories(data)
                setLoading(false)
            })
    }, [])

    if (isloading) {
        return <LinearProgress style={{ paddingTop: 20, paddingBottom: 20 }} />
    }

    const handleChange = (_event: any, value: any) => {
        setCategoriesSelected(value)
    };


    return (
        <form onSubmit={handleSubmit(onSubmit)}>
            <DialogContent>
                <Typography>Выберите одну или несколько категорий, к которой можно отнести Ваш бизнес.</Typography>
                <Divider />


                <Autocomplete
                    multiple
                    options={categories}
                    getOptionLabel={(option) => option.name}
                    //getOptionDisabled={(option) => option.length < 2 ? true : false}
                    //defaultValue={[categoryIds[1]]}
                    //defaultValue={[categories[1].name]}
                    onChange={handleChange}
                    renderInput={(params) => (
                        <TextField
                            {...params}
                            variant="standard"
                            label="Выберите категорию"
                            placeholder="Категория"
                        />
                    )}
                />
            </DialogContent>
            <DialogActions>
                <Button type="submit">Дальше ➡️</Button>
            </DialogActions>
        </form>
    )
}