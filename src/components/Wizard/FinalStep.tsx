import { DialogContent, LinearProgress, Typography } from '@material-ui/core';
import React from 'react';
import 'react-phone-input-2/lib/style.css';

interface IProps {

}



//Step3
export const FinalStep: React.FC<IProps> = () => {
    const [progress, setProgress] = React.useState(10);

    React.useEffect(() => {
        const timer = setInterval(() => {
            setProgress((prevProgress) => (prevProgress >= 100 ? 10 : prevProgress + 10));
        }, 800);
        return () => {
            clearInterval(timer);
        };
    }, []);

    if(progress===100){
       window.location.replace("/dashboard");
    }
    return (
        <DialogContent>
            <Typography variant="h6">Похоже, мы закончили. Дайте нам пару секунд 🥳</Typography>
            <LinearProgress variant="determinate" value={progress} />
        </DialogContent>
    )
}