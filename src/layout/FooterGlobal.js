import React from "react";
import { IconButton, Grid } from "@material-ui/core";
import { Facebook, YouTube, Instagram, Telegram } from "@material-ui/icons";

const FooterGlobal = () => {
  return (
    <Grid item style={{ marginTop: "6em" }}>
      <IconButton target="_blank" href="https://www.facebook.com/sendmecc">
        <Facebook />
      </IconButton>
      <IconButton
        target="_blank"
        href="https://www.youtube.com/channel/UCt4UPSRenth0havIeTwndLA"
      >
        <YouTube />
      </IconButton>
      <IconButton target="_blank" href="https://instagram.com/sendmecc/">
        <Instagram />
      </IconButton>
      <IconButton target="_blank" href="https://t.me/sendmecc">
        <Telegram />
      </IconButton>
    </Grid>
  );
};

export default FooterGlobal;
