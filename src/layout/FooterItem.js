import React from "react";
import { Button, Typography, Grid } from "@material-ui/core";
import { Link } from "react-router-dom";
import KeyboardArrowLeftIcon from "@material-ui/icons/KeyboardArrowLeft";
import SaveIcon from "@material-ui/icons/Save";

const FooterItem = (props) => {
  const { handleSave } = props;

  return (
    <Grid container justify="flex-end">
      <Button component={Link} to={"/dashboard/add/"} color="primary">
        <KeyboardArrowLeftIcon /> Back
      </Button>
      <Button
        color="primary"
        type="submit"
        onClick={() => {
          handleSave();
        }}
      >
        <SaveIcon /> Save
      </Button>
    </Grid>
  );
};

export default FooterItem;
