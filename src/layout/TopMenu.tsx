import { Avatar, Chip, Dialog, DialogActions, LinearProgress } from '@material-ui/core';
import AppBar from '@material-ui/core/AppBar';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';
import YouTube from "react-youtube";
import { logo } from '../base64';
import { useStyles } from '../style';
import { DialogTarif } from './../components/Tarif';
import { PriceContext } from './../context/priceContext';



interface IProps {
    user: any;
    tarif: any;
    settingsCallback: (value: boolean) => void;
}

// type ContextType = {
//     priceDialog: boolean;
//     setOpen: (value: boolean) => void;
// }

interface DialogVideoProps {
    open: boolean;
    setDialog: (value: boolean) => void;
}

export const TopMenu: React.FC<IProps> = ({ tarif, user, settingsCallback }) => {
    const { t } = useTranslation();
    const { open, setOpen } = React.useContext(PriceContext) as DialogDefaultContextType;

    const classes = useStyles();
    //const [theme,setTheme] = useContext(ThemeContext)
    const [dialog, setDialog] = useState(false)
    //const [dialogTarif, setDialogTarif] = useState(false)
    const { name } = tarif
    /**API */
    // const [user,setUser,userResponse] = useContext(ApiContext)
    // console.log(user)
    /**API */

    const ChipWidgetSettings = () => user.avatar
        ? <Chip onClick={() => { settingsCallback(true) }} variant="outlined" avatar={<Avatar src={process.env.REACT_APP_STORAGE_URL + user.avatar.data.fileName} />} label={t('settings')} />
        : <Chip onClick={() => { settingsCallback(true) }} variant="outlined" avatar={<Avatar>{user.username.substring(0, 2)}</Avatar>} label={t('settings')} />



    return user ?
        (
            <AppBar position="static" color="inherit" style={{ background: 'transparent', boxShadow: 'none' }}>
                <Toolbar>
                    <IconButton href="/dashboard" edge="start" className={classes.menuButton} color="inherit" aria-label="menu">
                        <img style={{ height: 25 }} src={logo} />
                    </IconButton>
                    <Typography variant="h6" className={classes.title}>
                        {/* <Link href="/tarif">Ваш тариф: {name}</Link> */}
                        <DialogVideo open={dialog} setDialog={setDialog} />
                        <Button onClick={() => setDialog(!dialog)}>Посмотреть видео обзор сервиса</Button>
                    </Typography>
                    <Chip clickable onClick={() => setOpen(true)} variant="outlined" label={name} style={{ marginRight: 5 }} />


                    {/* <Button color="inherit">Укажите бизнес</Button> */}
                    {/* <Button color="inherit">Журналист. Редактор.</Button> */}
                    {/* {user.avatar
                    ? <Avatar src={process.env.REACT_APP_STORAGE_URL+user.avatar.data.fileName}>H</Avatar>
                    : <Avatar>{user.username.substring(0,2).toUpperCase()}</Avatar>
                    }
                        

                    <Button href="/logout" color="inherit">Выйти</Button> */}
                    <ChipWidgetSettings />
                </Toolbar>
                <DialogTarif open={open} setDialogTarif={setOpen} />
            </AppBar>
        )

        : <LinearProgress />


}

const DialogVideo = ({ open, setDialog }: DialogVideoProps) => {
    return (
        <Dialog open={open} onBackdropClick={() => setDialog(false)}>
            <YouTube videoId='TFOWsfZB5T8' />
            <DialogActions>
                <Button color="primary" onClick={() => setDialog(false)}>Закрыть</Button>
            </DialogActions>
        </Dialog>
    )
}