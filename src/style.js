//import React from 'react';
//import React from 'react';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
    root: {
      flexGrow: 1,
      backgroundColor: '#fafafa'
    },
    menuButton: {
      marginRight: theme.spacing(2),
    },
    title: {
      flexGrow: 1,
    },
    linkBlock: {
      marginTop: 20,
    },
    linkBlockPadding: {
      padding:15,
    },
    imageSmall: {
      width: theme.spacing(3),
      height: theme.spacing(3),
    },
    imageBig: {
      width: theme.spacing(7),
      height: theme.spacing(7),
    },
    border: {
      border: '1px dotted',
    },
    paperfield: {
      marginTop: '2rem',
      padding: '2rem',
    },
    alert: {
      margin: '20px 0 20px',
    },
    formCenter: {
      alignItems: 'center',
    },
    buttonMargin: {
      margin: "24px 0px 16px",
    },
    ulList: {
      paddingLeft: 0,
      listStyle: 'none',
      '& li a': {
        textDecoration: 'none',
      }
    },
  }));

  export {useStyles}