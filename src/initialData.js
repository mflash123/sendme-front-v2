const initialData = {
    item:{
        'item-1': {
            id:'item-1',
            data: 'Ebat kolotit 1',
            type: 1,
        },
        'item-2': {
            id:'item-2',
            data: 'https://material-ui.com/static/images/avatar/3.jpg',
            type: 2,
        },
        'item-3': {
            id:'item-3',
            data: 'Ebat kolotit 1',
            type: 1,
        },

    },
    items:['item-1','item-2','item-3'] //current user items
}

export default initialData
/**
 * Types:
 * 1-text
 * 2-avatar
 */